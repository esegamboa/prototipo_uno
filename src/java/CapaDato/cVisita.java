/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package CapaDato;

/**
 *
 * @author EseGamboa
 */
public class cVisita {

    private int id_visita_categorizacion;
   private int d1_visita_categorizacion;
   private int d2_visita_categorizacion;
   private int d3_visita_categorizacion;
   private int d4_visita_categorizacion;
   private int d5_visita_categorizacion;
   private int d6_visita_categorizacion;
   private int r1_visita_categorizacion;
   private int r2_visita_categorizacion;
   private int r3_visita_categorizacion;
   private int r4_visita_categorizacion;
   private int r5_visita_categorizacion;
   private int r6_visita_categorizacion;
   private int r7_visita_categorizacion;
   private int r8_visita_categorizacion;
   private String cat_visita_categorizacion;
   private String obs_visita;
   private String fecha_visita;
   private String hora_visita;
   private String rut_usuario;
   private String nombre_usuario;
   private String apellidop_usuario;
   private String apellidom_usuario;
   private int id_cama;
   private String descripcion_cama;
   private int id_duo;
   private int tipo_visita;

    public cVisita() {
        this.id_visita_categorizacion = -1;
        this.d1_visita_categorizacion = -1;
        this.d2_visita_categorizacion = -1;
        this.d3_visita_categorizacion = -1;
        this.d4_visita_categorizacion = -1;
        this.d5_visita_categorizacion = -1;
        this.d6_visita_categorizacion = -1;
        this.r1_visita_categorizacion = -1;
        this.r2_visita_categorizacion =-1;
        this.r3_visita_categorizacion = -1;
        this.r4_visita_categorizacion = -1;
        this.r5_visita_categorizacion = -1;
        this.r6_visita_categorizacion = -1;
        this.r7_visita_categorizacion = -1;
        this.r8_visita_categorizacion = -1;
        this.cat_visita_categorizacion = "";
        this.obs_visita ="";
        this.fecha_visita = "";
        this.hora_visita = "";
        this.rut_usuario = "";
        this.nombre_usuario = "";
        this.apellidop_usuario = "";
        this.apellidom_usuario = "";
        this.id_cama = -1;
        this.descripcion_cama = "";
        this.id_duo = -1;
        this.tipo_visita = -1;
    }

    /**
     * @return the id_visita_categorizacion
     */
    public int getId_visita_categorizacion() {
        return id_visita_categorizacion;
    }

    /**
     * @param id_visita_categorizacion the id_visita_categorizacion to set
     */
    public void setId_visita_categorizacion(int id_visita_categorizacion) {
        this.id_visita_categorizacion = id_visita_categorizacion;
    }

    /**
     * @return the d1_visita_categorizacion
     */
    public int getD1_visita_categorizacion() {
        return d1_visita_categorizacion;
    }

    /**
     * @param d1_visita_categorizacion the d1_visita_categorizacion to set
     */
    public void setD1_visita_categorizacion(int d1_visita_categorizacion) {
        this.d1_visita_categorizacion = d1_visita_categorizacion;
    }

    /**
     * @return the d2_visita_categorizacion
     */
    public int getD2_visita_categorizacion() {
        return d2_visita_categorizacion;
    }

    /**
     * @param d2_visita_categorizacion the d2_visita_categorizacion to set
     */
    public void setD2_visita_categorizacion(int d2_visita_categorizacion) {
        this.d2_visita_categorizacion = d2_visita_categorizacion;
    }

    /**
     * @return the d3_visita_categorizacion
     */
    public int getD3_visita_categorizacion() {
        return d3_visita_categorizacion;
    }

    /**
     * @param d3_visita_categorizacion the d3_visita_categorizacion to set
     */
    public void setD3_visita_categorizacion(int d3_visita_categorizacion) {
        this.d3_visita_categorizacion = d3_visita_categorizacion;
    }

    /**
     * @return the d4_visita_categorizacion
     */
    public int getD4_visita_categorizacion() {
        return d4_visita_categorizacion;
    }

    /**
     * @param d4_visita_categorizacion the d4_visita_categorizacion to set
     */
    public void setD4_visita_categorizacion(int d4_visita_categorizacion) {
        this.d4_visita_categorizacion = d4_visita_categorizacion;
    }

    /**
     * @return the d5_visita_categorizacion
     */
    public int getD5_visita_categorizacion() {
        return d5_visita_categorizacion;
    }

    /**
     * @param d5_visita_categorizacion the d5_visita_categorizacion to set
     */
    public void setD5_visita_categorizacion(int d5_visita_categorizacion) {
        this.d5_visita_categorizacion = d5_visita_categorizacion;
    }

    /**
     * @return the d6_visita_categorizacion
     */
    public int getD6_visita_categorizacion() {
        return d6_visita_categorizacion;
    }

    /**
     * @param d6_visita_categorizacion the d6_visita_categorizacion to set
     */
    public void setD6_visita_categorizacion(int d6_visita_categorizacion) {
        this.d6_visita_categorizacion = d6_visita_categorizacion;
    }

    /**
     * @return the r1_visita_categorizacion
     */
    public int getR1_visita_categorizacion() {
        return r1_visita_categorizacion;
    }

    /**
     * @param r1_visita_categorizacion the r1_visita_categorizacion to set
     */
    public void setR1_visita_categorizacion(int r1_visita_categorizacion) {
        this.r1_visita_categorizacion = r1_visita_categorizacion;
    }

    /**
     * @return the r2_visita_categorizacion
     */
    public int getR2_visita_categorizacion() {
        return r2_visita_categorizacion;
    }

    /**
     * @param r2_visita_categorizacion the r2_visita_categorizacion to set
     */
    public void setR2_visita_categorizacion(int r2_visita_categorizacion) {
        this.r2_visita_categorizacion = r2_visita_categorizacion;
    }

    /**
     * @return the r3_visita_categorizacion
     */
    public int getR3_visita_categorizacion() {
        return r3_visita_categorizacion;
    }

    /**
     * @param r3_visita_categorizacion the r3_visita_categorizacion to set
     */
    public void setR3_visita_categorizacion(int r3_visita_categorizacion) {
        this.r3_visita_categorizacion = r3_visita_categorizacion;
    }

    /**
     * @return the r4_visita_categorizacion
     */
    public int getR4_visita_categorizacion() {
        return r4_visita_categorizacion;
    }

    /**
     * @param r4_visita_categorizacion the r4_visita_categorizacion to set
     */
    public void setR4_visita_categorizacion(int r4_visita_categorizacion) {
        this.r4_visita_categorizacion = r4_visita_categorizacion;
    }

    /**
     * @return the r5_visita_categorizacion
     */
    public int getR5_visita_categorizacion() {
        return r5_visita_categorizacion;
    }

    /**
     * @param r5_visita_categorizacion the r5_visita_categorizacion to set
     */
    public void setR5_visita_categorizacion(int r5_visita_categorizacion) {
        this.r5_visita_categorizacion = r5_visita_categorizacion;
    }

    /**
     * @return the r6_visita_categorizacion
     */
    public int getR6_visita_categorizacion() {
        return r6_visita_categorizacion;
    }

    /**
     * @param r6_visita_categorizacion the r6_visita_categorizacion to set
     */
    public void setR6_visita_categorizacion(int r6_visita_categorizacion) {
        this.r6_visita_categorizacion = r6_visita_categorizacion;
    }

    /**
     * @return the r7_visita_categorizacion
     */
    public int getR7_visita_categorizacion() {
        return r7_visita_categorizacion;
    }

    /**
     * @param r7_visita_categorizacion the r7_visita_categorizacion to set
     */
    public void setR7_visita_categorizacion(int r7_visita_categorizacion) {
        this.r7_visita_categorizacion = r7_visita_categorizacion;
    }

    /**
     * @return the r8_visita_categorizacion
     */
    public int getR8_visita_categorizacion() {
        return r8_visita_categorizacion;
    }

    /**
     * @param r8_visita_categorizacion the r8_visita_categorizacion to set
     */
    public void setR8_visita_categorizacion(int r8_visita_categorizacion) {
        this.r8_visita_categorizacion = r8_visita_categorizacion;
    }

    /**
     * @return the cat_visita_categorizacion
     */
    public String getCat_visita_categorizacion() {
        return cat_visita_categorizacion;
    }

    /**
     * @param cat_visita_categorizacion the cat_visita_categorizacion to set
     */
    public void setCat_visita_categorizacion(String cat_visita_categorizacion) {
        this.cat_visita_categorizacion = cat_visita_categorizacion;
    }

    /**
     * @return the obs_visita
     */
    public String getObs_visita() {
        return obs_visita;
    }

    /**
     * @param obs_visita the obs_visita to set
     */
    public void setObs_visita(String obs_visita) {
        this.obs_visita = obs_visita;
    }

    /**
     * @return the fecha_visita
     */
    public String getFecha_visita() {
        return fecha_visita;
    }

    /**
     * @param fecha_visita the fecha_visita to set
     */
    public void setFecha_visita(String fecha_visita) {
        this.fecha_visita = fecha_visita;
    }

    /**
     * @return the hora_visita
     */
    public String getHora_visita() {
        return hora_visita;
    }

    /**
     * @param hora_visita the hora_visita to set
     */
    public void setHora_visita(String hora_visita) {
        this.hora_visita = hora_visita;
    }

    /**
     * @return the rut_usuario
     */
    public String getRut_usuario() {
        return rut_usuario;
    }

    /**
     * @param rut_usuario the rut_usuario to set
     */
    public void setRut_usuario(String rut_usuario) {
        this.rut_usuario = rut_usuario;
    }

    /**
     * @return the nombre_usuario
     */
    public String getNombre_usuario() {
        return nombre_usuario;
    }

    /**
     * @param nombre_usuario the nombre_usuario to set
     */
    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    /**
     * @return the apellidop_usuario
     */
    public String getApellidop_usuario() {
        return apellidop_usuario;
    }

    /**
     * @param apellidop_usuario the apellidop_usuario to set
     */
    public void setApellidop_usuario(String apellidop_usuario) {
        this.apellidop_usuario = apellidop_usuario;
    }

    /**
     * @return the apellidom_usuario
     */
    public String getApellidom_usuario() {
        return apellidom_usuario;
    }

    /**
     * @param apellidom_usuario the apellidom_usuario to set
     */
    public void setApellidom_usuario(String apellidom_usuario) {
        this.apellidom_usuario = apellidom_usuario;
    }

    /**
     * @return the id_cama
     */
    public int getId_cama() {
        return id_cama;
    }

    /**
     * @param id_cama the id_cama to set
     */
    public void setId_cama(int id_cama) {
        this.id_cama = id_cama;
    }

    /**
     * @return the descripcion_cama
     */
    public String getDescripcion_cama() {
        return descripcion_cama;
    }

    /**
     * @param descripcion_cama the descripcion_cama to set
     */
    public void setDescripcion_cama(String descripcion_cama) {
        this.descripcion_cama = descripcion_cama;
    }

    /**
     * @return the id_duo
     */
    public int getId_duo() {
        return id_duo;
    }

    /**
     * @param id_duo the id_duo to set
     */
    public void setId_duo(int id_duo) {
        this.id_duo = id_duo;
    }

    /**
     * @return the tipo_visita
     */
    public int getTipo_visita() {
        return tipo_visita;
    }

    /**
     * @param tipo_visita the tipo_visita to set
     */
    public void setTipo_visita(int tipo_visita) {
        this.tipo_visita = tipo_visita;
    }

    

   

}
