/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaDato;

/**
 *
 * @author Shiro
 */
public class cIngresoEnfermeria extends cUsuario {

    private int id_ing_enfermeria;
    private String fecha_hora_ing_enfermeria;
    private String otro_comorbilidad_ing_enfermeria;
    private String farmaco_ing_enfermeria;
    private String obs_ing_enfermeria;
    // rut_usuario_ing_enfermeria,
    private int id_examen_fisico_ing_enfermeria;
    private int id_duo_ing_enfermeria;
    private String otro_ex_docto_ing_enfermeria;
    private String conciencia_ex_fisico;
    private String cabeza_ex_fisico;
    private String mucosa_ex_fisico;
    private String torax_ex_fisico;
    private String abdomen_ex_fisico;
    private String eess_ex_fisico;
    private String eeii_ex_fisico;
    private String z_sacra_ex_fisico;
    private String peso_ex_fisico;
    private String talla_ex_fisico;
    private String pulso_ex_fisico;
    private String presion_a_ex_fisico;
    private String temp_ex_fisico;
    private String satura_ex_fisico;
    private String vvp1_ex_fisico;
    private String vvp2_ex_fisico;
    private String vvc_ex_fisico;
    private String sng_ex_fisico;
    private String s_foley_ex_fisico;
    private String dorso_lumbar_ex_fisico;
    private String piel_ex_fisico;

    public cIngresoEnfermeria() {
        this.id_ing_enfermeria = 0;
        this.fecha_hora_ing_enfermeria = "";
        this.otro_comorbilidad_ing_enfermeria = "";
        this.farmaco_ing_enfermeria =  "";
        this.obs_ing_enfermeria = "";
        this.id_examen_fisico_ing_enfermeria = 0;
        this.id_duo_ing_enfermeria =0;
        this.otro_ex_docto_ing_enfermeria =  "";
        this.conciencia_ex_fisico =  "";
        this.cabeza_ex_fisico =  "";
        this.mucosa_ex_fisico = "";
        this.torax_ex_fisico = "";
        this.abdomen_ex_fisico =  "";
        this.eess_ex_fisico = "";
        this.eeii_ex_fisico = "";
        this.z_sacra_ex_fisico =  "";
        this.peso_ex_fisico = "";
        this.talla_ex_fisico =  "";
        this.pulso_ex_fisico = "";
        this.presion_a_ex_fisico =  "";
        this.temp_ex_fisico =  "";
        this.satura_ex_fisico =  "";
        this.vvp1_ex_fisico =  "";
        this.vvp2_ex_fisico = "";
        this.vvc_ex_fisico =  "";
        this.sng_ex_fisico =  "";
        this.s_foley_ex_fisico =  "";
        this.dorso_lumbar_ex_fisico = "";
        this.piel_ex_fisico = "";
    }

    /**
     * @return the id_ing_enfermeria
     */
    public int getId_ing_enfermeria() {
        return id_ing_enfermeria;
    }

    /**
     * @param id_ing_enfermeria the id_ing_enfermeria to set
     */
    public void setId_ing_enfermeria(int id_ing_enfermeria) {
        this.id_ing_enfermeria = id_ing_enfermeria;
    }

    /**
     * @return the fecha_hora_ing_enfermeria
     */
    public String getFecha_hora_ing_enfermeria() {
        return fecha_hora_ing_enfermeria;
    }

    /**
     * @param fecha_hora_ing_enfermeria the fecha_hora_ing_enfermeria to set
     */
    public void setFecha_hora_ing_enfermeria(String fecha_hora_ing_enfermeria) {
        this.fecha_hora_ing_enfermeria = fecha_hora_ing_enfermeria;
    }

    /**
     * @return the otro_comorbilidad_ing_enfermeria
     */
    public String getOtro_comorbilidad_ing_enfermeria() {
        return otro_comorbilidad_ing_enfermeria;
    }

    /**
     * @param otro_comorbilidad_ing_enfermeria the otro_comorbilidad_ing_enfermeria to set
     */
    public void setOtro_comorbilidad_ing_enfermeria(String otro_comorbilidad_ing_enfermeria) {
        this.otro_comorbilidad_ing_enfermeria = otro_comorbilidad_ing_enfermeria;
    }

    /**
     * @return the farmaco_ing_enfermeria
     */
    public String getFarmaco_ing_enfermeria() {
        return farmaco_ing_enfermeria;
    }

    /**
     * @param farmaco_ing_enfermeria the farmaco_ing_enfermeria to set
     */
    public void setFarmaco_ing_enfermeria(String farmaco_ing_enfermeria) {
        this.farmaco_ing_enfermeria = farmaco_ing_enfermeria;
    }

    /**
     * @return the obs_ing_enfermeria
     */
    public String getObs_ing_enfermeria() {
        return obs_ing_enfermeria;
    }

    /**
     * @param obs_ing_enfermeria the obs_ing_enfermeria to set
     */
    public void setObs_ing_enfermeria(String obs_ing_enfermeria) {
        this.obs_ing_enfermeria = obs_ing_enfermeria;
    }

    /**
     * @return the id_examen_fisico_ing_enfermeria
     */
    public int getId_examen_fisico_ing_enfermeria() {
        return id_examen_fisico_ing_enfermeria;
    }

    /**
     * @param id_examen_fisico_ing_enfermeria the id_examen_fisico_ing_enfermeria to set
     */
    public void setId_examen_fisico_ing_enfermeria(int id_examen_fisico_ing_enfermeria) {
        this.id_examen_fisico_ing_enfermeria = id_examen_fisico_ing_enfermeria;
    }

    /**
     * @return the id_duo_ing_enfermeria
     */
    public int getId_duo_ing_enfermeria() {
        return id_duo_ing_enfermeria;
    }

    /**
     * @param id_duo_ing_enfermeria the id_duo_ing_enfermeria to set
     */
    public void setId_duo_ing_enfermeria(int id_duo_ing_enfermeria) {
        this.id_duo_ing_enfermeria = id_duo_ing_enfermeria;
    }

    /**
     * @return the otro_ex_docto_ing_enfermeria
     */
    public String getOtro_ex_docto_ing_enfermeria() {
        return otro_ex_docto_ing_enfermeria;
    }

    /**
     * @param otro_ex_docto_ing_enfermeria the otro_ex_docto_ing_enfermeria to set
     */
    public void setOtro_ex_docto_ing_enfermeria(String otro_ex_docto_ing_enfermeria) {
        this.otro_ex_docto_ing_enfermeria = otro_ex_docto_ing_enfermeria;
    }

    /**
     * @return the conciencia_ex_fisico
     */
    public String getConciencia_ex_fisico() {
        return conciencia_ex_fisico;
    }

    /**
     * @param conciencia_ex_fisico the conciencia_ex_fisico to set
     */
    public void setConciencia_ex_fisico(String conciencia_ex_fisico) {
        this.conciencia_ex_fisico = conciencia_ex_fisico;
    }

    /**
     * @return the cabeza_ex_fisico
     */
    public String getCabeza_ex_fisico() {
        return cabeza_ex_fisico;
    }

    /**
     * @param cabeza_ex_fisico the cabeza_ex_fisico to set
     */
    public void setCabeza_ex_fisico(String cabeza_ex_fisico) {
        this.cabeza_ex_fisico = cabeza_ex_fisico;
    }

    /**
     * @return the mucosa_ex_fisico
     */
    public String getMucosa_ex_fisico() {
        return mucosa_ex_fisico;
    }

    /**
     * @param mucosa_ex_fisico the mucosa_ex_fisico to set
     */
    public void setMucosa_ex_fisico(String mucosa_ex_fisico) {
        this.mucosa_ex_fisico = mucosa_ex_fisico;
    }

    /**
     * @return the torax_ex_fisico
     */
    public String getTorax_ex_fisico() {
        return torax_ex_fisico;
    }

    /**
     * @param torax_ex_fisico the torax_ex_fisico to set
     */
    public void setTorax_ex_fisico(String torax_ex_fisico) {
        this.torax_ex_fisico = torax_ex_fisico;
    }

    /**
     * @return the abdomen_ex_fisico
     */
    public String getAbdomen_ex_fisico() {
        return abdomen_ex_fisico;
    }

    /**
     * @param abdomen_ex_fisico the abdomen_ex_fisico to set
     */
    public void setAbdomen_ex_fisico(String abdomen_ex_fisico) {
        this.abdomen_ex_fisico = abdomen_ex_fisico;
    }

    /**
     * @return the eess_ex_fisico
     */
    public String getEess_ex_fisico() {
        return eess_ex_fisico;
    }

    /**
     * @param eess_ex_fisico the eess_ex_fisico to set
     */
    public void setEess_ex_fisico(String eess_ex_fisico) {
        this.eess_ex_fisico = eess_ex_fisico;
    }

    /**
     * @return the eeii_ex_fisico
     */
    public String getEeii_ex_fisico() {
        return eeii_ex_fisico;
    }

    /**
     * @param eeii_ex_fisico the eeii_ex_fisico to set
     */
    public void setEeii_ex_fisico(String eeii_ex_fisico) {
        this.eeii_ex_fisico = eeii_ex_fisico;
    }

    /**
     * @return the z_sacra_ex_fisico
     */
    public String getZ_sacra_ex_fisico() {
        return z_sacra_ex_fisico;
    }

    /**
     * @param z_sacra_ex_fisico the z_sacra_ex_fisico to set
     */
    public void setZ_sacra_ex_fisico(String z_sacra_ex_fisico) {
        this.z_sacra_ex_fisico = z_sacra_ex_fisico;
    }

    /**
     * @return the peso_ex_fisico
     */
    public String getPeso_ex_fisico() {
        return peso_ex_fisico;
    }

    /**
     * @param peso_ex_fisico the peso_ex_fisico to set
     */
    public void setPeso_ex_fisico(String peso_ex_fisico) {
        this.peso_ex_fisico = peso_ex_fisico;
    }

    /**
     * @return the talla_ex_fisico
     */
    public String getTalla_ex_fisico() {
        return talla_ex_fisico;
    }

    /**
     * @param talla_ex_fisico the talla_ex_fisico to set
     */
    public void setTalla_ex_fisico(String talla_ex_fisico) {
        this.talla_ex_fisico = talla_ex_fisico;
    }

    /**
     * @return the pulso_ex_fisico
     */
    public String getPulso_ex_fisico() {
        return pulso_ex_fisico;
    }

    /**
     * @param pulso_ex_fisico the pulso_ex_fisico to set
     */
    public void setPulso_ex_fisico(String pulso_ex_fisico) {
        this.pulso_ex_fisico = pulso_ex_fisico;
    }

    /**
     * @return the presion_a_ex_fisico
     */
    public String getPresion_a_ex_fisico() {
        return presion_a_ex_fisico;
    }

    /**
     * @param presion_a_ex_fisico the presion_a_ex_fisico to set
     */
    public void setPresion_a_ex_fisico(String presion_a_ex_fisico) {
        this.presion_a_ex_fisico = presion_a_ex_fisico;
    }

    /**
     * @return the temp_ex_fisico
     */
    public String getTemp_ex_fisico() {
        return temp_ex_fisico;
    }

    /**
     * @param temp_ex_fisico the temp_ex_fisico to set
     */
    public void setTemp_ex_fisico(String temp_ex_fisico) {
        this.temp_ex_fisico = temp_ex_fisico;
    }

    /**
     * @return the satura_ex_fisico
     */
    public String getSatura_ex_fisico() {
        return satura_ex_fisico;
    }

    /**
     * @param satura_ex_fisico the satura_ex_fisico to set
     */
    public void setSatura_ex_fisico(String satura_ex_fisico) {
        this.satura_ex_fisico = satura_ex_fisico;
    }

    /**
     * @return the vvp1_ex_fisico
     */
    public String getVvp1_ex_fisico() {
        return vvp1_ex_fisico;
    }

    /**
     * @param vvp1_ex_fisico the vvp1_ex_fisico to set
     */
    public void setVvp1_ex_fisico(String vvp1_ex_fisico) {
        this.vvp1_ex_fisico = vvp1_ex_fisico;
    }

    /**
     * @return the vvp2_ex_fisico
     */
    public String getVvp2_ex_fisico() {
        return vvp2_ex_fisico;
    }

    /**
     * @param vvp2_ex_fisico the vvp2_ex_fisico to set
     */
    public void setVvp2_ex_fisico(String vvp2_ex_fisico) {
        this.vvp2_ex_fisico = vvp2_ex_fisico;
    }

    /**
     * @return the vvc_ex_fisico
     */
    public String getVvc_ex_fisico() {
        return vvc_ex_fisico;
    }

    /**
     * @param vvc_ex_fisico the vvc_ex_fisico to set
     */
    public void setVvc_ex_fisico(String vvc_ex_fisico) {
        this.vvc_ex_fisico = vvc_ex_fisico;
    }

    /**
     * @return the sng_ex_fisico
     */
    public String getSng_ex_fisico() {
        return sng_ex_fisico;
    }

    /**
     * @param sng_ex_fisico the sng_ex_fisico to set
     */
    public void setSng_ex_fisico(String sng_ex_fisico) {
        this.sng_ex_fisico = sng_ex_fisico;
    }

    /**
     * @return the s_foley_ex_fisico
     */
    public String getS_foley_ex_fisico() {
        return s_foley_ex_fisico;
    }

    /**
     * @param s_foley_ex_fisico the s_foley_ex_fisico to set
     */
    public void setS_foley_ex_fisico(String s_foley_ex_fisico) {
        this.s_foley_ex_fisico = s_foley_ex_fisico;
    }

    /**
     * @return the dorso_lumbar_ex_fisico
     */
    public String getDorso_lumbar_ex_fisico() {
        return dorso_lumbar_ex_fisico;
    }

    /**
     * @param dorso_lumbar_ex_fisico the dorso_lumbar_ex_fisico to set
     */
    public void setDorso_lumbar_ex_fisico(String dorso_lumbar_ex_fisico) {
        this.dorso_lumbar_ex_fisico = dorso_lumbar_ex_fisico;
    }

    /**
     * @return the piel_ex_fisico
     */
    public String getPiel_ex_fisico() {
        return piel_ex_fisico;
    }

    /**
     * @param piel_ex_fisico the piel_ex_fisico to set
     */
    public void setPiel_ex_fisico(String piel_ex_fisico) {
        this.piel_ex_fisico = piel_ex_fisico;
    }
    
    

}
