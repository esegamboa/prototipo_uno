/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cDas;
import CapaNegocio.Negocio;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author EseGamboa
 */
public class ingreso_suam extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Negocio neg = new Negocio();
        HttpSession session1 = request.getSession();

        String obtiene_usuario = session1.getAttribute("usuario_rut") + "";

        String ip = request.getRemoteAddr(); //IP del cliente

        int modo = Integer.parseInt(request.getParameter("txt_modo"));

        switch (modo) {
            case 1:
                /*ingreso das*/
                int id_dau = Integer.parseInt(request.getParameter("txt_dau_id"));
                String paciente_rut = request.getParameter("txt_paciente");
                int derivador = Integer.parseInt(request.getParameter("cbo_derivador"));
                String medico = request.getParameter("cbo_medico");
                int camilla = Integer.parseInt(request.getParameter("cbo_camilla"));
                int tipo_dau = Integer.parseInt(request.getParameter("txt_dau_id_tipo"));
                cDas das = new cDas();

                if (tipo_dau == 1) {
                    // si es un dau normal setea 0 en dau nn
                    das.setDau_id(id_dau);
                    das.setDau_nn_id(0);
                    das.setRut_paciente(paciente_rut);
                } else  {
                    // si es un dau nn, setea 0 en dau, con la posibilidad de modificar este 0
                    das.setDau_nn_id(id_dau);
                    das.setDau_id(0);
                    das.setRut_paciente("");
                }

                das.setDerivador(derivador);
                das.setCamilla(camilla);
                das.setMedico(medico);
                das.setIp(ip);
                das.setUsuario(obtiene_usuario);

                neg.ingresa_das(das);
               
                response.sendRedirect(neg.getLocal() + "uh_visita_suam.jsp");
                /* ingreso das*/
                break;
            case 2:

                break;
            case 3:


                break;

        }





    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
