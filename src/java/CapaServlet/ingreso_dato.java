/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cDuo;
import CapaNegocio.NegocioQ;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author EseGamboa
 */
public class ingreso_dato extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int opcion = Integer.parseInt(request.getParameter("txt_modo"));

        HttpSession session1 = request.getSession();
        String obtiene_usuario = "" + request.getParameter("txt_rut_usuario");
        int estado = Integer.parseInt(String.valueOf(request.getParameter("estado")));
        int id_duo = Integer.parseInt(String.valueOf(request.getParameter("id_duo")));
        String duo_ip = request.getRemoteAddr(); //IP del cliente

        NegocioQ neg = new NegocioQ();
        cDuo duo = new cDuo();
        /****PRESTACIONES Y ENFERMEDADES****/
        if (request.getParameterValues("EnfCronH") != null) {
            String EnfCronH[] = request.getParameterValues("EnfCronH");
            for (int i = 0; i < EnfCronH.length; i++) {
                neg.ingresa_EnfCronica(id_duo, Integer.parseInt(EnfCronH[i]));
            }
        }
        try {
            /* 01 marzo 2012  */
            if (request.getParameterValues("PreDuo") != null) {
                String PreDuo[] = request.getParameterValues("PreDuo");
                for (int i = 0; i < PreDuo.length; i++) {
                    neg.ingresa_PrestacionesDuo(id_duo, Integer.parseInt(PreDuo[i]));
                }
            }
            /* 01 marzo 2012  */
        } catch (Exception ex) {
            out.write("Try Prestación--linea 65 ingreso_dato");
        }
        /****PRESTACIONES Y ENFERMEDADES****/
        switch (opcion) {
            case 1:
                /***************INGRESO ENFERMERIA****************/
                String otro_ex_docto_ing_enfermeria = request.getParameter("otro_ex_docto_ing_enfermeria");
                String dorso_lumbar_ex_fisico = request.getParameter("dorso_lumbar_ex_fisico");
                String piel_ex_fisico = request.getParameter("piel_ex_fisico");
                String morbilidades = request.getParameter("morbilidades");
                String farmacos = request.getParameter("farmacos");
                String observacion = request.getParameter("observacion");

                String conciencia = request.getParameter("conciencia");
                String cabeza = request.getParameter("cabeza");
                String mucoza = request.getParameter("mucoza");
                String torax = request.getParameter("torax");
                String abdomen = request.getParameter("abdomen");
                String eess = request.getParameter("eess");
                String eeii = request.getParameter("eeii");
                String zona = request.getParameter("zona");
                String sng = request.getParameter("sng");
                String sfoley = request.getParameter("sfoley");
                String peso = request.getParameter("peso");
                String talla = request.getParameter("talla");
                String pulso = request.getParameter("pulso");
                String presion = request.getParameter("presion");
                String temp = request.getParameter("temp");
                String sat = request.getParameter("sat");
                String vvp1 = request.getParameter("vvp1");
                String vvp2 = request.getParameter("vvp2");
                String vvc = request.getParameter("vvc");
                /* INGRESO DOCUMENTOS*/

                if (!obtiene_usuario.equalsIgnoreCase("null")) {

                    if (request.getParameterValues("doctosAdjuntos") != null) {
                        String doctosAdjuntos[] = request.getParameterValues("doctosAdjuntos");
                        for (int i = 0; i < doctosAdjuntos.length; i++) {
                            neg.ingresa_DoctoAdjunto(id_duo, Integer.parseInt(doctosAdjuntos[i]));
                            // out.println("<h1>"+Integer.parseInt(doctosAdjuntos[i])+"</h1>");
                        }
                    }

                    /* FIN INGRESO DOCUMENTOS*/
                    // ingreso examen fisico que se asocia al ingreso de enfermeria
                    int id_ex_fisico = neg.ingresa_ExamenFisico(conciencia, cabeza, mucoza, torax, abdomen, eess, eeii, zona, sng, sfoley, peso, talla, pulso, presion, temp, sat, vvp1, vvp2, vvc, dorso_lumbar_ex_fisico, piel_ex_fisico);
                    // ingreso ingreso enfermeria con los datos recopilados
                    neg.ingresa_ingreso_enfermeria(morbilidades, farmacos, observacion, obtiene_usuario, id_ex_fisico, id_duo, otro_ex_docto_ing_enfermeria);
                    /*seteo datos para el insert*/
                    duo.setIp_ing_enf(duo_ip);
                    if (estado == 2) {
                        cDuo duox = neg.obtiene_duo_liviano(id_duo);
                        if (duox.getEstado_duo() == 21) {
                            duo.setEstado_duo(21);
                        } else {
                            duo.setEstado_duo(estado);
                        }

                    } else {
                        duo.setEstado_duo(estado);
                    }

                    duo.setId_duo(id_duo);
                    // modifico datos del registro de la tabla duo
                    neg.modifica_duo_x_enfermeria(duo);
                    response.sendRedirect(neg.getLocal() + "uh_visita.jsp");

                } else {

                    out.write("<script> alert('Su sesion ha expirado'); ");
                    out.write(" window.location = '" + neg.getLocal() + "cierra_sesion' ");
                    out.write("</script>");
                }
                /***************FIN INGRESO ENFERMERIA****************/
                break;
            case 2:
                /***************INGRESO MEDICO****************/
                // out.write("PASO 117<BR>");
                String anamnesis = request.getParameter("anamnesis").replace("'", "");
                //  int categorizacion_entrada = Integer.parseInt(request.getParameter("id_categorizado"));
                int consultorio_pertenencia = Integer.parseInt(request.getParameter("cbo_consultorio_pertenencia"));
                duo.setConsultorio(consultorio_pertenencia);
                /**/
                String rut_paciente=request.getParameter("txt_rut").toUpperCase();
                neg.modifica_paciente_datos(rut_paciente, consultorio_pertenencia);
                /**/

                duo.setCategorizacion_id(0); // debe ser 0 ya q hay una clave foranea desde DUO  parar categorizacion
                /*seteo datos para el insert*/
                duo.setAnamnesis_duo(anamnesis);
                // duo.setCategorizacion_id(categorizacion_entrada);
                duo.setIp_ing_med(duo_ip);
                //   duo.setEstado_duo(estado);
                duo.setEstado_duo(21);
                duo.setId_duo(id_duo);

                /* 28-02-2013, validacion de sesion de medico*/
                if (!obtiene_usuario.equalsIgnoreCase("null")) {
                    duo.setRut_usuario_ing_med(obtiene_usuario);
                    neg.modifica_duo_x_medico(duo);
                } else {
                    out.write("<script> alert('Su sesion ha expirado'); ");
                    out.write(" window.location = '" + neg.getLocal() + "cierra_sesion' ");
                    out.write("</script>");
                }
                /* FIN 28-02-2013, validacion de sesion de medico*/
                //out.write("PASO 127<BR>");
                response.sendRedirect(neg.getLocal() + "uh_visita.jsp");
                /***************FIN INGRESO MEDICO****************/
                break;
            case 3:
                /***************  ****************/
                /***************  ****************/
                break;
        }

        out.write("El sistema no pudo redireccionar a la pagina siguiente...<a href='uh_visita.jsp'>IR A VISITA...</a>");

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
