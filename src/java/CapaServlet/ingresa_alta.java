/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cDuo;
import CapaDato.cEpicrisis;
import CapaNegocio.NegocioQ;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author EseGamboa
 */
public class ingresa_alta extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int opcion = Integer.parseInt(request.getParameter("txt_modo"));

        HttpSession session1 = request.getSession();
        String obtiene_usuario = "" + request.getParameter("txt_rut_usuario");
        int id_duo = Integer.parseInt(String.valueOf(request.getParameter("id_duo")));
        String ip = request.getRemoteAddr(); //IP del cliente
        NegocioQ neg = new NegocioQ();
        cDuo duo = new cDuo();

        switch (opcion) {
            case 1:
                // ingresa epicrisis
                // solo modifico estado del duo,doctor e ip de med
                String fecha_dma = request.getParameter("fecha_epi");

                String dia = fecha_dma.substring(0, 2);
                String mes = fecha_dma.substring(3, 5);
                String año = fecha_dma.substring(6, 10);

                String fecha_mda = mes + "-" + dia + "-" + año;
                out.write("" + fecha_mda + "<br>");
                String hora = request.getParameter("fecha_epi");
                hora = hora.substring(hora.length() - 8, hora.length());
                out.write("" + hora);
                String resumen = request.getParameter("resumen").replace("'", "");
                String examenes = request.getParameter("examenes").replace("'", "");
                String diagnosticos = request.getParameter("diagnosticos").replace("'", "");
                String indicaciones = request.getParameter("indicaciones").replace("'", "");

                cEpicrisis epi = new cEpicrisis();
                epi.setFecha_epicrisis(fecha_mda);
                epi.setHora_epicrisis(hora);
                epi.setResumen_epicrisis(resumen);
                epi.setExamen_epicrisis(examenes);
                epi.setDiagnostico_epicrisis(diagnosticos);
                epi.setIndicacion_epicrisis(indicaciones);

                epi.setRut_usuario(obtiene_usuario);
                epi.setIp_epicrisis(ip);
                epi.setId_duo(id_duo);
                if (!obtiene_usuario.equalsIgnoreCase("null")) {
                    neg.ingresa_epicrisis(epi);
                    neg.modifica_estado_duo(id_duo, 3);
                }else{
                out.write("<script>alert('No se pudo completar esta operación porque su sesion ya habia caducado')</script>");
                }
                response.sendRedirect(neg.getLocal() + "uh_visita.jsp");

                break;
            case 2:
                // modifico duo en fecha hora egreso medico
                neg.modifica_valida_epicrisis(id_duo);
                response.sendRedirect(neg.getLocal() + "uh_visita.jsp");

                break;
            case 3:
                // ingreso alta medica

                int id_destino = Integer.parseInt(request.getParameter("id_destino"));
                fecha_dma = request.getParameter("fecha_epi");
                dia = fecha_dma.substring(0, 2);
                mes = fecha_dma.substring(3, 5);
                año = fecha_dma.substring(6, 10);
                fecha_mda = mes + "-" + dia + "-" + año;
                hora = request.getParameter("fecha_epi");
                hora = hora.substring(hora.length() - 8, hora.length());
                String obs = request.getParameter("obs");
                //  modifico estado duo,y fech alta administrativa
                neg.ingresa_alta_adm(obs, fecha_mda, hora, obtiene_usuario, id_duo, id_destino, ip);
                neg.modifica_estado_duo_alta_administrativa(id_duo, 4);

//                out.write("<br><br>Se ha Realizado el Alta Médica del paciente<br>La cama ha quedado liberada.");
//                out.write("<a href='"+neg.getLocal()+"uh_visita.jsp'>Volver al Sistema y continuar</a>");
                response.sendRedirect(neg.getLocal() + "uh_visita.jsp");
                break;
        }




    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
