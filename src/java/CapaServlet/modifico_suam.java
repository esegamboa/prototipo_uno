/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cAlta_Das;
import CapaDato.cDas;
import CapaNegocio.NegocioQ;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author EseGamboa
 */
public class modifico_suam extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        NegocioQ neg = new NegocioQ();
        int modo = 0;
        try {
            modo = Integer.parseInt(request.getParameter("txt_modo"));
        } catch (NumberFormatException exn) {
        }
        HttpSession session1 = request.getSession();
        //       if ( == null)
        String obtiene_usuario = session1.getAttribute("usuario_rut") + "";
        String ip = request.getRemoteAddr(); //IP del cliente

        switch (modo) {
            case 1:
                /**************EGRESO FINAL PACIENTE**************/
                cAlta_Das alta = new cAlta_Das();
                int destino = Integer.parseInt(request.getParameter("cbo_destino"));
                String observaciones = ""+request.getParameter("txa_observacion").trim();
                int id_das = Integer.parseInt(request.getParameter("txt_das"));
                String rut_medico=request.getParameter("cbo_medico");

                alta.setId_das(id_das);
                alta.setRut_usuario(obtiene_usuario);
                alta.setDetalle(observaciones);
                alta.setDestino(destino);
                alta.setIp(ip);
                alta.setMedico_rut(rut_medico);
                neg.ingresa_alta_das(alta);
// cambia el estado del das a 4 (egresado).-
                neg.modifica_estado_das(id_das, 4);
                /**************FIN EGRESO FINAL PACIENTE**************/
                response.sendRedirect("" + neg.getLocal() + "uh_visita_suam.jsp");
                break;

            case 2:
                /**************MODIFICA PACIENTE ENTRE CAMILLAS**************/
                // out.write("caso 2 <br>");
                int modo_cama = Integer.parseInt(request.getParameter("txt_modo_camilla"));
                int das_actual = Integer.parseInt(request.getParameter("txt_das_actual"));
                int cama_actual = Integer.parseInt(request.getParameter("txt_camilla_actual"));
                int cama_nueva = Integer.parseInt(request.getParameter("cbo_camilla_seleccionada"));// obtiene el numero de duo si el modo es 2
//out.write(""+cama_nueva);
                if (modo_cama == 1) {
                    neg.modifica_camilla(das_actual, cama_nueva);
                } else if (modo_cama == 2) {
                    cDas das = neg.obtiene_das(cama_nueva);
//                    out.write("<br>" + duo.getCama());
                    // si el caso es 2, la cama nueva es el numero de das
                    neg.modifica_camilla(cama_nueva, cama_actual); //EL PACIENTE QUE ES CAMBIADO DE CAMA
                    neg.modifica_camilla(das_actual, das.getCamilla()); //EL PACIENTE ACTUAL
                }

//                out.write("-|" + modo_cama + "|<br>");
//                out.write("-|" + das_actual + "|<br>");
//                out.write("-|" + cama_actual + "|<br>");
//
//                out.write("-|" + cama_nueva + "|");
                response.sendRedirect("" + neg.getLocal() + "uh_visita_suam.jsp");
                /**************FIN MODIFICA PACIENTE ENTRE CAMILLAS**************/
                break;

            case 3:
                /**************EGRESO FINAL PACIENTE**************/
                cDas indica = new cDas();

                int destino_indica = Integer.parseInt(request.getParameter("cbo_destino"));
                int id_das_indaca = Integer.parseInt(request.getParameter("txt_das"));
                indica.setIndicacion_destino_id(destino_indica);
                indica.setIndicacion_ip(ip);
                indica.setIndicacion_usuario_rut(obtiene_usuario);
                indica.setId_das(id_das_indaca);

                out.write(destino_indica + " destino");

                out.write("UPDATE  schema_suam.das  SET  "
                        + " das_estado = '2', "
                        + " das_indicacion_destino = '" + indica.getIndicacion_destino_id() + "', "
                        + " das_indicacion_usuario = '" + indica.getIndicacion_usuario_rut() + "', "
                        + " das_indicacion_fecha_ingreso = CURRENT_TIMESTAMP, "
                        + " das_indicacion_ip = '" + indica.getIndicacion_ip() + "' "
                        + " WHERE  das_id = '" + indica.getId_das() + "' ");

                neg.modifica_das_x_indicacion_alta(indica);
                /**************FIN EGRESO FINAL PACIENTE**************/
                response.sendRedirect("" + neg.getLocal() + "uh_visita_suam.jsp");
                break;

            case 97:


                break;

            case 98:

                break;

            case 99:
                /**/

                /**/
                break;

        }





    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
