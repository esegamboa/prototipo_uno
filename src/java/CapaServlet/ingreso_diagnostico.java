/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cDiagnostico;
import CapaNegocio.NegocioQ;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author EseGamboa
 */
public class ingreso_diagnostico extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String modo = request.getParameter("modo");
        //modo 1 ingresa
        //modo 2 elimina
        String descripcion_diagnostico = request.getParameter("diagnostico");
        int id_duo = 0;
        int tipo_diagnostico = 0;
        int id_diagnostico = 0;

        try {
            tipo_diagnostico = Integer.parseInt(request.getParameter("tipo_diagnostico_duo"));
        } catch (Exception ex) {
        }

        try {
            id_duo = Integer.parseInt(request.getParameter("id_duo"));
        } catch (Exception ex) {
        }

        try {
            id_diagnostico = Integer.parseInt(request.getParameter("id_diagnostico_duo"));
        } catch (Exception ex) {
        }

        NegocioQ neg = new NegocioQ();

        cDiagnostico diag = new cDiagnostico();
        diag.setDescripcion_diagnostico(descripcion_diagnostico);
        diag.setId_diagnostico(id_diagnostico);
        diag.setId_duo(id_duo);
        diag.setTipo_diagnostico(tipo_diagnostico);

        if (modo.equals("1")) {
            neg.ingresa_diagnostico_duo(diag);
        } else if (modo.equals("2")) {
           neg.elimina_diagnostico_duo(diag.getId_diagnostico());
        }

        ArrayList lista_diagnostico = neg.lista_diagnostico(diag.getId_duo(), diag.getTipo_diagnostico() + "");
        Iterator it = lista_diagnostico.iterator();

        out.write(" <table width=\"700\"><tr> ");
        out.write(" <th class=\"DATOS\">N°</th>");
        out.write(" <th class=\"DATOS\">Descripción Diagnóstico</th>");
        out.write(" <th class=\"DATOS\">Eli</th> ");
        out.write(" </tr>");

        int contador = 1;
        while (it.hasNext()) {
            cDiagnostico aux = (cDiagnostico) it.next();
            out.write("<tr>");
            out.write("<td class=\"DATOS\">" + contador + " </td> ");
            out.write("<td class=\"DATOS\">" + aux.getDescripcion_diagnostico() + "  </td>");
            out.write("<td class=\"DATOS\"> <img src=\"../Iconos/action_stop.gif\""
                    + " onclick=\"EliminaDiag(" + aux.getId_diagnostico() + ")\" "
                    + "style=\"cursor:pointer\"> </td>");
            out.write(" </tr> ");
            contador++;
        }

        out.write("  </table>");


    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
