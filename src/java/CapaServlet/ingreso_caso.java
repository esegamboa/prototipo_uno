/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaServlet;

import CapaDato.cEvaNeurologia;
import CapaDato.cEvaTraumatologia;
import CapaDato.cPaciente;
import CapaDato.cRegistroSeguimiento;
import CapaDato.cRegistroSocial;
import CapaDato.cSesionKine;
import CapaNegocio.Negocio;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Informatica
 */
public class ingreso_caso extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*para que no se recarque un ingreso*/
        response.setHeader("Pragma", "no-cache");
        response.addHeader("Cache-Control", "no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "private");
        response.setDateHeader("Expires", -1);
        /*FIN para que no se recarque un ingreso*/

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        int opcion = Integer.parseInt(request.getParameter("txt_modo"));

        HttpSession session1 = request.getSession();
        String obtiene_usuario = "" + request.getParameter("txt_usuario");
        String ip = request.getRemoteAddr(); //IP del cliente
        Negocio neg = new Negocio();
        String mensaje = "";
        switch (opcion) {
            case 1:
// eva respiratoria

                break;
            case 2:
// eva neurologica
                cEvaNeurologia neu = new cEvaNeurologia();

                neu.setLesion_evaluada(Integer.parseInt(request.getParameter("cbo2_lesion_evaluada")));
                neu.setId_duo(Integer.parseInt(request.getParameter("txt_duo")));
                neu.setRut_usuario(obtiene_usuario);

                if (neu.getLesion_evaluada() == 1) {

                    neu.setLesion(Integer.parseInt(request.getParameter("cbo2_lesion_medular")));
                    neu.setAsia(request.getParameter("cbo2_asia"));
                    neu.setLesion_tipo(Integer.parseInt(request.getParameter("cbo2_tipo_lesion")));

                    neu.setAshworth(request.getParameter("cbo2_ashworth_medular"));

                    neu.setReflejo_osteorendineo_desc(request.getParameter("txt2_reflejo_osteorendineo"));
                    neu.setEvaluacion_sensitiva(request.getParameter("txt2_evaluacion_sensitiva"));
                    neu.setSensibilidad(request.getParameter("txt2_sensibilidad"));
                    neu.setMotor_index(request.getParameter("txt2_motor_index"));
                    neu.setContraccion(request.getParameter("txt2_contraccion"));

                    neu.setSilla_ruedas(request.getParameter("txt2_silla_rueda"));
                    neu.setMarcha(request.getParameter("txt2_marcha_medular"));
                    neu.setNivel_motor(request.getParameter("txt2_nivel_motor"));
                    neu.setNivel_sentivo(request.getParameter("txt2_nivel_sentivo"));
                    neu.setNivel_neurologico(request.getParameter("txt2_nivel_neurologico"));
                    /* tabla adicional */
                    neu.setOp1(Integer.parseInt(request.getParameter("rbt2_giro")));
                    neu.setOp1_desc(request.getParameter("txt2_giro"));

                    neu.setOp2(Integer.parseInt(request.getParameter("rbt2_supino_decubito_lateral")));
                    neu.setOp2_desc(request.getParameter("txt2_supino_decubito_lateral"));

                    neu.setOp3(Integer.parseInt(request.getParameter("rbt2_supino_prono")));
                    neu.setOp3_desc(request.getParameter("txt2_supino_prono"));

                    neu.setOp4(Integer.parseInt(request.getParameter("rbt2_decubito_lateral_sedente")));
                    neu.setOp4_desc(request.getParameter("txt2_decubito_lateral_sedente"));

                    neu.setOp5(Integer.parseInt(request.getParameter("rbt2_supino_sedente")));
                    neu.setOp5_desc(request.getParameter("txt2_supino_sedente"));

                    neu.setOp6(Integer.parseInt(request.getParameter("rbt2_sedente_bipedo")));
                    neu.setOp6_desc(request.getParameter("txt2_sedente_bipedo"));

                } else {

                    /* fin tabla adicional */ //otro  //    
                    neu.setLesion(Integer.parseInt(request.getParameter("cbo2_lesion_otro")));
                    neu.setMotoneurona(request.getParameter("txt2_motoneurona"));
                    neu.setExtrapiramiral(request.getParameter("txt2_extrapiramidal"));

                    neu.setPostura(request.getParameter("txt2_postura"));
                    neu.setAshworth(request.getParameter("cbo2_ashworth_otro"));

                    neu.setFuerza(request.getParameter("txt2_fuerza"));
                    neu.setTono_muscular(request.getParameter("txt2_tono_muscular"));
                    neu.setTrofismo(Integer.parseInt(request.getParameter("cbo2_trofismo")));
                    neu.setTrofismo_adicional(request.getParameter("txt2_trofismo_adicional"));
                    neu.setEess(request.getParameter("txt2_eess"));
                    neu.setEeii(request.getParameter("txt2_eeii"));
                    neu.setReflejo_osteorendineo(Integer.parseInt(request.getParameter("cbo2_reflejo_osteotendineo")));
                    neu.setPropiocepcion(Integer.parseInt(request.getParameter("cbo2_propiocepcion")));
                    neu.setPropiocepcion_adicional(request.getParameter("txt2_propiocepcion_adicional"));

                    neu.setTransicion(request.getParameter("txt2_transicion"));
                    neu.setReaccion_equilibrio(Integer.parseInt(request.getParameter("cbo2_reaccion_equilibrio")));
                    neu.setReaccion_enderezamiento(Integer.parseInt(request.getParameter("cbo2_reaccion_enderezamiento")));
                    neu.setReaccion_apoyo(Integer.parseInt(request.getParameter("cbo2_reaccion_apoyo")));

                    neu.setMarcha(request.getParameter("txt2_marcha_otro"));
                    neu.setTest_especial(request.getParameter("txt2_test_especial"));

                    neu.setOp1_desc(request.getParameter("txt2_hta"));
                    neu.setOp2_desc(request.getParameter("txt2_dm"));
                    neu.setOp3_desc(request.getParameter("txt2_dislipidemia"));
                    neu.setOp4_desc(request.getParameter("txt2_tabaquismo"));

                    if (request.getParameter("chk2_hta") != null) {
                        neu.setOp1(1);
                    } else {
                        neu.setOp1(0);
                    }

                    if (request.getParameter("chk2_dm") != null) {
                        neu.setOp2(1);
                    } else {
                        neu.setOp2(0);
                    }

                    if (request.getParameter("chk2_dislipidemia") != null) {
                        neu.setOp3(1);
                    } else {
                        neu.setOp3(0);
                    }

                    if (request.getParameter("chk2_tabaquismo") != null) {
                        neu.setOp4(1);
                    } else {
                        neu.setOp4(0);
                    }

                }

                if (neg.ingresa_evaluacion_neurologica(neu)) {
                    out.write("se ingreso");
                    response.sendRedirect(neg.getLocal() + "uh_visita.jsp");
                } else {
                    out.write("No se ingreso");
                }

                break;
            case 3:
// eva traumatologica
                cEvaTraumatologia tra = new cEvaTraumatologia();
                tra.setObservacion_inicial(request.getParameter("txa3_observacion_inicial"));
                tra.setHistorial_usuario(request.getParameter("txa3_historial"));
                tra.setDolor(request.getParameter("txa3_dolor"));
                tra.setPrueba_especial(request.getParameter("txa3_prueba_especial"));
                tra.setPalpacion(request.getParameter("txa3_palpacion"));
                tra.setMarcha(request.getParameter("txt3_marcha"));

                tra.setPlano_frontal(request.getParameter("txa3_plano_frontal"));
                tra.setPlano_sagital(request.getParameter("txa3_plano_sagital"));
                tra.setPlano_posterior(request.getParameter("txa3_plano_posterior"));

                tra.setMovimiento_pasivo(request.getParameter("txa3_movimiento_pasivo"));
                tra.setMovimiento_activo(request.getParameter("txa3_movimiento_activo"));
                tra.setObservacion(request.getParameter("txa3_observacion"));

                tra.setDermatoma(request.getParameter("txa3_dermatoma"));
                tra.setMiotoma(request.getParameter("txa3_miotoma"));
                tra.setReflejo_osteotendineo(request.getParameter("txa3_reflejo_osteotendineo"));
                tra.setTest_neurodinamico(request.getParameter("txa3_test_neurodinamico"));

                tra.setDiagnostico_imagen(request.getParameter("txa3_diagnostico_imagen"));
                tra.setDiagnostico_kinesico(request.getParameter("txa3_diagnostico_kinesico"));
                tra.setId_duo(Integer.parseInt(request.getParameter("txt_duo")));
                tra.setRut_usuario(obtiene_usuario);

                if (neg.ingresa_evaluacion_traumatologica(tra)) {
                    mensaje = "Se registraron los datos";
                    response.sendRedirect(neg.getLocal() + "uh_visita.jsp");
                } else {
                    mensaje = "Error. No se pudo ingresar elr egistro de traumatologia";
                }

                break;
            case 4:
//ingreso sesion kine
             /*en sesion_kine_carga.jsp */

                break;
            /*elimina*/
            case 11:
//
                break;
            case 12:

                break;
            case 13:

                break;
            case 14:
//elimina sesion kine
                break;

            case 21:
                // ingreso registro social

                cRegistroSocial reg = new cRegistroSocial();
                reg.setId_duo(Integer.parseInt(request.getParameter("txt_duo")));

                reg.setInstitucionalizado(Integer.parseInt(request.getParameter("cbo_institucionalizado")));
                if (reg.getInstitucionalizado() == 1) {
                    reg.setInstitucion_nombre(request.getParameter("txt_institucion"));
                }

                reg.setEstado_civil(Integer.parseInt(request.getParameter("cbo_estado_civil")));
                reg.setSituacion_laboral(Integer.parseInt(request.getParameter("cbo_situacion_laboral")));
                reg.setPlan(request.getParameter("txa_plan"));

                reg.setSituacion(request.getParameter("txa_situacion"));

                reg.setVive(Integer.parseInt(request.getParameter("cbo_vive")));
                reg.setHijos(Integer.parseInt(request.getParameter("cbo_hijo")));
                if (reg.getHijos() == 1) {
                    reg.setHijos_cantidad(Integer.parseInt(request.getParameter("txt_cantidad")));
                }

                reg.setRut_usuario(obtiene_usuario);
                neg.ingresa_registro_social(reg);
                /*07112014*/
                int id_consultorio = Integer.parseInt(request.getParameter("cbo_consultorio"));
                String rut_paciente = request.getParameter("txt_paciente_rut");
                neg.modifica_paciente_datos(rut_paciente, id_consultorio);
                /*07112014*/

                response.sendRedirect(neg.getLocal() + "asistencia_social/social_ingreso.jsp?txt_duo=" + reg.getId_duo());

                break;
            case 22:
                // modificacion registro
                reg = new cRegistroSocial();
                reg.setId_registro(Integer.parseInt(request.getParameter("txt_id_registro")));

                reg.setId_duo(Integer.parseInt(request.getParameter("txt_duo")));

                reg.setInstitucionalizado(Integer.parseInt(request.getParameter("cbo_institucionalizado")));
                if (reg.getInstitucionalizado() == 1) {
                    reg.setInstitucion_nombre(request.getParameter("txt_institucion"));
                }

                reg.setEstado_civil(Integer.parseInt(request.getParameter("cbo_estado_civil")));
                reg.setSituacion_laboral(Integer.parseInt(request.getParameter("cbo_situacion_laboral")));
                reg.setPlan(request.getParameter("txa_plan"));

                reg.setSituacion(request.getParameter("txa_situacion"));

                reg.setVive(Integer.parseInt(request.getParameter("cbo_vive")));
                reg.setHijos(Integer.parseInt(request.getParameter("cbo_hijo")));
                if (reg.getHijos() == 1) {
                    reg.setHijos_cantidad(Integer.parseInt(request.getParameter("txt_cantidad")));
                }
                reg.setRut_usuario(obtiene_usuario);
                neg.modifica_registro_social(reg);

                /*07112014*/
                 id_consultorio = Integer.parseInt(request.getParameter("cbo_consultorio"));
                 rut_paciente = request.getParameter("txt_paciente_rut");
                neg.modifica_paciente_datos(rut_paciente, id_consultorio);
                /*07112014*/

                response.sendRedirect(neg.getLocal() + "asistencia_social/social_ingreso.jsp?txt_duo=" + reg.getId_duo());

                break;
            case 23:
                // anulacion registro
                break;
            case 24:
                //ingresa contacto

                // se ingresa en lista_contacto.jsp
//                         cPaciente pac = new cPaciente();
//                pac.setTelefono1(request.getParameter("txt_contacto_fono1"));
//                pac.setTelefono2(request.getParameter("txt_contacto_fono2"));
//                pac.setNombres_paciente(request.getParameter("txt_contacto_nombre"));
//                pac.setParentesco_desc(request.getParameter("cbo_contacto_parentesco"));
//                pac.setDireccion(request.getParameter("txt_contacto_direccion"));
//                pac.setComuna_codigo(Integer.parseInt(request.getParameter("cbo_contacto_comuna")));
//                
//                neg.ingresa_contacto(pac);
//                out.write("Se ingreso");
                break;
            case 25:
                // anulacion contacto

                break;
            case 26:
                // ingresa seguimiento

//                       cRegistroSeguimiento seg= new cRegistroSeguimiento();
//                       seg.setId_registro_id(Integer.parseInt(request.getParameter("")));
//                       seg.setFecha_seguimiento(request.getParameter(""));
//                       seg.setDescripcion(request.getParameter("txa_descripcion"));
//                       seg.setRut_usuario(obtiene_usuario);
                break;
            case 27:
                // anula seguimiento

                break;

        }

        out.append(mensaje);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
