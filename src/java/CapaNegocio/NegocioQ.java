/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaDato.cAlta_Das;
import CapaDato.cCama;
import CapaDato.cCatRiesgoDependencia;
import CapaDato.cCategorizacion;
import CapaDato.cComuna;
import CapaDato.cConsultorio;
import CapaDato.cContacto;
import CapaDato.cDas;
import CapaDato.cDau;
import CapaDato.cDiagnostico;
import CapaDato.cDocumento;
import CapaDato.cDuo;
import CapaDato.cEnfermedad;
import CapaDato.cEpicrisis;
import CapaDato.cEvaNeurologia;
import CapaDato.cEvaRespiratoria;
import CapaDato.cEvaTraumatologia;
import CapaDato.cExamen;
import CapaDato.cHistorial_Consultorio;
import CapaDato.cHito;
import CapaDato.cIngresoEnfermeria;
import CapaDato.cNacion;
import CapaDato.cObservacion;
import CapaDato.cPaciente;
import CapaDato.cPerfil;
import CapaDato.cPueblo;
import CapaDato.cRegistroSeguimiento;
import CapaDato.cRegistroSocial;
import CapaDato.cSesionKine;
import CapaDato.cUsuario;
import CapaDato.cVisita;
import CapaDato.cVistaUoce;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EseGamboa
 */
public class NegocioQ extends Negocio {

    public ArrayList lista_camas_actuales() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT duo.id_duo, "
                + "   p.paciente_rut, p.paciente_nombres, p.paciente_apellidop, p.paciente_apellidom,"
                + " p.paciente_fecha_nac, age(duo.fecha_duo, p.paciente_fecha_naagec) AS paciente_edad, "
                + "   cf.codigo_fonasa_descripcion, pp.tramo_prevision_paciente, de.descripcion_derivador, "
                + "   CASE "
                + "     WHEN duo.estado_duo = 1 THEN 'Ingresado Adm.'  "
                + "    WHEN duo.estado_duo = 2 THEN 'En Cama con Ingreso Enf. '  "
                + "     WHEN duo.estado_duo = 21 THEN 'En Cama con Ingreso Med.'  "
                + "     WHEN duo.estado_duo = 3 THEN 'En Cama con Alta Medica'  "
                + "     WHEN duo.estado_duo = 4 THEN 'Alta Administrativa'  "
                + "     ELSE NULL "
                + "   END AS estado, duo.estado_duo,  duo.fecha_duo + duo.hora_duo AS fecha_duo, "
                + "   e.fecha_epicrisis + e.hora_epicrisis AS fecha_epicrisis, "
                + "   age(e.fecha_epicrisis + e.hora_epicrisis, duo.fecha_duo + duo.hora_duo) "
                + "    AS qdias_epi_duo, "
                + "   age(timenow() , datetime_pl(duo.fecha_duo, "
                + "    duo.hora_duo)) AS hasta_hoy, "
                + "   duo.id_cama, duo.duo_tiene_enfermeria "
                + " FROM schema_urgencia.paciente p, "
                + "  schema_uo.derivador de, "
                + "  schema_urgencia.prevision_paciente pp, "
                + "  schema_urgencia.codigo_fonasa cf, "
                + "  schema_uo.duo duo "
                + "  LEFT JOIN schema_uo.epicrisis e ON e.id_duo = duo.id_duo "
                + " WHERE p.paciente_rut = duo.rut_paciente AND "
                + "   duo.id_derivador = de.id_derivador AND "
                + "   duo.id_prevision = pp.id_prevision_paciente AND "
                + "   pp.codigo_fonasa_prevision_paciente = cf.codigo_fonasa_id AND "
                + "   (duo.estado_duo = ANY (ARRAY [ 1, 2, 21, 3 ])) "
                + " ORDER BY duo.id_duo  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDuo duo = new cDuo();
                duo.setCama(cnn.getRst().getInt("id_cama"));

                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));

                String hasta_hoy = cnn.getRst().getString("hasta_hoy");
                hasta_hoy = hasta_hoy.replace("days", "dias");
                hasta_hoy = hasta_hoy.replace("mons", "meses");
                hasta_hoy = hasta_hoy.replace("years", "años");
                hasta_hoy = hasta_hoy.replace("day", "dia");
                hasta_hoy = hasta_hoy.replace("mon", "mes");
                hasta_hoy = hasta_hoy.replace("year", "año");

                duo.setDias_hasta_hoy(hasta_hoy);
                // hasta_hoy
                duo.setRut_paciente(cnn.getRst().getString("paciente_rut"));
                duo.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                duo.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                duo.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                duo.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));

                String edad = cnn.getRst().getString("paciente_edad");
                edad = edad.replace("days", "dias.z");
                edad = edad.replace("mons", "meses");
                edad = edad.replace("years", "años");
                edad = edad.replace("day", "dia.z");
                edad = edad.replace("mon", "mes");
                edad = edad.replace("year", "año");
                duo.setEdad(edad);

                try {
                    edad = edad.substring(0, edad.indexOf("z") - 1);
                    duo.setEdad(edad);
                } catch (Exception exx) {
                }

                duo.setDerivador_descripcion(cnn.getRst().getString("descripcion_derivador"));
                duo.setEstado_duo(cnn.getRst().getInt("estado_duo"));
                duo.setEstado_duo_descripcion(cnn.getRst().getString("estado"));

                duo.setTramo_prevision(cnn.getRst().getString("tramo_prevision_paciente"));
                duo.setCodigo_fonasa_descripcion(cnn.getRst().getString("codigo_fonasa_descripcion"));

                duo.setFecha_hora_alta_med_duo(cnn.getRst().getString("fecha_epicrisis"));

                String qdias_epi_duo = cnn.getRst().getString("qdias_epi_duo");
                qdias_epi_duo = qdias_epi_duo.replace("days", "dias");
                qdias_epi_duo = qdias_epi_duo.replace("mons", "meses");
                qdias_epi_duo = qdias_epi_duo.replace("years", "años");
                qdias_epi_duo = qdias_epi_duo.replace("day", "dia");
                qdias_epi_duo = qdias_epi_duo.replace("mon", "mes");
                qdias_epi_duo = qdias_epi_duo.replace("year", "año");
                duo.setDias_epi_duo(qdias_epi_duo);

                lista.add(duo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public int lista_duos_hoy(String fecha_hoy, int modo) {
        // fecha_duo+hora_duo
        ArrayList lista = new ArrayList();
        int total = 0;
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        if (modo == 1) {
            this.cnn.setSentenciaSQL("select count(*) as suma  from schema_uo.duo D where D.fecha_duo=CURRENT_DATE and estado_duo NOT IN (99)");
        } else {
            this.cnn.setSentenciaSQL("select  count(*) as suma   from schema_uo.epicrisis E where E.fecha_epicrisis=CURRENT_DATE");
        }

        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                cDuo duo = new cDuo();
                // duo.setId_duo(cnn.getRst().getInt("id_duo"));
                total = cnn.getRst().getInt("suma");
//                duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
//                duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                //  lista.add(duo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
//        return lista;
        return total;
    }

    public String getCatRiesgo(int valor_riesgo) {
        String letra = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from schema_uo.cat_riesgo CR where " + valor_riesgo + " "
                + "between CR.desde_cat_riesgo and CR.hasta_cat_riesgo");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                letra = cnn.getRst().getString("resultado_cat_riesgo");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return (letra);
    }

    public String getCatDependencia(int valor_dependencia) {
        String numero = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from schema_uo.cat_dependencia CD where "
                + "" + valor_dependencia + " between CD.desde_cat_dependencia and CD.hasta_cat_dependencia");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                numero = cnn.getRst().getString("resultado_cat_dependencia");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return (numero);
    }

    public ArrayList lista_historial_visita_enfermeria(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select cat_visita_categorizacion,to_char(fecha_visita,'DD/MM/YYYY')as fecha_visita ,id_visita,"
                + "descripcion_cama from schema_uo.visita V,schema_uo.visita_categorizacion VC,"
                + "schema_uo.cama C where V.id_cama=C.id_cama and "
                + "V.id_duo=" + id_duo + " and V.id_visita_categorizacion=VC.id_visita_categorizacion "
                + "order by V.fecha_visita");
//            this.cnn.setSentenciaSQL("select * from schema_uo.visita V,schema_uo.visita_categorizacion VC,"
//                + "schema_uo.cama C where V.id_cama=C.id_cama and "
//                + "V.id_duo=" + id_duo + " and V.id_visita_categorizacion=VC.id_visita_categorizacion "
//                + "order by V.fecha_visita");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cVisita aux = new cVisita();
                aux.setCat_visita_categorizacion(cnn.getRst().getString("cat_visita_categorizacion"));
                aux.setFecha_visita(cnn.getRst().getString("fecha_visita"));
                aux.setId_visita_categorizacion(cnn.getRst().getInt("id_visita"));
                aux.setDescripcion_cama(cnn.getRst().getString("descripcion_cama"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cDuo obtiene_duo_liviano(int id_duo) {
        cDuo duo = new cDuo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT * FROM schema_uo.duo  where id_duo='" + id_duo + "' LIMIT  ; ");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                duo.setEstado_duo(cnn.getRst().getInt("estado_duo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return duo;
    }

    public cDuo obtiene_duo(int id_duo) {
        cDuo duo = new cDuo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
//        this.cnn.setSentenciaSQL("SELECT "
//                + " AA.id_duo,to_char(fecha_duo,'DD/MM/YYYY')as fecha_duo,hora_duo,estado_duo, "
//                + " AA.id_cama, descripcion_cama,id_prevision, "
//                + " to_char(fecha_hora_ing_duo,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_ing_duo, rut_usuario, "
//                + " (SELECT nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as nombre_completo "
//                + " FROM  schema_uo.usuario where rut_usuario=AA.rut_usuario) as nombre_admisor, "
//                + " rut_paciente,anamnesis_duo,  AA.id_derivador,descripcion_derivador, "
//                + "  AA.id_categorizacion,descripcion_categorizacion,tipo_duo_id, "
//                + " to_char(fecha_hora_alta_med_duo,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_alta_med_duo ,"
//                + " to_char(fecha_hora_alta_adm_duo ,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_alta_adm_duo,"
//                + " duo_tiene_enfermeria,  rut_usuario_ing_med,"
//                + " (SELECT LL.fecha_hora_ing_enfermeria FROM schema_uo.ing_enfermeria LL  "
//                + " where LL.id_duo_ing_enfermeria=AA.id_duo limit 1) as fecha_hora_ing_enf, "
//                + "  (SELECT  "
//                + "  nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as nombre_completo "
//                + "  FROM  schema_uo.usuario VV JOIN schema_uo.ing_enfermeria RR ON "
//                + " (VV.rut_usuario=RR.rut_usuario_ing_enfermeria)  "
//                + "  where RR.id_duo_ing_enfermeria=AA.id_duo limit 1) as nombre_enf, "
//                + " (SELECT  "
//                + "  nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as nombre_completo "
//                + "  FROM  schema_uo.usuario where rut_usuario= rut_usuario_ing_med) as nombre_med, "
//                + "  to_char(fecha_hora_ing_med,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_ing_med, ip_ing_enf,ip_ing_med, "
//                + "  ( SELECT BB.cat_visita_categorizacion  "
//                + "  FROM schema_uo.visita AA   "
//                + "  JOIN schema_uo.visita_categorizacion BB ON   "
//                + " (AA.id_visita_categorizacion=BB.id_visita_categorizacion)   "
//                + " where id_duo=AA.id_duo order by AA.fecha_hora_visita DESC limit 1 ) as ultima_cat, "
//                + "  ( SELECT BB.cat_visita_categorizacion  "
//                + "  FROM schema_uo.visita AA   "
//                + "  JOIN schema_uo.visita_categorizacion BB ON   "
//                + "  (AA.id_visita_categorizacion=BB.id_visita_categorizacion)   "
//                + "  where id_duo=AA.id_duo order by AA.fecha_hora_visita ASC limit 1 ) as primera_cat, "
//                + "   BB.paciente_nombres,BB.paciente_apellidop, BB.paciente_apellidom, "
//                + "   BB.paciente_telefono1, BB.paciente_telefono2, "
//                + "  to_char( BB.paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac, age(AA.fecha_duo,BB.paciente_fecha_nac) AS paciente_edad, "
//                + "   BB.paciente_direccion,comuna_descripcion,BB.paciente_sexo, "
//                + "  BB.paciente_pueblo,BB.paciente_consultorio, "
//                + "  (SELECT PP.con_descripcion from schemaoirs.consultorio_pertenencia PP WHERE "
//                + " PP.con_id=BB.paciente_consultorio)as consultorio_pertenencia, "
//                + " (SELECT QQ.pue_descripcion from schemaoirs.pueblo_originario QQ WHERE "
//                + " QQ.pue_id=BB.paciente_pueblo) as pueblo_originario, "
//                + " codigo_fonasa_descripcion,tramo_prevision_paciente, "
//                + " prais_prevision_paciente,verificado_fonasa "
//                + " FROM schema_uo.duo AA "
//                + " JOIN schema_urgencia.paciente BB ON "
//                + " (AA.rut_paciente=BB.paciente_rut) "
//                + " JOIN schema_urgencia.prevision_paciente  ON  "
//                + " (BB.paciente_rut=prevision_paciente.id_paciente_prevision_paciente) "
//                + " JOIN schema_urgencia.codigo_fonasa ON  "
//                + " (prevision_paciente.codigo_fonasa_prevision_paciente=codigo_fonasa.codigo_fonasa_id) "
//                + "  JOIN schema_uo.cama ON (AA.id_cama=cama.id_cama) "
//                + "  JOIN schemaoirs.comuna ON (BB.comuna_codigo=comuna.comuna_codigo) "
//                + "  JOIN schema_uo.derivador ON (AA.id_derivador=derivador.id_derivador) "
//                + " JOIN schema_uo.categorizacion ON (AA.id_categorizacion=categorizacion.id_categorizacion)"
//                + "  where id_duo='" + id_duo + "' and   prevision_paciente.estado_prevision_paciente='1'");

        this.cnn.setSentenciaSQL("SELECT  "
                + " AA.id_duo,to_char(fecha_duo,'DD/MM/YYYY')as fecha_duo,hora_duo,estado_duo,  "
                + " AA.id_cama, descripcion_cama,id_prevision,  "
                + " to_char(fecha_hora_ing_duo,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_ing_duo,  "
                + " rut_usuario,AA.programa_duo,  "
                + " EXTRACT(SECOND FROM CURRENT_TIMESTAMP-fecha_hora_ing_med) as dif_ss,  "
                + " EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-fecha_hora_ing_med) as dif_mm,   "
                + " EXTRACT(HOUR FROM CURRENT_TIMESTAMP-fecha_hora_ing_med)as dif_hh,   "
                + " EXTRACT(DAY FROM CURRENT_TIMESTAMP-fecha_hora_ing_med)as dif_dd, "
                + " (SELECT nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as "
                + "  nombre_completo  "
                + "   FROM  schema_uo.usuario where rut_usuario=AA.rut_usuario) as nombre_admisor,  "
                + "    rut_paciente,anamnesis_duo,  AA.id_derivador,descripcion_derivador,  "
                + "   AA.id_categorizacion,descripcion_categorizacion,tipo_duo_id,  "
                + " to_char(fecha_hora_alta_med_duo,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_alta_med_duo ,"
                + " to_char(fecha_hora_alta_adm_duo ,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_alta_adm_duo, "
                + " duo_tiene_enfermeria,  rut_usuario_ing_med, "
                + " (SELECT to_char(LL.fecha_hora_ing_enfermeria,'DD/MM/YYYY HH24:MI:SS') as fecha_hora_ing_enfermeria FROM schema_uo.ing_enfermeria LL   "
                + " where LL.id_duo_ing_enfermeria=AA.id_duo limit 1) as fecha_hora_ing_enf,  "
                + "   (SELECT   "
                + "  nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as nombre_completo  "
                + "  FROM  schema_uo.usuario VV JOIN schema_uo.ing_enfermeria RR ON  "
                + "  (VV.rut_usuario=RR.rut_usuario_ing_enfermeria)   "
                + "  where RR.id_duo_ing_enfermeria=AA.id_duo limit 1) as nombre_enf,  "
                + " (SELECT   "
                + " nombre_usuario||' '||apellidop_usuario||' '||apellidom_usuario as nombre_completo  "
                + " FROM  schema_uo.usuario where rut_usuario= rut_usuario_ing_med) as nombre_med,  "
                + "  to_char(fecha_hora_ing_med,'DD/MM/YYYY HH24:MI:SS')as fecha_hora_ing_med, "
                + "   ip_ing_enf,ip_ing_med,  "
                + "  ( SELECT BB.cat_visita_categorizacion   "
                + "    FROM schema_uo.visita AA    "
                + " JOIN schema_uo.visita_categorizacion BB ON    "
                + "   (AA.id_visita_categorizacion=BB.id_visita_categorizacion)    "
                + "    where id_duo=AA.id_duo order by AA.fecha_hora_visita DESC limit 1 ) as ultima_cat,  "
                + "    ( SELECT BB.cat_visita_categorizacion   "
                + "  FROM schema_uo.visita AA    "
                + "  JOIN schema_uo.visita_categorizacion BB ON    "
                + " (AA.id_visita_categorizacion=BB.id_visita_categorizacion)    "
                + "  where id_duo=AA.id_duo order by AA.fecha_hora_visita ASC limit 1 ) as primera_cat,  "
                + "  BB.paciente_nombres,BB.paciente_apellidop, BB.paciente_apellidom,  "
                + " BB.paciente_telefono1, BB.paciente_telefono2,  "
                + "  to_char( BB.paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac,  "
                + "  age(AA.fecha_duo,BB.paciente_fecha_nac) AS paciente_edad,  "
                + "  BB.paciente_direccion,comuna_descripcion,BB.paciente_sexo,  "
                + "   BB.paciente_pueblo,BB.paciente_consultorio,BB.paciente_nacionalidad,"
                + " (SELECT nac_descripcion FROM  schemaoirs.nacion where nac_id=BB.paciente_nacionalidad limit 1)as paciente_nacionalidad_descripcion,  "
                + "  (SELECT PP.con_descripcion from schemaoirs.consultorio_pertenencia PP WHERE  "
                + " PP.con_id=BB.paciente_consultorio)as consultorio_pertenencia,  "
                + " (SELECT QQ.pue_descripcion from schemaoirs.pueblo_originario QQ WHERE  "
                + "  QQ.pue_id=BB.paciente_pueblo limit 1) as pueblo_originario,  "
                + "  codigo_fonasa_descripcion,tramo_prevision_paciente,  "
                + "  prais_prevision_paciente,verificado_fonasa,"
                + " EXTRACT(DAY FROM (fecha_hora_alta_med_duo)-(fecha_duo+hora_duo)) as dias_cama  "
                + " FROM schema_uo.duo AA  "
                + " JOIN schema_urgencia.paciente BB ON  "
                + "  (AA.rut_paciente=BB.paciente_rut)  "
                + "    JOIN schema_urgencia.prevision_paciente  ON   "
                + " (BB.paciente_rut=prevision_paciente.id_paciente_prevision_paciente)  "
                + "   JOIN schema_urgencia.codigo_fonasa ON   "
                + " (prevision_paciente.codigo_fonasa_prevision_paciente=codigo_fonasa.codigo_fonasa_id)  "
                + "  JOIN schema_uo.cama ON (AA.id_cama=cama.id_cama)  "
                + "   JOIN schemaoirs.comuna ON (BB.comuna_codigo=comuna.comuna_codigo)  "
                + "  JOIN schema_uo.derivador ON (AA.id_derivador=derivador.id_derivador)  "
                + " JOIN schema_uo.categorizacion ON (AA.id_categorizacion=categorizacion.id_categorizacion) "
                + "  where id_duo='" + id_duo + "' and   prevision_paciente.estado_prevision_paciente='1'");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                duo.setEstado_duo(cnn.getRst().getInt("estado_duo"));

                duo.setDias_cama(cnn.getRst().getInt("dias_cama"));

                if (duo.getEstado_duo() == 1) {
                    duo.setEstado_duo_descripcion("INGRESADO ADM.");
                } else if (duo.getEstado_duo() == 2) {
                    duo.setEstado_duo_descripcion("RECEPCIONADO CON INGRESO DE ENFERMERIA");
                } else if (duo.getEstado_duo() == 3) {
                    duo.setEstado_duo_descripcion("CON ALTA MEDICA");
                } else if (duo.getEstado_duo() == 4) {
                    duo.setEstado_duo_descripcion("CON ALTA ADMINISTRATIVA");
                } else if (duo.getEstado_duo() == 21) {
                    duo.setEstado_duo_descripcion("EN CAMA");
                } else if (duo.getEstado_duo() == 99) {
                    duo.setEstado_duo_descripcion("ANULADO");
                }

                duo.setCama(cnn.getRst().getInt("id_cama"));
                duo.setCama_descripcion(cnn.getRst().getString("descripcion_cama"));
                duo.setId_prevision(cnn.getRst().getInt("id_prevision"));
                duo.setFecha_hora_ing_duo(cnn.getRst().getString("fecha_hora_ing_duo"));
                duo.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                duo.setNombre_usuario_admision(cnn.getRst().getString("nombre_admisor"));
                duo.setRut_paciente(cnn.getRst().getString("rut_paciente"));
                duo.setAnamnesis_duo(cnn.getRst().getString("anamnesis_duo"));
                duo.setDerivador_id(cnn.getRst().getInt("id_derivador"));
                duo.setDerivador_descripcion(cnn.getRst().getString("descripcion_derivador"));
                duo.setCategorizacion_id(cnn.getRst().getInt("id_categorizacion"));
                duo.setCategorizacion_descripcion(cnn.getRst().getString("descripcion_categorizacion"));
                duo.setTipo_duo_id(cnn.getRst().getInt("tipo_duo_id"));

                duo.setFecha_hora_alta_adm_duo(cnn.getRst().getString("fecha_hora_alta_adm_duo"));
                duo.setFecha_hora_alta_med_duo(cnn.getRst().getString("fecha_hora_alta_med_duo"));

                if (duo.getFecha_hora_alta_adm_duo() == null) {
                    duo.setFecha_hora_alta_adm_duo("---");
                }

                if (duo.getFecha_hora_alta_med_duo() == null) {
                    duo.setFecha_hora_alta_med_duo("---");
                }

                duo.setTiene_enfermeria(cnn.getRst().getInt("duo_tiene_enfermeria"));
                duo.setRut_usuario_ing_med(cnn.getRst().getString("rut_usuario_ing_med"));
                duo.setNombre_usuario_ing_enf(cnn.getRst().getString("nombre_enf"));
                duo.setNombre_usuario_ing_med(cnn.getRst().getString("nombre_med"));
                if (duo.getNombre_usuario_ing_med().contains("PRUEBA")) {
                    duo.setNombre_usuario_ing_med("--");
                }
                duo.setFecha_hora_ing_med(cnn.getRst().getString("fecha_hora_ing_med"));
                if (duo.getFecha_hora_ing_med() == null) {
                    duo.setFecha_hora_ing_med("---");
                }
                duo.setFecha_hora_ing_enf(cnn.getRst().getString("fecha_hora_ing_enf"));
                if (duo.getFecha_hora_ing_med() == null) {
                    duo.setFecha_hora_ing_enf("---");
                }
                duo.setIp_ing_enf(cnn.getRst().getString("ip_ing_enf"));
                duo.setIp_ing_med(cnn.getRst().getString("ip_ing_med"));

                duo.setPrimera_clasificacion(cnn.getRst().getString("primera_cat"));
                duo.setUltima_clasificacion(cnn.getRst().getString("ultima_cat"));
                duo.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                duo.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                duo.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                duo.setTelefono1(cnn.getRst().getString("paciente_telefono1"));
                duo.setTelefono2(cnn.getRst().getString("paciente_telefono2"));
                duo.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));

                String edad = cnn.getRst().getString("paciente_edad");
                edad = edad.replace("years", "años");
                edad = edad.replace("year", "año");
                edad = edad.replace("mons", "meses");
                edad = edad.replace("mon", "mes");
                edad = edad.replace("days", "dias.z");
                edad = edad.replace("day", "dia.z");

                duo.setEdad(edad);

                try {
                    edad = edad.substring(0, edad.indexOf("z") - 1);
                    duo.setEdad(edad);
                } catch (Exception exx) {
                }

                duo.setDireccion(cnn.getRst().getString("paciente_direccion"));
                duo.setComuna_descri(cnn.getRst().getString("comuna_descripcion"));
                duo.setSexo(cnn.getRst().getInt("paciente_sexo"));
                if (duo.getSexo() == 0) {
                    duo.setSexo_descri("Masculino");
                } else if (duo.getSexo() == 1) {
                    duo.setSexo_descri("Femenino");
                } else {
                    duo.setSexo_descri("---");
                }
                duo.setPueblo(cnn.getRst().getInt("paciente_pueblo"));
                duo.setConsultorio(cnn.getRst().getInt("paciente_consultorio"));
                duo.setPueblo_descri(cnn.getRst().getString("pueblo_originario"));
                duo.setConsultorio_descri(cnn.getRst().getString("consultorio_pertenencia"));

                if (duo.getPueblo_descri() == null) {
                    duo.setPueblo_descri("");
                }
                if (duo.getConsultorio_descri() == null) {
                    duo.setConsultorio_descri("");
                }

                duo.setPrograma(cnn.getRst().getInt("programa_duo"));
                if (duo.getPrograma() == 0) {
                    duo.setPrograma_descripcion("Sin Información");
                } else if (duo.getPrograma() == 1) {
                    duo.setPrograma_descripcion("Ges Si");
                } else if (duo.getPrograma() == 2) {
                    duo.setPrograma_descripcion("Ges No");
                }

                duo.setCodigo_fonasa_descripcion(cnn.getRst().getString("codigo_fonasa_descripcion"));
                duo.setTramo_prevision(cnn.getRst().getString("tramo_prevision_paciente"));
                duo.setPrais(cnn.getRst().getInt("prais_prevision_paciente"));
                duo.setVerificado_fonasa(cnn.getRst().getInt("verificado_fonasa"));

                duo.setNacion(cnn.getRst().getInt("paciente_nacionalidad"));

                duo.setNacion_descripcion(cnn.getRst().getString("paciente_nacionalidad_descripcion"));

                if (duo.getNacion_descripcion() == null) {
                    duo.setNacion_descripcion("");
                }

                duo.setDif_dd(cnn.getRst().getInt("dif_dd"));
                duo.setDif_hh(cnn.getRst().getInt("dif_hh"));
                duo.setDif_mm(cnn.getRst().getInt("dif_mm"));
                duo.setDif_ss(cnn.getRst().getInt("dif_ss"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return duo;
    }

    public ArrayList obtiene_duo_enfermedad(int duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " id_duo_enfermedad_cro,id_duo, "
                + "  AA.id_enfermedad_cro, BB.descripcion_enfermedad_cro "
                + "   FROM  schema_uo.duo_enfermedad_cro AA  "
                + "  JOIN schema_uo.enfermedad_cro BB ON (AA.id_enfermedad_cro=BB.id_enfermedad_cro)  "
                + "  where id_duo='" + duo + "'");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEnfermedad aux = new cEnfermedad();
                aux.setDescripcion(cnn.getRst().getString("descripcion_enfermedad_cro"));
                aux.setId(cnn.getRst().getInt("id_duo_enfermedad_cro"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_diagnostico(int id_duo, String tipo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + " id_diagnostico_duo, descripcion_diagnostico_duo, "
                + " tipo_diagnostico_duo, id_duo "
                + " FROM  schema_uo.diagnostico_duo where id_duo='" + id_duo + "' and "
                + " tipo_diagnostico_duo IN (" + tipo + ") ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("id_diagnostico_duo"));
                diag.setId_duo(cnn.getRst().getInt("id_duo"));
                diag.setTipo_diagnostico(cnn.getRst().getInt("tipo_diagnostico_duo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("descripcion_diagnostico_duo"));
                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_diagnostico_trazador(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " duop_id,duop_duo, duop_prestacion,pre_nombre, "
                + " duop_estado,to_char(duop_ingreso,'DD/MM/YYYY HH24:MI:SS')as duop_ingreso "
                + " FROM  schema_uo.prestacion_duo PED JOIN schema_uo.prestacion PRE "
                + " ON (PED.duop_prestacion=PRE.pre_id) WHERE "
                + " duop_duo=" + id_duo + " and duop_estado=1  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("duop_id"));
                diag.setId_duo(cnn.getRst().getInt("duop_duo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("pre_nombre"));
                diag.setFecha_ingreso(cnn.getRst().getString("duop_ingreso"));
                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_enfermedad_cronica(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " id_duo_enfermedad_cro, id_duo,DEC.id_enfermedad_cro,descripcion_enfermedad_cro "
                + " FROM  schema_uo.duo_enfermedad_cro DEC "
                + " JOIN schema_uo.enfermedad_cro ENF ON (DEC.id_enfermedad_cro=ENF.id_enfermedad_cro) "
                + " WHERE DEC.id_duo='" + id_duo + "'");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("id_duo_enfermedad_cro"));
                diag.setId_duo(cnn.getRst().getInt("id_duo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("descripcion_enfermedad_cro"));
                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cPaciente obtiene_paciente(String rut_paciente) {
        cPaciente pac = new cPaciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " paciente_rut,paciente_nombres,paciente_apellidop, "
                + "  paciente_apellidom,paciente_sexo,to_char(paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac, "
                + "  paciente_direccion,paciente_telefono1,paciente_telefono2, "
                + "  comuna_codigo,to_char(paciente_fecha_creacion,'DD/MM/YYYY HH24:MI:SS') as paciente_fecha_creacion,paciente_procedencia, "
                + "  paciente_consultorio,paciente_pueblo, "
                + "  BB.codigo_fonasa_prevision_paciente,id_prevision_paciente, "
                + "  BB.tramo_prevision_paciente, "
                + "  BB.prais_prevision_paciente, "
                + "  codigo_fonasa.codigo_fonasa_descripcion,paciente_mail "
                + "  FROM  schema_urgencia.paciente AA "
                + "  JOIN schema_urgencia.prevision_paciente BB  "
                + "  ON (AA.paciente_rut=BB.id_paciente_prevision_paciente) "
                + "  JOIN schema_urgencia.codigo_fonasa ON  "
                + "  (BB.codigo_fonasa_prevision_paciente=codigo_fonasa.codigo_fonasa_id) "
                + "  where  paciente_rut='" + rut_paciente + "' and  BB.estado_prevision_paciente='1' "
                + "  limit 1 ");
        this.cnn.conectar();

        try {
            if (cnn.getRst().next()) {
                pac.setRut_paciente(cnn.getRst().getString("paciente_rut"));
                pac.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                pac.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                pac.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                pac.setDireccion(cnn.getRst().getString("paciente_direccion"));
                pac.setTelefono1(cnn.getRst().getString("paciente_telefono1"));
                pac.setTelefono2(cnn.getRst().getString("paciente_telefono2"));
                pac.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));
                pac.setDv(cnn.getRst().getString("paciente_rut").substring(pac.getRut_paciente().length() - 1, pac.getRut_paciente().length()));
                pac.setSexo(cnn.getRst().getInt("paciente_sexo"));
                if (pac.getSexo() == 0) {
                    pac.setSexo_descri("MASCULINO");
                } else if (pac.getSexo() == 1) {
                    pac.setSexo_descri("FEMENINO");
                }
                pac.setComuna_codigo(cnn.getRst().getInt("comuna_codigo"));
                pac.setPueblo(cnn.getRst().getInt("paciente_pueblo"));
                pac.setConsultorio(cnn.getRst().getInt("paciente_consultorio"));

                pac.setId_prevision(cnn.getRst().getInt("id_prevision_paciente"));
                pac.setTramo_prevision(cnn.getRst().getString("tramo_prevision_paciente"));
                pac.setCodigo_fonasa(cnn.getRst().getString("codigo_fonasa_prevision_paciente"));
                pac.setCodigo_fonasa_descripcion(cnn.getRst().getString("codigo_fonasa_descripcion"));
                pac.setPrais(cnn.getRst().getInt("prais_prevision_paciente"));

                pac.setMail(cnn.getRst().getString("paciente_mail"));
                if (pac.getMail() == null) {
                    pac.setMail("");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.cnn.cerrarConexion();
        return pac;
    }

    public ArrayList lista_grilla_camas() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT to_char(D.fecha_duo,'DD/MM/YYYY') as fecha_duo ,D.hora_duo, "
                + "  C.id_cama,C.id_sala,schema_uo.sala.descripcion_sala ,C.descripcion_cama, "
                + "  D.id_duo,D.rut_paciente,D.id_categorizacion,D.estado_duo,  "
                + " paciente_nombres,paciente_apellidop,paciente_apellidom ,"
                + " fecha_hora_alta_adm_duo,fecha_hora_alta_med_duo, "
                + " D.duo_tiene_enfermeria, (SELECT BB.cat_visita_categorizacion "
                + " FROM schema_uo.visita AA  "
                + "  JOIN schema_uo.visita_categorizacion BB ON  "
                + " (AA.id_visita_categorizacion=BB.id_visita_categorizacion)  "
                + " where id_duo=D.id_duo order by AA.fecha_hora_visita DESC limit 1) as ultima_cat,"
                + "  EXTRACT(DAY FROM (CURRENT_DATE+CURRENT_TIME)-(d.fecha_duo+d.hora_duo)) as dias_cama "
                + "   FROM schema_uo.cama C left JOIN schema_uo.duo D ON D.id_cama = C.id_cama    "
                + "   and (D.estado_duo=2 or D.estado_duo=1 or D.estado_duo=3 or D.estado_duo=21)    "
                + "     left JOIN schema_uo.sala   "
                + "      ON (schema_uo.sala.id_sala=C.id_sala)   "
                + "     left JOIN schema_urgencia.paciente   "
                + "   ON (schema_urgencia.paciente.paciente_rut=D.rut_paciente)   "
                + "  where C.id_sala in (11,12,13,14,15,16,17,18,19,20) and C.estado_cama=1 order by C.id_cama");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDuo duo = new cDuo();
                duo.setRut_paciente(cnn.getRst().getString("rut_paciente"));
                duo.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                duo.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                duo.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setEstado_duo(cnn.getRst().getInt("estado_duo"));
                duo.setTiene_enfermeria(cnn.getRst().getInt("duo_tiene_enfermeria"));
                duo.setCama(cnn.getRst().getInt("id_cama"));
                duo.setCama_descripcion(cnn.getRst().getString("descripcion_cama"));
                duo.setSala(cnn.getRst().getInt("id_sala"));
                duo.setSala_descripcion(cnn.getRst().getString("descripcion_sala"));
                duo.setUltima_clasificacion(cnn.getRst().getString("ultima_cat") + "");
                if (duo.getUltima_clasificacion().equals("null")) {
                    duo.setUltima_clasificacion("P");
                } else {
                    duo.setUltima_clasificacion(cnn.getRst().getString("ultima_cat") + "");
                }
                duo.setDias_cama(cnn.getRst().getInt("dias_cama"));

                duo.setFecha_hora_alta_adm_duo(cnn.getRst().getString("fecha_hora_alta_adm_duo"));
                duo.setFecha_hora_alta_med_duo(cnn.getRst().getString("fecha_hora_alta_med_duo"));

                if (duo.getFecha_hora_alta_adm_duo() == null) {
                    duo.setFecha_hora_alta_adm_duo("---");
                }

                if (duo.getFecha_hora_alta_med_duo() == null) {
                    duo.setFecha_hora_alta_med_duo("---");
                }

                lista.add(duo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_documentos_paciente(String rut_paciente) {
        // este metodo sirve para ver si un paciente tiene un duo activo en la unidad y si esta registradoen sistema
        /*DUDA CON ESTE METODO; PIENSO QUE DEBERIA FILTRAR POR EL NUMERO DE DUO 25-05-2012 */
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        /*  FIN 23082013
         this.cnn.setSentenciaSQL("SELECT D.id_duo,D.estado_duo,to_char(D.fecha_duo,'DD/MM/YYYY')as fecha_duo ,E.id_epicrisis,"
         + " to_char(E.fecha_epicrisis,'DD/MM/YYYY')as fecha_epicrisis ,E.hora_epicrisis "
         + " FROM schema_uo.duo D LEFT JOIN schema_uo.epicrisis E "
         + " ON D.id_duo=E.id_duo where D.rut_paciente='" + rut_paciente + "' and estado_duo NOT IN (99) ORDER BY D.id_duo desc");
         23082013 */
        this.cnn.setSentenciaSQL("SELECT D.id_duo,D.estado_duo,to_char(D.fecha_duo,'DD/MM/YYYY')as fecha_duo ,E.id_epicrisis, "
                + " to_char(E.fecha_epicrisis,'DD/MM/YYYY')as fecha_epicrisis ,E.hora_epicrisis , "
                + " D.id_prevision,PRE.tramo_prevision_paciente,PRE.prais_prevision_paciente, "
                + " COD.codigo_fonasa_descripcion "
                + " FROM schema_uo.duo D "
                + " JOIN schema_urgencia.prevision_paciente PRE ON (D.id_prevision=PRE.id_prevision_paciente) "
                + " JOIN schema_urgencia.codigo_fonasa COD ON (PRE.codigo_fonasa_prevision_paciente=COD.codigo_fonasa_id) "
                + " LEFT JOIN schema_uo.epicrisis E ON D.id_duo=E.id_duo  "
                + " WHERE D.rut_paciente='" + rut_paciente + "'  "
                + " and estado_duo NOT IN (99) ORDER BY D.id_duo desc");

        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEpicrisis epi = new cEpicrisis();
                epi.setId_duo(cnn.getRst().getInt("id_duo"));
                epi.setEstado_duo(cnn.getRst().getInt("estado_duo"));
                epi.setId_epicrisis(cnn.getRst().getInt("id_epicrisis"));
                epi.setFecha_epicrisis(cnn.getRst().getString("fecha_epicrisis"));
                epi.setFecha_duo(cnn.getRst().getString("fecha_duo"));

                epi.setPrevision(cnn.getRst().getInt("id_prevision"));
                epi.setPrevision_descri(cnn.getRst().getString("codigo_fonasa_descripcion"));
                epi.setPrais(cnn.getRst().getInt("prais_prevision_paciente"));
                if (epi.getPrais() == 1) {
                    epi.setPrais_descri("PRAIS");
                } else {
                    epi.setPrais_descri("");
                }

                epi.setTramo(cnn.getRst().getString("tramo_prevision_paciente"));
                lista.add(epi);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_documentos_paciente_suam(String parametro, int modo) {

        String campo = "";
        if (modo == 1) {
            campo = "das_paciente";
        } else if (modo == 2) {
            campo = "dau_id";
        } else if (modo == 3) {
            campo = "dau_nn_id";
        } else {
            campo = "das_paciente";
        }

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " das_id, dau_id,dau_nn_id, "
                + " das_paciente, das_estado,  "
                + " to_char(das_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS') as das_fecha_ingreso, "
                + " das_camilla, das_derivador, das_medico, "
                + " das_usuario, das_ip, das_indicacion_destino, "
                + " das_indicacion_usuario,das_indicacion_fecha_ingreso, das_indicacion_ip, "
                + " cam.cam_descripcion "
                + " FROM schema_suam.das da JOIN schema_suam.camilla cam ON(da.das_camilla=cam.cam_id) "
                + " WHERE " + campo + "='" + parametro + "' and das_estado NOT IN (99);");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDas das = new cDas();

                das.setRut_paciente(cnn.getRst().getString("das_paciente"));
                das.setDau_nn_id(cnn.getRst().getInt("dau_nn_id"));
                das.setDau_id(cnn.getRst().getInt("dau_id"));
                das.setId_das(cnn.getRst().getInt("das_id"));
                das.setCamilla(cnn.getRst().getInt("das_camilla"));
                das.setCamilla_descri(cnn.getRst().getString("cam_descripcion"));
                das.setFecha_ingreso(cnn.getRst().getString("das_fecha_ingreso"));

                lista.add(das);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cEpicrisis obtiene_epicrisis(String id_epicrisis) {
        cEpicrisis aux = new cEpicrisis();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " id_epicrisis,resumen_epicrisis,examen_epicrisis, "
                + " diagnostico_epicrisis,indicacion_epicrisis, "
                + " to_char(fecha_epicrisis,'DD/MM/YYYY') as fecha_epicrisis, hora_epicrisis, "
                + " BB.rut_usuario,AA.id_duo,fecha_hora_epicrisis, "
                + " BB.nombre_usuario, BB.apellidop_usuario,BB.apellidom_usuario, "
                + " paciente_nombres,paciente_apellidop,paciente_apellidom "
                + " FROM schema_uo.epicrisis AA  "
                + " JOIN schema_uo.usuario BB ON  "
                + " (AA.rut_usuario=BB.rut_usuario) "
                + " JOIN schema_uo.duo ON "
                + " (AA.id_duo=schema_uo.duo.id_duo) "
                + " JOIN schema_urgencia.paciente ON  "
                + " (schema_uo.duo.rut_paciente=schema_urgencia.paciente.paciente_rut) "
                + "  where id_epicrisis='" + id_epicrisis + "';");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return aux;
    }

    public ArrayList lista_comuna() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + " comuna_codigo, AA.provincia_codigo,BB.region_codigo,comuna_descripcion, "
                + " BB.provincia_descripcion  ,region_descripcion "
                + " FROM  schemaoirs.comuna AA JOIN schemaoirs.provincia BB ON  "
                + " (AA.provincia_codigo=BB.provincia_codigo) "
                + " JOIN schemaoirs.region ON (BB.region_codigo=schemaoirs.region.region_codigo)");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cComuna aux = new cComuna();
                aux.setComuna_codigo(cnn.getRst().getString("comuna_codigo"));
                aux.setComuna_descripcion(cnn.getRst().getString("comuna_descripcion"));
                aux.setProvincia_codigo(cnn.getRst().getString("provincia_codigo"));
                aux.setProvincia_descripcion(cnn.getRst().getString("provincia_descripcion"));
                aux.setRegion_codigo(cnn.getRst().getString("region_codigo"));
                aux.setRegion_descripcion(cnn.getRst().getString("region_descripcion"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_cama_desocupada(String salas) {
        // 1 es por defecto area.-
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT C.id_cama,C.descripcion_cama,D.id_duo,S.descripcion_sala  "
                + " FROM schema_uo.sala S,schema_uo.cama C left JOIN schema_uo.duo D "
                + " ON D.id_cama = C.id_cama and D.estado_duo NOT IN (4,99) "
                + " where D.id_duo isnull and S.id_sala=C.id_sala and C.estado_cama=1  "
                + " and S.id_area_clinica IN (1) and S.id_sala IN (" + salas + ")  order by C.id_cama");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cCama aux = new cCama();

                aux.setId_cama(cnn.getRst().getInt("id_cama"));
                aux.setDescripcion_cama(cnn.getRst().getString("descripcion_cama"));
                aux.setDescripcion_sala(cnn.getRst().getString("descripcion_sala"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_pueblo() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  pue_id,pue_descripcion, pue_fecha_ingreso "
                + " FROM  schemaoirs.pueblo_originario where pue_estado='1';");

        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cPueblo aux = new cPueblo();
                aux.setDescripcion_pueblo(cnn.getRst().getString("pue_descripcion"));
                aux.setId_pueblo(cnn.getRst().getInt("pue_id"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_consultorio_pertenecia() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  con_id,con_descripcion, "
                + " con_fecha_ingreso FROM  schemaoirs.consultorio_pertenencia "
                + " where con_estado='1' ");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cConsultorio aux = new cConsultorio();
                aux.setDescripcion(cnn.getRst().getString("con_descripcion"));
                aux.setId(cnn.getRst().getInt("con_id"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_derivador() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + "  id_derivador, descripcion_derivador, estado_crd_deriador "
                + " FROM  schema_uo.derivador where estado_derivador='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cConsultorio aux = new cConsultorio();
                aux.setDescripcion(cnn.getRst().getString("descripcion_derivador"));
                aux.setId(cnn.getRst().getInt("id_derivador"));
                aux.setEstado_crd(cnn.getRst().getInt("estado_crd_deriador"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_destino() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_destino, "
                + " descripcion_destino, estado_destino "
                + " FROM   schema_uo.destino where estado_destino='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cConsultorio aux = new cConsultorio();
                aux.setDescripcion(cnn.getRst().getString("descripcion_destino"));
                aux.setId(cnn.getRst().getInt("id_destino"));
                aux.setEstado_crd(cnn.getRst().getInt("estado_destino"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_categorizacion() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_categorizacion,descripcion_categorizacion "
                + " FROM  schema_uo.categorizacion where "
                + " estado_categorizacion='1' order by id_categorizacion;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cCategorizacion cat = new cCategorizacion();
                cat.setDescripcion(cnn.getRst().getString("descripcion_categorizacion"));
                cat.setId(cnn.getRst().getInt("id_categorizacion"));
                lista.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_documentos() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_documento, descripcion_documento "
                + " FROM  schema_uo.documento where estado_documento='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDocumento doc = new cDocumento();
                doc.setDescripcion(cnn.getRst().getString("descripcion_documento"));
                doc.setId(cnn.getRst().getInt("id_documento"));
                lista.add(doc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_prestacion_trazadora() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  pre_id,pre_nombre "
                + " FROM  schema_uo.prestacion where  pre_estado=1 order by pre_nombre;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEnfermedad enf = new cEnfermedad();
                enf.setDescripcion(cnn.getRst().getString("pre_nombre"));
                enf.setId(cnn.getRst().getInt("pre_id"));
                lista.add(enf);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_enfermedad_cronica() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  id_enfermedad_cro,descripcion_enfermedad_cro "
                + " FROM  schema_uo.enfermedad_cro AA where  estado_enfermedad_cro='1' "
                + "  order by descripcion_enfermedad_cro ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEnfermedad enf = new cEnfermedad();
                enf.setDescripcion(cnn.getRst().getString("descripcion_enfermedad_cro"));
                enf.setId(cnn.getRst().getInt("id_enfermedad_cro"));
                lista.add(enf);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_riesgo_dependendencia() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select * from schema_uo.ambito_crd ACRD where "
                + "ACRD.estado_ambito_crd=1 order by ACRD.id_ambito_crd");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cCatRiesgoDependencia cat = new cCatRiesgoDependencia();
                cat.setId_ambito_crd(cnn.getRst().getInt("id_ambito_crd"));
                cat.setDescripcion_ambito_crd(cnn.getRst().getString("descripcion_ambito_crd"));
                cat.setTipo_ambito_crd(cnn.getRst().getInt("tipo_ambito_crd"));
                cat.setAbre_ambito_crd(cnn.getRst().getString("abre_ambito_crd"));
                lista.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cDuo obtiene_dato_segun_duo(int id_duo) {
        cDuo duo = new cDuo();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT id_duo,id_cama "
                + " FROM   schema_uo.duo where id_duo='" + id_duo + "' ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setCama(cnn.getRst().getInt("id_cama"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return duo;
    }

    public String obtiene_fecha_hora() {
        String hora = "";
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT to_char(current_timestamp,'DD/MM/YYYY HH24:MI:SS')as hora FROM schema_uo.duo limit 1 ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                hora = cnn.getRst().getString("hora");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return hora;
    }

    public String tiempo_desde_alta_med(int duo) {
        String tiempo = "";
        int dia = 0;
        int hora = 0;
        int minuto = 0;
        int segundo = 0;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  Select "
                + " EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-fecha_hora_epicrisis) as resta_minuto,  "
                + " EXTRACT(HOUR FROM CURRENT_TIMESTAMP-fecha_hora_epicrisis) as resta_hora,  "
                + " EXTRACT(DAY FROM CURRENT_TIMESTAMP-fecha_hora_epicrisis) as resta_dia,"
                + " EXTRACT(SECOND FROM CURRENT_TIMESTAMP-fecha_hora_epicrisis) as resta_segundo"
                + " from schema_uo.epicrisis AA where AA.id_duo='" + duo + "'");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {

                dia = cnn.getRst().getInt("resta_dia");
                hora = cnn.getRst().getInt("resta_hora");
                minuto = cnn.getRst().getInt("resta_minuto");
                segundo = cnn.getRst().getInt("resta_segundo");

                if (dia > 0) {
                    if (dia == 1) {
                        tiempo = dia + " día ";
                    } else {
                        tiempo = dia + " días ";
                    }
                }

                if (hora < 10) {
                    tiempo += "0" + hora + ":";
                } else {
                    tiempo += "" + hora + ":";
                }

                if (minuto < 10) {
                    tiempo += "0" + minuto + ":";
                } else {
                    tiempo += "" + minuto + ":";
                }

                if (segundo < 10) {
                    tiempo += "0" + segundo;
                } else {
                    tiempo += "" + segundo;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return tiempo;

    }

    public ArrayList lista_perfil() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " id_perfil, descripcion_perfil, "
                + " estado_perfil, tipo_perfil "
                + " FROM  schema_uo.perfil where estado_perfil='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cPerfil per = new cPerfil();
                per.setDescripcion_perfil(cnn.getRst().getString("descripcion_perfil"));
                per.setEstado_perfil(cnn.getRst().getInt("estado_perfil"));
                per.setTipo_perfil(cnn.getRst().getInt("tipo_perfil"));
                per.setId_perfil(cnn.getRst().getInt("id_perfil"));

                lista.add(per);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_nacion() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT nac_id,nac_descripcion,nac_estado "
                + " FROM schemaoirs.nacion where nac_estado='1' order by nac_descripcion ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cNacion nac = new cNacion();
                nac.setDescripcion_nac(cnn.getRst().getString("nac_descripcion"));
                nac.setId_nac(cnn.getRst().getInt("nac_id"));
                nac.setEstado_nac(cnn.getRst().getInt("nac_estado"));

                lista.add(nac);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cUsuario obtiene_funcionario_rrhh(String rut) {
        cUsuario aux = new cUsuario();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  rut_funcionario, nombres_funcionario, "
                + "  apellido_p_funcionario, apellido_m_funcionario "
                + " FROM  schema_rrhh.funcionario  where  rut_funcionario='" + rut + "' ;");

        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                aux.setApellidom_usuario(cnn.getRst().getString("apellido_m_funcionario"));
                aux.setApellidop_usuario(cnn.getRst().getString("apellido_p_funcionario"));
                aux.setNombre_usuario(cnn.getRst().getString("nombres_funcionario"));
                aux.setRut_usuario(cnn.getRst().getString("rut_funcionario"));
            } else {
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return aux;
    }

    public ArrayList obtiene_usuario_sistema(String columna, String valor) {

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT "
                + " rut_usuario,nombre_usuario, "
                + " apellidop_usuario,apellidom_usuario "
                + " FROM  schema_uo.usuario where upper(" + columna + ") LIKE '" + valor + "%';");

        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cUsuario aux = new cUsuario();
                aux.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));
                aux.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                aux.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                aux.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;

    }

    public cPaciente obtiene_paciente_prevision(String rut) {
        cPaciente pac = new cPaciente();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("select * from schema_urgencia.paciente P,"
                + "schema_urgencia.prevision_paciente PP  where P.paciente_rut='" + rut + "' and "
                + "PP.id_paciente_prevision_paciente=P.paciente_rut and PP.estado_prevision_paciente=1");
        this.cnn.conectar();

        try {
            if (cnn.getRst().next()) {
                pac.setId_prevision(cnn.getRst().getInt("id_prevision_paciente"));
                pac.setRut_paciente(cnn.getRst().getString("paciente_rut"));
            } else {
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.cnn.cerrarConexion();
        return pac;
    }

    /**
     * **09-08-2012 ***
     */
    public ArrayList lista_vista_duo(String campo_fecha, String fecha1, String fecha2, String resto_sql) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);

        this.cnn.setSentenciaSQL("SELECT duo.id_duo,e.id_epicrisis,duo.estado_duo, p.paciente_rut, "
                + " p.paciente_nombres, p.paciente_apellidop,p.paciente_apellidom, "
                + "    (CASE "
                + "  WHEN p.paciente_sexo = 1 THEN 'FEMENINO'  "
                + "  WHEN p.paciente_sexo = 0 THEN 'MASCULINO'   "
                + "  END) AS paciente_sexo, to_char(p.paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac, "
                + "  age(duo.fecha_duo, p.paciente_fecha_nac) AS paciente_edad, "
                + "  (Select con.con_descripcion from schemaoirs.consultorio_pertenencia con "
                + "  where con.con_id=p.paciente_consultorio) as con_descripcion, "
                + "  cf.codigo_fonasa_descripcion,   pp.tramo_prevision_paciente, "
                + "  to_char(fecha_hora_ing_enfermeria,'DD/MM/YYYY HH24:MI:SS') AS fecha_hora_ing_enfermeria ,e.resumen_epicrisis, "
                + "  e.diagnostico_epicrisis, e.examen_epicrisis,e.indicacion_epicrisis, "
                + "  de.descripcion_derivador, d.descripcion_destino, "
                + "  to_char(duo.fecha_duo + duo.hora_duo,'DD/MM/YYYY HH24:MI:SS') AS fecha_duo, "
                + "  to_char(e.fecha_epicrisis + e.hora_epicrisis,'DD/MM/YYYY HH24:MI:SS') AS fecha_epicrisis,  "
                + "  to_char( a.fecha_hora_alta_adm, 'DD/MM/YYYY HH24:MI:SS') AS fecha_hora_alta_adm, "
                + "  age(e.fecha_epicrisis + e.hora_epicrisis, duo.fecha_duo + duo.hora_duo) "
                + "   AS qdias_epi_duo, "
                + "  age(a.fecha_hora_alta_adm, duo.fecha_duo + duo.hora_duo) "
                + "   AS qdias_altaadm_duo, fecha_hora_alta_med_duo ,"
                + "  EXTRACT(DAY FROM (fecha_hora_alta_med_duo)-(fecha_duo+hora_duo)) as fecha_dias, "
                + "  e.rut_usuario, gg.nombre_usuario, gg.apellidop_usuario, gg.apellidom_usuario "
                + "   FROM schema_urgencia.paciente p, "
                + "  schema_uo.destino d, schema_uo.duo duo, schema_uo.epicrisis e, "
                + "  schema_uo.alta_adm a,schema_uo.derivador de, "
                + "  schema_urgencia.prevision_paciente pp,schema_urgencia.codigo_fonasa cf, "
                + "  schema_uo.ing_enfermeria ie,schema_uo.usuario gg "
                + "    WHERE p.paciente_rut = duo.rut_paciente AND "
                + "   e.id_duo = duo.id_duo AND  a.id_destino = d.id_destino AND "
                + "   a.id_duo = duo.id_duo AND  duo.id_derivador = de.id_derivador AND "
                + "   duo.id_prevision = pp.id_prevision_paciente AND "
                + "   pp.codigo_fonasa_prevision_paciente = cf.codigo_fonasa_id AND "
                + "   ie.id_duo_ing_enfermeria = duo.id_duo AND gg.rut_usuario = e.rut_usuario "
                + "   AND " + campo_fecha + " BETWEEN '" + fecha1 + " 00:00:00' AND '" + fecha2 + " 23:59:59' " + resto_sql + " "
                + "  ORDER BY duo.id_duo ");

        this.cnn.conectar();

        try {
            while (cnn.getRst().next()) {
                cVistaUoce vis = new cVistaUoce();

                vis.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));
                vis.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                vis.setCodigo_fonasa_descripcion(cnn.getRst().getString("codigo_fonasa_descripcion"));
                vis.setDescripcion_derivador(cnn.getRst().getString("descripcion_derivador"));
                vis.setDescripcion_destino(cnn.getRst().getString("descripcion_destino"));
                vis.setDiagnostico_epicrisis(cnn.getRst().getString("diagnostico_epicrisis"));
                vis.setEstado_duo(cnn.getRst().getInt("estado_duo"));
                vis.setExamen_epicrisis(cnn.getRst().getString("examen_epicrisis"));
                vis.setFecha_dias(cnn.getRst().getInt("fecha_dias") + 1);
                vis.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                vis.setFecha_epicrisis(cnn.getRst().getString("fecha_epicrisis"));
                vis.setFecha_hora_alta_adm(cnn.getRst().getString("fecha_hora_alta_adm"));
                vis.setFecha_hora_alta_med(cnn.getRst().getString("fecha_hora_alta_med_duo") + "ww");

                vis.setId_duo(cnn.getRst().getInt("id_duo"));
                vis.setId_epicrisis(cnn.getRst().getInt("id_epicrisis"));
                vis.setIndicacion_epicrisis(cnn.getRst().getString("indicacion_epicrisis"));
                vis.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                vis.setPaciente_apellidom(cnn.getRst().getString("paciente_apellidom"));
                vis.setPaciente_apellidop(cnn.getRst().getString("paciente_apellidop"));
                vis.setPaciente_consultorio_pertenencia(cnn.getRst().getString("con_descripcion"));

                vis.setPaciente_fecha_nac(cnn.getRst().getString("paciente_fecha_nac"));
                vis.setPaciente_nombres(cnn.getRst().getString("paciente_nombres"));
                vis.setPaciente_rut(cnn.getRst().getString("paciente_rut"));
                vis.setPaciente_sexo(cnn.getRst().getString("paciente_sexo"));

                vis.setFecha_ingreso_enfermeria(cnn.getRst().getString("fecha_hora_ing_enfermeria"));

                /**
                 * ******************************
                 */
                String edad = cnn.getRst().getString("paciente_edad");
                edad = edad.replace("days", "dias");
                edad = edad.replace("mons", "meses");
                edad = edad.replace("years", "años");
                edad = edad.replace("day", "dia");
                edad = edad.replace("mon", "mes");
                edad = edad.replace("year", "año");

                String qdias_epi_duo = cnn.getRst().getString("qdias_epi_duo");
                qdias_epi_duo = qdias_epi_duo.replace("days", "dias");
                qdias_epi_duo = qdias_epi_duo.replace("mons", "meses");
                qdias_epi_duo = qdias_epi_duo.replace("years", "años");
                qdias_epi_duo = qdias_epi_duo.replace("day", "dia");
                qdias_epi_duo = qdias_epi_duo.replace("mon", "mes");
                qdias_epi_duo = qdias_epi_duo.replace("year", "año");

                String qdias_altaadm_duo = cnn.getRst().getString("qdias_altaadm_duo");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("days", "dias");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("mons", "meses");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("years", "años");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("day", "dia");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("mon", "mes");
                qdias_altaadm_duo = qdias_altaadm_duo.replace("year", "año");
                /**
                 * ******************************
                 */
                vis.setPaciente_edad(edad);
                vis.setQdias_altaadm_duo(qdias_altaadm_duo);
                vis.setQdias_epi_duo(qdias_epi_duo);

                vis.setResumen_epicrisis(cnn.getRst().getString("resumen_epicrisis"));
                vis.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                vis.setTramo_prevision_paciente(cnn.getRst().getString("tramo_prevision_paciente"));

                lista.add(vis);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_hitos(String rut_paciente) {

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  SELECT "
                + " AA.hit_id,hit_tipo, hit_descripcion,to_char( hit_fecha,'DD/MM/YYYY HH24:MI:SS') AS hit_fecha,hit_paciente_rut, "
                + " hit_detalle,AA.hit_estado,hit_usuario,hit_ip, "
                + " paciente_nombres,paciente_apellidop,paciente_apellidom, "
                + " rut_usuario,nombre_usuario,apellidop_usuario,apellidom_usuario "
                + " FROM  schema_uo.hito_paciente AA "
                + " JOIN schema_uo.hito_tipo BB ON (AA.hit_tipo=BB.hit_id)  "
                + " JOIN schema_urgencia.paciente CC ON (AA.hit_paciente_rut=CC.paciente_rut) "
                + " JOIN schema_uo.usuario DD ON (AA.hit_usuario=DD.rut_usuario)  "
                + " where hit_paciente_rut='" + rut_paciente + "' and AA.hit_estado='1' "
                + " order by hit_fecha desc ;");

        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cHito aux = new cHito();
                aux.setId(cnn.getRst().getInt("hit_id"));
                aux.setTipo(cnn.getRst().getInt("hit_tipo"));
                aux.setFecha(cnn.getRst().getString("hit_fecha"));
                aux.setDetalle(cnn.getRst().getString("hit_detalle"));
                aux.setEstado(cnn.getRst().getInt("hit_estado"));
                aux.setIp(cnn.getRst().getString("hit_ip"));
                aux.setUsuario_rut(cnn.getRst().getString("rut_usuario"));
                aux.setUsuario_nombre(cnn.getRst().getString("nombre_usuario"));
                aux.setUsuario_apellidop(cnn.getRst().getString("apellidop_usuario"));
                aux.setUsuario_apellidom(cnn.getRst().getString("apellidom_usuario"));

                aux.setEstado_descripcion("---");
                aux.setTipo_descripcion(cnn.getRst().getString("hit_descripcion"));

                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;

    }

    public ArrayList lista_tipo_hito() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT "
                + " hit_id,hit_descripcion,hit_estado, "
                + " hit_fecha_creacion FROM schema_uo.hito_tipo where hit_estado='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cHito aux = new cHito();
                aux.setTipo(cnn.getRst().getInt("hit_id"));
                aux.setTipo_descripcion(cnn.getRst().getString("hit_descripcion"));

                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    /**
     * ** 09-08-2012 ***
     */
    public ArrayList lista_historial_consultorio(String rut_paciente) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " his_id,his_consultorio_anterior, "
                + " his_consultorio_nuevo,to_char( his_fecha,'DD/MM/YYYY HH24:MI:SS') as his_fecha, "
                + " his_usuario, his_ip,BB.nombre_usuario,BB.apellidop_usuario,BB.apellidom_usuario "
                + " FROM schema_uo.historial_consultorio_pertenencia AA  "
                + " JOIN schema_uo.usuario BB ON (AA.his_usuario=BB.rut_usuario) "
                + " WHERE  his_paciente_rut='" + rut_paciente + "' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cHistorial_Consultorio aux = new cHistorial_Consultorio();
                aux.setHis_consultorio_anterior_descripcion(cnn.getRst().getString(""));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    //12082014
    public cIngresoEnfermeria obtiene_ingreso_enfermeria(int id_duo) {
        cIngresoEnfermeria enf = new cIngresoEnfermeria();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_ing_enfermeria, fecha_hora_ing_enfermeria,\n"
                + "  otro_comorbilidad_ing_enfermeria,farmaco_ing_enfermeria,\n"
                + "  obs_ing_enfermeria, rut_usuario_ing_enfermeria,\n"
                + " id_examen_fisico_ing_enfermeria, id_duo_ing_enfermeria,\n"
                + "  otro_ex_docto_ing_enfermeria,\n"
                + "  nombre_usuario,apellidop_usuario,apellidom_usuario,\n"
                + "  conciencia_ex_fisico,cabeza_ex_fisico, mucosa_ex_fisico,\n"
                + "  torax_ex_fisico,abdomen_ex_fisico, eess_ex_fisico,\n"
                + "  eeii_ex_fisico, z_sacra_ex_fisico, peso_ex_fisico,\n"
                + "  talla_ex_fisico, pulso_ex_fisico, presion_a_ex_fisico,\n"
                + "  temp_ex_fisico,satura_ex_fisico, vvp1_ex_fisico,\n"
                + "  vvp2_ex_fisico, vvc_ex_fisico, sng_ex_fisico,\n"
                + "  s_foley_ex_fisico, dorso_lumbar_ex_fisico, piel_ex_fisico\n"
                + "FROM  schema_uo.ing_enfermeria ENF INNER JOIN \n"
                + "schema_uo.ex_fisico FIS  ON (ENF.id_examen_fisico_ing_enfermeria=FIS.id_ex_fisico)\n"
                + "INNER JOIN schema_uo.usuario USU ON (ENF.rut_usuario_ing_enfermeria=USU.rut_usuario)\n"
                + "WHERE  id_duo_ing_enfermeria='" + id_duo + "' LIMIT 1");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                enf.setId_ing_enfermeria(cnn.getRst().getInt("id_ing_enfermeria"));
                enf.setId_examen_fisico_ing_enfermeria(cnn.getRst().getInt("id_examen_fisico_ing_enfermeria"));
                enf.setId_duo_ing_enfermeria(cnn.getRst().getInt("id_duo_ing_enfermeria"));

                enf.setFecha_hora_ing_enfermeria(cnn.getRst().getString("fecha_hora_ing_enfermeria"));
                enf.setOtro_comorbilidad_ing_enfermeria(cnn.getRst().getString("otro_comorbilidad_ing_enfermeria"));
                enf.setFarmaco_ing_enfermeria(cnn.getRst().getString("farmaco_ing_enfermeria"));
                enf.setObs_ing_enfermeria(cnn.getRst().getString("obs_ing_enfermeria"));

                enf.setRut_usuario(cnn.getRst().getString("rut_usuario_ing_enfermeria"));
                enf.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                enf.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                enf.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));

                enf.setOtro_ex_docto_ing_enfermeria(cnn.getRst().getString("otro_ex_docto_ing_enfermeria"));
                //ex fisico
                enf.setConciencia_ex_fisico(cnn.getRst().getString("conciencia_ex_fisico"));
                enf.setCabeza_ex_fisico(cnn.getRst().getString("cabeza_ex_fisico"));
                enf.setMucosa_ex_fisico(cnn.getRst().getString("mucosa_ex_fisico"));
                enf.setTorax_ex_fisico(cnn.getRst().getString("torax_ex_fisico"));
                enf.setAbdomen_ex_fisico(cnn.getRst().getString("abdomen_ex_fisico"));
                enf.setEess_ex_fisico(cnn.getRst().getString("eess_ex_fisico"));
                enf.setEeii_ex_fisico(cnn.getRst().getString("eeii_ex_fisico"));
                enf.setZ_sacra_ex_fisico(cnn.getRst().getString("z_sacra_ex_fisico"));
                enf.setPeso_ex_fisico(cnn.getRst().getString("peso_ex_fisico"));
                enf.setTalla_ex_fisico(cnn.getRst().getString("talla_ex_fisico"));
                enf.setPulso_ex_fisico(cnn.getRst().getString("pulso_ex_fisico"));
                enf.setPresion_a_ex_fisico(cnn.getRst().getString("presion_a_ex_fisico"));
                enf.setTemp_ex_fisico(cnn.getRst().getString("temp_ex_fisico"));
                enf.setSatura_ex_fisico(cnn.getRst().getString("satura_ex_fisico"));
                enf.setVvp1_ex_fisico(cnn.getRst().getString("vvp1_ex_fisico"));
                enf.setVvp2_ex_fisico(cnn.getRst().getString("vvp2_ex_fisico"));
                enf.setVvc_ex_fisico(cnn.getRst().getString("vvc_ex_fisico"));
                enf.setSng_ex_fisico(cnn.getRst().getString("sng_ex_fisico"));
                enf.setS_foley_ex_fisico(cnn.getRst().getString("s_foley_ex_fisico"));
                enf.setDorso_lumbar_ex_fisico(cnn.getRst().getString("dorso_lumbar_ex_fisico"));
                enf.setPiel_ex_fisico(cnn.getRst().getString("piel_ex_fisico"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return enf;
    }

    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    /**
     * *************************HOSPITALIZACION
     * SUAM************************************
     */
    public cDau obtiene_paciente_segun_dau(int dau_id) {
        cDau aux = new cDau();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + "  dau_id,AA.paciente_rut, admisor_rut, "
                + " AA.tipo_dau_id,CC.tipo_dau_descripcion, "
                + "  AA.medio_id,DD.medio_descripcion, "
                + "  AA.unidad_ingreso_id,EE.unidad_ingreso_descripcion, "
                + "  dau_estado,(CASE WHEN  dau_estado=0 THEN 'Inactivo'  "
                + "  WHEN  dau_estado=1 THEN 'Activo' "
                + "  WHEN  dau_estado=2 THEN 'Anulado' END)as estado_descripcion,"
                + "  dau_clasificacion, "
                + "  to_char(dau_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as dau_fecha_ingreso, "
                + "  to_char(dau_fecha_clasificacion,'DD/MM/YYYY HH24:MI:SS')as dau_fecha_clasificacion, "
                + "  to_char(dau_fecha_hora_atencion,'DD/MM/YYYY HH24:MI:SS')as dau_fecha_hora_atencion, "
                + "  to_char(dau_fecha_egreso_med,'DD/MM/YYYY HH24:MI:SS')as  dau_fecha_egreso_med , "
                + "   to_char(dau_fecha_clasifica_enf,'DD/MM/YYYY HH24:MI:SS')as  dau_fecha_clasifica_enf, "
                + "  dau_vif,rut_medico,brazalete,dau_rut_usuario_clasificacion, "
                + "  dau_categorizado, dau_destino,dau_recupera, "
                + "  dau_fallece, dau_observa,dau_clasifica_enf, "
                + "  dau_diag_alta, "
                + "  BB.paciente_nombres, BB.paciente_apellidop,BB.paciente_apellidom, "
                + "  age(AA.dau_fecha_ingreso, BB.paciente_fecha_nac) AS paciente_edad, "
                + "  BB.paciente_sexo, "
                + "  (CASE WHEN BB.paciente_sexo=1 THEN 'Femenino'  "
                + "  WHEN BB.paciente_sexo=0 THEN 'Masculino'  "
                + "   END) AS genero "
                + " FROM  schema_urgencia.dau AA "
                + " JOIN schema_urgencia.paciente BB ON (AA.paciente_rut=BB.paciente_rut)  "
                + " JOIN schema_urgencia.tipo_dau CC ON (CC.tipo_dau_id=AA.tipo_dau_id) "
                + " JOIN schema_urgencia.medio DD ON (AA.medio_id=DD.medio_id) "
                + " JOIN schema_urgencia.unidad_ingreso EE ON(AA.unidad_ingreso_id=EE.unidad_ingreso_id) "
                + " WHERE AA.dau_id='" + dau_id + "' "
                + " limit 1 ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {

                aux.setId_dau(cnn.getRst().getInt("dau_id"));
                aux.setDau_estado(cnn.getRst().getInt("dau_estado"));
                aux.setDau_estado_descri(cnn.getRst().getString("estado_descripcion"));
                aux.setTipo_dau_id(cnn.getRst().getInt("tipo_dau_id"));
                aux.setTipo_dau_descri(cnn.getRst().getString("tipo_dau_descripcion"));
                aux.setMedio_id(cnn.getRst().getInt("medio_id"));
                aux.setMedio_descri(cnn.getRst().getString("medio_descripcion"));
                aux.setUnidad_ingreso_id(cnn.getRst().getInt("unidad_ingreso_id"));
                aux.setUnidad_ingreso_descri(cnn.getRst().getString("unidad_ingreso_descripcion"));
                aux.setDau_fecha_ingreso(cnn.getRst().getString("dau_fecha_ingreso"));
                aux.setDau_fecha_hora_atencion(cnn.getRst().getString("dau_fecha_hora_atencion"));
                aux.setDau_fecha_egreso_med(cnn.getRst().getString("dau_fecha_egreso_med"));
                aux.setDau_fecha_clasifica_enf(cnn.getRst().getString("dau_fecha_clasifica_enf"));
                aux.setDau_fecha_clasificacion(cnn.getRst().getString("dau_fecha_clasificacion"));
                aux.setDau_clasificacion(cnn.getRst().getInt("dau_clasificacion"));
                aux.setDau_clasifica_enf(cnn.getRst().getInt("dau_clasifica_enf"));
                aux.setAdmisor_rut(cnn.getRst().getString("admisor_rut"));
                aux.setRut_medico(cnn.getRst().getString("rut_medico"));
                aux.setDau_vif(cnn.getRst().getInt("dau_vif"));
                aux.setBrazalete(cnn.getRst().getInt("brazalete"));
                aux.setDau_rut_usuario_clasificacion(cnn.getRst().getString("dau_rut_usuario_clasificacion"));
                aux.setDau_categorizado(cnn.getRst().getInt("dau_categorizado"));
                aux.setDau_destino(cnn.getRst().getInt("dau_destino"));
                aux.setDau_recupera(cnn.getRst().getInt("dau_recupera"));
                aux.setDau_fallece(cnn.getRst().getInt("dau_fallece"));
                aux.setDau_observa(cnn.getRst().getInt("dau_observa"));
                aux.setDau_diag_alta(cnn.getRst().getInt("dau_diag_alta"));
//aux.setAdmisor_nombre(cnn.getRst().getString(""));
//aux.setAdmisor_apellidop(cnn.getRst().getString(""));
//aux.setAdmisor_apellidom(cnn.getRst().getString(""));
//aux.setMedico_nombre(cnn.getRst().getString(""));
//aux.setMedico_apellidop(cnn.getRst().getString(""));
//aux.setMedico_apellidom(cnn.getRst().getString(""));
//aux.setDau_clasifica_enf_descri(cnn.getRst().getString(""));
//aux.setDau_clasifica_med_descri(cnn.getRst().getString(""));

                aux.setRut_paciente(cnn.getRst().getString("paciente_rut"));
                aux.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                aux.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                aux.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                aux.setSexo(cnn.getRst().getInt("paciente_sexo"));
                aux.setSexo_descri(cnn.getRst().getString("genero"));

                String edad = cnn.getRst().getString("paciente_edad");
                edad = edad.replace("days", "dias");
                edad = edad.replace("mons", "meses");
                edad = edad.replace("years", "años");
                edad = edad.replace("day", "dia");
                edad = edad.replace("mon", "mes");
                edad = edad.replace("year", "año");
                aux.setEdad(edad);
                try {
                    edad = edad.substring(0, edad.length() - 8);
                    aux.setEdad(edad);
                } catch (Exception ex) {
                }

                // viene de dau_clasifica_enf u corresponde a e1,e2,e3,e4,e5 y s/n
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return aux;
    }

    public cDau obtiene_paciente_segun_dauNN(int dau_nn_id) {
        cDau aux = new cDau();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " dau_nn_id,AA.paciente_nn_id,admisor_rut, "
                + "  AA.tipo_dau_id,CC.tipo_dau_descripcion, "
                + "  AA.medio_id,DD.medio_descripcion, "
                + "  AA.unidad_ingreso_id,EE.unidad_ingreso_descripcion, "
                + "  to_char(dau_nn_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as dau_nn_fecha_ingreso , "
                + "  to_char(dau_nn_fecha_clasificacion,'DD/MM/YYYY HH24:MI:SS')as dau_nn_fecha_clasificacion, "
                + "  prevision_nn_id,dau_nn_clasificacion, dau_nn_vif,dau_nn_impresion, "
                + "  dau_nn_estado, "
                + "  (CASE WHEN  dau_nn_estado=0 THEN 'Inactivo'   "
                + " WHEN  dau_nn_estado=1 THEN 'Activo'  "
                + "  WHEN dau_nn_estado=2 THEN 'Anulado' END)as estado_descripcion, "
                + "  brazalete,BB.paciente_nn_ano,BB.paciente_nn_apellidom, "
                + "  BB.paciente_nn_apellidop,BB.paciente_nn_descripcion, "
                + "  BB.paciente_nn_edad,BB.paciente_nn_nombres,BB.paciente_nn_sexo, "
                + "  (CASE WHEN BB.paciente_nn_sexo=1 THEN 'Femenino'  "
                + "   WHEN BB.paciente_nn_sexo=0 THEN 'Masculino'  "
                + "   END) AS genero, "
                + "  BB.paciente_nn_telefono1 "
                + "  FROM schema_urgencia.dau_nn AA  "
                + "  JOIN schema_urgencia.paciente_nn BB ON (AA.paciente_nn_id=BB.paciente_nn_id)  "
                + "  JOIN schema_urgencia.tipo_dau CC ON (CC.tipo_dau_id=AA.tipo_dau_id) "
                + "  JOIN schema_urgencia.medio DD ON (AA.medio_id=DD.medio_id) "
                + "  JOIN schema_urgencia.unidad_ingreso EE ON(AA.unidad_ingreso_id=EE.unidad_ingreso_id) "
                + "  where dau_nn_id='" + dau_nn_id + "'   limit 1 ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                aux.setPaciente_descripcion(cnn.getRst().getString("paciente_nn_descripcion"));

                aux.setId_dau(cnn.getRst().getInt("dau_nn_id"));
                //paciente_nn_id
                aux.setDau_estado(cnn.getRst().getInt("dau_nn_estado"));
                aux.setDau_estado_descri(cnn.getRst().getString("estado_descripcion"));
                aux.setTipo_dau_id(cnn.getRst().getInt("tipo_dau_id"));
                aux.setTipo_dau_descri(cnn.getRst().getString("tipo_dau_descripcion"));
                aux.setMedio_id(cnn.getRst().getInt("medio_id"));
                aux.setMedio_descri(cnn.getRst().getString("medio_descripcion"));
                aux.setUnidad_ingreso_id(cnn.getRst().getInt("unidad_ingreso_id"));
                aux.setUnidad_ingreso_descri(cnn.getRst().getString("unidad_ingreso_descripcion"));
                aux.setDau_fecha_ingreso(cnn.getRst().getString("dau_nn_fecha_ingreso"));
                aux.setDau_fecha_clasificacion(cnn.getRst().getString("dau_nn_fecha_clasificacion"));
                aux.setDau_clasificacion(cnn.getRst().getInt("dau_nn_clasificacion"));
                aux.setDau_vif(cnn.getRst().getInt("dau_nn_vif"));
                aux.setBrazalete(cnn.getRst().getInt("brazalete"));
                aux.setAdmisor_rut(cnn.getRst().getString("admisor_rut"));

//               // aux.setRut_paciente(cnn.getRst().getString("paciente_rut"));
                aux.setNombres_paciente(cnn.getRst().getString("paciente_nn_nombres"));
                aux.setApellidop_paciente(cnn.getRst().getString("paciente_nn_apellidop"));
                aux.setApellidom_paciente(cnn.getRst().getString("paciente_nn_apellidom"));
                aux.setSexo(cnn.getRst().getInt("paciente_nn_sexo"));
                aux.setSexo_descri(cnn.getRst().getString("genero"));

                String edad = cnn.getRst().getString("paciente_nn_edad");
                // paciente_nn_ano;
                aux.setEdad(edad);

                aux.setTelefono1(cnn.getRst().getString("paciente_nn_telefono1"));
                aux.setTelefono2("");
                aux.setDireccion("");
                aux.setComuna_descri("");
                aux.setFecha_nac("");
                aux.setPaciente_descripcion(cnn.getRst().getString("paciente_nn_descripcion"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return aux;
    }

    public ArrayList lista_derivador_suam() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT der_id,der_descripcion "
                + " FROM schema_suam.derivador WHERE der_estado=1 ORDER BY  der_id;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cConsultorio aux = new cConsultorio();
                aux.setDescripcion(cnn.getRst().getString("der_descripcion"));
                aux.setId(cnn.getRst().getInt("der_id"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_destino_suam() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT des_id,des_descripcion,des_indicacionegreso,des_alta "
                + " FROM  schema_suam.destino WHERE  des_estado=1"
                + " order by des_orden");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cConsultorio aux = new cConsultorio();
                aux.setDescripcion(cnn.getRst().getString("des_descripcion"));
                aux.setId(cnn.getRst().getInt("des_id"));
                aux.setIndicacionEgreso(cnn.getRst().getInt("des_indicacionegreso"));
                aux.setAlta(cnn.getRst().getInt("des_alta"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_medicos_suam() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " usuario_urgencia_rut,usuario_urgencia_apellidop, "
                + " usuario_urgencia_apellidom,usuario_urgencia_nombres,perfil_urgencia_id "
                + " FROM  schema_urgencia.usuario_urgencia where usuario_urgencia_doc='1' and "
                + " usuario_urgencia_estado=1 "
                + " order by usuario_urgencia_apellidop,usuario_urgencia_apellidom, "
                + " usuario_urgencia_nombres ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cUsuario aux = new cUsuario();
                aux.setRut_usuario(cnn.getRst().getString("usuario_urgencia_rut"));
                aux.setApellidom_usuario(cnn.getRst().getString("usuario_urgencia_apellidom"));
                aux.setApellidop_usuario(cnn.getRst().getString("usuario_urgencia_apellidop"));
                aux.setNombre_usuario(cnn.getRst().getString("usuario_urgencia_nombres"));
                lista.add(aux);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_diagnostico_suam(int id_das, String tipo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " dia_id,das_id,dia_usuario, "
                + " dia_estado,dia_ip,dia_tipo,dia_detalle, "
                + " to_char(dia_ingreso,'DD/MM/YYYY HH24:MI:SS')as dia_ingreso, "
                + " to_char(dia_ingreso,'DD/MM HH24:MI')as dia_ingreso_short "
                + " FROM  schema_suam.diagnostico where dia_estado='1' and das_id='" + id_das + "'  "
                + " and dia_tipo IN (" + tipo + ") ");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("dia_id"));
                diag.setId_duo(cnn.getRst().getInt("das_id"));
                diag.setTipo_diagnostico(cnn.getRst().getInt("dia_tipo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("dia_detalle"));
                diag.setFecha_ingreso(cnn.getRst().getString("dia_ingreso"));

                diag.setRut_usuario(cnn.getRst().getString("dia_usuario"));
                diag.setFecha_corta(cnn.getRst().getString("dia_ingreso_short"));
                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_observacion_suam(int id_das) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  obs_id, obs_usuario, "
                + "to_char(obs_ingreso,'DD/MM/YYYY HH24:MI:SS')as obs_ingreso ,"
                + " to_char(obs_ingreso,'DD/MM HH24:MI')as obs_ingreso_short ,  "
                + " obs_estado, obs_ip, obs_espera_radiografia, obs_espera_ex_laboratorio, obs_detalle "
                + " FROM   schema_suam.observacion WHERE das_id='" + id_das + "' and obs_estado='1'  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cObservacion obs = new cObservacion();
                obs.setId_obs(cnn.getRst().getInt("obs_id"));
                obs.setEspera_radiografia(cnn.getRst().getInt("obs_espera_radiografia"));
                obs.setEspera_ex_laboratorio(cnn.getRst().getInt("obs_espera_ex_laboratorio"));

                if (obs.getEspera_ex_laboratorio() == 1) {
                    obs.setEspera_ex_laboratorio_descri("SI");
                } else {
                    obs.setEspera_ex_laboratorio_descri("NO");
                }

                if (obs.getEspera_radiografia() == 1) {
                    obs.setEspera_radiografia_descri("SI");
                } else {
                    obs.setEspera_radiografia_descri("NO");
                }

                obs.setRut_usuario(cnn.getRst().getString("obs_usuario"));
                obs.setObservacion_detalle(cnn.getRst().getString("obs_detalle"));
                obs.setFecha_ingreso(cnn.getRst().getString("obs_ingreso"));
                obs.setFecha_corta(cnn.getRst().getString("obs_ingreso_short"));

                lista.add(obs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_motivos_consulta_segun_dau(int id_dau, int modo) {
        String esNN = "";
        if (modo == 2) {
            esNN = "nn_";
        }
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT dau_" + esNN + "id,BB.motivo_consulta_id,BB.motivo_consulta_descripcion_col "
                + " FROM schema_urgencia.dau_" + esNN + "motivo_consulta AA "
                + " JOIN schema_urgencia.motivo_consulta BB  "
                + " ON(AA.motivo_consulta_id=BB.motivo_consulta_id) "
                + " WHERE dau_" + esNN + "id IN (" + id_dau + ")");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("motivo_consulta_id"));
                diag.setId_duo(cnn.getRst().getInt("dau_" + esNN + "id"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("motivo_consulta_descripcion_col"));

                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    /*ista_documentos_paciente_suACTUALES_SUAM 01-02-2013*/
    public ArrayList lista_pacientes_actuales_suam() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("Select das.das_id,das.das_paciente,das.das_camilla, "
                + " pac.paciente_nombres,     "
                + " pac.paciente_apellidop,pac.paciente_apellidom,    "
                + " to_char(pac.paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac,    "
                + " age(das.das_fecha_ingreso, pac.paciente_fecha_nac) AS paciente_edad,  "
                + " pac.paciente_sexo, (CASE WHEN paciente_sexo=1 THEN 'FEMENINO'    "
                + " WHEN paciente_sexo=0 THEN 'MASCULINO' END) AS genero  "
                + " from schema_suam.das das "
                + " JOIN schema_urgencia.paciente pac ON (das.das_paciente=pac.paciente_rut)  "
                + " WHERE das.das_estado IN (1,2,3)");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDas das = new cDas();
                das.setId_das(cnn.getRst().getInt("das_id"));
                das.setCamilla(cnn.getRst().getInt("das_camilla"));

                das.setRut_paciente(cnn.getRst().getString("das_paciente"));
                das.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                das.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                das.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                das.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));

                String edad = cnn.getRst().getString("paciente_edad");
                if (edad != null) {
                    edad = edad.replace("days", "dias.z");
                    edad = edad.replace("mons", "meses");
                    edad = edad.replace("years", "años");
                    edad = edad.replace("day", "dia.z");
                    edad = edad.replace("mon", "mes");
                    edad = edad.replace("year", "año");
                    das.setEdad(edad);
                } else {
                    das.setEdad("00");
                }
                try {
                    edad = edad.substring(0, edad.indexOf("z") - 1);
                    das.setEdad(edad);
                } catch (Exception ex) {
                }

                lista.add(das);

            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_camillas_actuales() {
        ArrayList lista = new ArrayList();
        ArrayList obtiene_datos_paciente = this.lista_pacientes_actuales_suam();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " cam_id,cam_descripcion,cam_estado,     "
                + " sala.sal_id, sala.sal_descripcion,   "
                + " das.das_id,das.das_estado,    "
                + "  das.dau_id,das.dau_nn_id,     "
                + "  das.das_paciente, "
                + " to_char( das.das_fecha_ingreso,'DD/MM HH24:MI' )as das_fecha_ingreso,    "
                + "  EXTRACT(SECOND FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_ss,    "
                + "  EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_mm,    "
                + "  EXTRACT(HOUR FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_hh,   "
                + " EXTRACT(DAY FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_dd,    "
                + " usu.usuario_urgencia_nombres,usu.usuario_urgencia_apellidop,   "
                + " usu.usuario_urgencia_apellidom,    "
                + " usuario.nombre_usuario,usuario.apellidop_usuario,usuario.apellidom_usuario,  "
                + " des_descripcion       "
                + " FROM  schema_suam.camilla cam "
                + " LEFT JOIN schema_suam.das das ON das.das_camilla=cam.cam_id   "
                + " AND (das.das_estado=1 or das.das_estado=2 or das.das_estado=3)    "
                + " LEFT JOIN schema_urgencia.usuario_urgencia usu ON (das.das_medico=usu.usuario_urgencia_rut)     "
                + " LEFT JOIN schema_uo.usuario usuario ON (das.das_usuario=usuario.rut_usuario)    "
                + " LEFT JOIN schema_suam.destino des ON (das.das_indicacion_destino=des.des_id)    "
                + " JOIN schema_suam.sala sala ON (cam.sal_id=sala.sal_id)  "
                + " WHERE cam_estado='1' "
                + " order by cam_id");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {

                Iterator paci = obtiene_datos_paciente.iterator();

                cDas das = new cDas();

                das.setId_das(cnn.getRst().getInt("das_id"));
                das.setCamilla(cnn.getRst().getInt("cam_id"));
                das.setCamilla_descri(cnn.getRst().getString("cam_descripcion"));
                das.setSala(cnn.getRst().getInt("sal_id"));
                das.setSala_descri(cnn.getRst().getString("sal_descripcion"));

                das.setEstado(cnn.getRst().getInt("das_estado"));
                das.setEstado_descri("");
                das.setDau_id(cnn.getRst().getInt("dau_id"));
                das.setDau_nn_id(cnn.getRst().getInt("dau_nn_id"));
                das.setFecha_ingreso(cnn.getRst().getString("das_fecha_ingreso"));


                /*SETEO DATOS PACIENTE 01-02-2013*/
                while (paci.hasNext()) {
                    cDas itera = (cDas) paci.next();

                    if (itera.getId_das() == das.getId_das()) {
                        das.setRut_paciente(itera.getRut_paciente());
                        das.setNombres_paciente(itera.getNombres_paciente());
                        das.setApellidop_paciente(itera.getApellidop_paciente());
                        das.setApellidom_paciente(itera.getApellidom_paciente());
                        das.setFecha_nac(itera.getFecha_nac());
                        das.setEdad(itera.getEdad());
                    }

                }

                /*FIN SETEO DATOS PACIENTE 01-20-2013*/
                das.setNombre_medico(cnn.getRst().getString("usuario_urgencia_nombres"));
                das.setApellidop_medico(cnn.getRst().getString("usuario_urgencia_apellidop"));
                das.setApellidom_medico(cnn.getRst().getString("usuario_urgencia_apellidom"));

                das.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                das.setApellidom_usuario(cnn.getRst().getString("apellidop_usuario"));
                das.setApellidop_usuario(cnn.getRst().getString("apellidom_usuario"));

                das.setDif_dd(cnn.getRst().getInt("dif_dd"));
                das.setDif_hh(cnn.getRst().getInt("dif_hh"));
                das.setDif_mm(cnn.getRst().getInt("dif_mm"));
                das.setDif_ss(cnn.getRst().getInt("dif_ss"));

                das.setIndicacion_destino_descri(cnn.getRst().getString("des_descripcion"));
                if (das.getIndicacion_destino_descri() == null) {
                    das.setIndicacion_destino_descri("");
                }

                if (das.getDau_id() == 0) {
                    das.setNombres_paciente("PACIENTE NN");
                    das.setApellidom_paciente("");
                    das.setApellidop_paciente("");
                    das.setEdad("");
                    das.setRut_paciente("");
                }

                lista.add(das);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;

    }

    public cDas obtiene_das(int id_das) {
        cDas das = new cDas();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " cam_id,cam_descripcion,cam_estado,   "
                + " sala.sal_id, sala.sal_descripcion,   "
                + " das.das_id,das.das_estado,   "
                + "  das.dau_id,das.dau_nn_id,   "
                + "  das.das_paciente,pac.paciente_nombres,   "
                + " pac.paciente_apellidop,pac.paciente_apellidom,   "
                + "  to_char( das.das_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS' )as das_fecha_ingreso,   "
                + "    EXTRACT(SECOND FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_ss,   "
                + "   EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_mm,   "
                + "   EXTRACT(HOUR FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_hh,   "
                + "  EXTRACT(DAY FROM CURRENT_TIMESTAMP-das.das_fecha_ingreso) as dif_dd,   "
                + "  to_char(pac.paciente_fecha_nac,'DD/MM/YYYY')as paciente_fecha_nac,   "
                + "   age(das.das_fecha_ingreso, pac.paciente_fecha_nac) AS paciente_edad,   "
                + "   pac.paciente_sexo, (CASE WHEN paciente_sexo=1 THEN 'FEMENINO'    "
                + "  WHEN paciente_sexo=0 THEN 'MASCULINO' END) AS genero,   "
                + "  pac.paciente_direccion,pac.paciente_telefono1, "
                + "  pac.paciente_telefono2,comuna_descripcion , "
                + "  des_id,des_descripcion , "
                + "  to_char(das.das_indicacion_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS' )as das_indicacion_fecha_ingreso, "
                + "  usuario_urgencia_rut,usuario_urgencia_nombres,usuario_urgencia_apellidop, "
                + "  usuario_urgencia_apellidom,DER.der_id,der_descripcion "
                + "  FROM  schema_suam.camilla cam    "
                + "  LEFT JOIN schema_suam.das das ON (cam.cam_id=das.das_camilla)    "
                + "  LEFT JOIN schema_urgencia.paciente pac ON (das.das_paciente=pac.paciente_rut)   "
                + "  LEFT JOIN schemaoirs.comuna comu ON (pac.comuna_codigo=comu.comuna_codigo)  "
                + "  LEFT JOIN schema_suam.destino des ON (das.das_indicacion_destino=des.des_id)  "
                + "  LEFT JOIN schema_urgencia.usuario_urgencia USU  ON (das.das_medico=USU.usuario_urgencia_rut)  "
                + "  LEFT JOIN schema_suam.derivador DER ON (das.das_derivador=DER.der_id) "
                + "  JOIN schema_suam.sala sala ON (cam.sal_id=sala.sal_id)   "
                + "  WHERE das_id='" + id_das + "'  limit 1 ;  ");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                das.setId_das(cnn.getRst().getInt("das_id"));
                das.setCamilla(cnn.getRst().getInt("cam_id"));
                das.setCamilla_descri(cnn.getRst().getString("cam_descripcion"));
                das.setSala(cnn.getRst().getInt("sal_id"));
                das.setSala_descri(cnn.getRst().getString("sal_descripcion"));

                das.setEstado(cnn.getRst().getInt("das_estado"));
                das.setEstado_descri("--- ---");

                das.setDau_id(cnn.getRst().getInt("dau_id"));
                das.setDau_nn_id(cnn.getRst().getInt("dau_nn_id"));

                das.setFecha_ingreso(cnn.getRst().getString("das_fecha_ingreso"));

                das.setRut_paciente(cnn.getRst().getString("das_paciente"));
                das.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                das.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                das.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                das.setSexo(cnn.getRst().getInt("paciente_sexo"));
                das.setSexo_descri(cnn.getRst().getString("genero"));
                String edad = cnn.getRst().getString("paciente_edad");
                if (edad != null) {
                    edad = edad.replace("days", "dias.z");
                    edad = edad.replace("mons", "meses");
                    edad = edad.replace("years", "años");
                    edad = edad.replace("day", "dia.z");
                    edad = edad.replace("mon", "mes");
                    edad = edad.replace("year", "año");
                    das.setEdad(edad);
                } else {
                    das.setEdad("00");
                }

                try {
                    edad = edad.substring(0, edad.indexOf("z") - 1);
                    das.setEdad(edad);
                } catch (Exception exx) {
                }

                das.setDireccion(cnn.getRst().getString("paciente_direccion"));

                das.setTelefono1(cnn.getRst().getString("paciente_telefono1"));
                das.setTelefono2(cnn.getRst().getString("paciente_telefono2"));
                das.setComuna_descri(cnn.getRst().getString("comuna_descripcion"));
                das.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));

                das.setDif_dd(cnn.getRst().getInt("dif_dd"));
                das.setDif_hh(cnn.getRst().getInt("dif_hh"));
                das.setDif_mm(cnn.getRst().getInt("dif_mm"));
                das.setDif_ss(cnn.getRst().getInt("dif_ss"));

                das.setIndicacion_destino_id(cnn.getRst().getInt("des_id"));
                das.setIndicacion_destino_descri(cnn.getRst().getString("des_descripcion"));
                das.setIndicacion_fecha(cnn.getRst().getString("das_indicacion_fecha_ingreso"));

                das.setNombre_medico(cnn.getRst().getString("usuario_urgencia_nombres"));
                das.setApellidop_medico(cnn.getRst().getString("usuario_urgencia_apellidop"));
                das.setApellidom_medico(cnn.getRst().getString("usuario_urgencia_apellidom"));
                das.setMedico(cnn.getRst().getString("usuario_urgencia_rut"));

                das.setDerivador(cnn.getRst().getInt("der_id"));
                das.setDerivador_descri(cnn.getRst().getString("der_descripcion"));

                if (das.getDau_id() == 0) {
                    das.setRut_paciente("NN");
                    das.setNombres_paciente("PACIENTE NN");
                    das.setApellidop_paciente("");
                    das.setApellidom_paciente("");
                    das.setEdad("");
                    das.setRut_paciente("");
                    das.setDireccion("");

                    das.setTelefono1("");
                    das.setTelefono2("");
                    das.setComuna_descri("");
                    das.setFecha_nac("");
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return das;
    }

    public cAlta_Das obtiene_alta_das(int das) {
        cAlta_Das alt = new cAlta_Das();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " alt_id,alt.das_id,alt_usuario, "
                + " usu.nombre_usuario,usu.apellidop_usuario,usu.apellidom_usuario, "
                + " to_char(alt_ingreso,'DD/MM/YYYY HH24:MI:SS' )as alt_ingreso,alt_estado,alt_destino,des_descripcion, "
                + " alt_detalle, alt_ip,alt_medico, "
                + " med.usuario_urgencia_nombres,med.usuario_urgencia_apellidop, "
                + " med.usuario_urgencia_apellidom, "
                + " EXTRACT(SECOND FROM alt_ingreso-dasi.das_fecha_ingreso) as dif_ss,   "
                + " EXTRACT(MINUTE FROM alt_ingreso-dasi.das_fecha_ingreso) as dif_mm,   "
                + " EXTRACT(HOUR FROM alt_ingreso-dasi.das_fecha_ingreso) as dif_hh,    "
                + " EXTRACT(DAY FROM alt_ingreso-dasi.das_fecha_ingreso) as dif_dd       "
                + " FROM  schema_suam.alta_das alt "
                + " JOIN schema_suam.das dasi ON (dasi.das_id=alt.das_id) "
                + " JOIN schema_uo.usuario usu ON (usu.rut_usuario=alt.alt_usuario) "
                + " JOIN schema_suam.destino des ON (des.des_id=alt.alt_destino) "
                + " LEFT JOIN schema_urgencia.usuario_urgencia med ON (med.usuario_urgencia_rut=alt.alt_medico) "
                + " where dasi.das_id='" + das + "' ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                alt.setId_das(cnn.getRst().getInt("das_id"));
                alt.setId_alta_das(cnn.getRst().getInt("alt_id"));

                alt.setDestino(cnn.getRst().getInt("alt_destino"));
                alt.setDestino_descri(cnn.getRst().getString("des_descripcion"));
                alt.setFecha_ingreso(cnn.getRst().getString("alt_ingreso"));
                alt.setDetalle(cnn.getRst().getString("alt_detalle"));

                if (alt.getDetalle() == null || alt.getDetalle().equalsIgnoreCase("null")) {

                    alt.setDetalle("");
                }

                alt.setMedico_rut(cnn.getRst().getString("alt_medico"));
                alt.setMedico_nombre(cnn.getRst().getString("usuario_urgencia_nombres"));
                alt.setMedico_apellidop(cnn.getRst().getString("usuario_urgencia_apellidop"));
                alt.setMedico_apellidom(cnn.getRst().getString("usuario_urgencia_apellidom"));

                if (alt.getMedico_rut() == null) {
                    alt.setMedico_nombre("No registrado");
                    alt.setMedico_apellidop("");
                    alt.setMedico_apellidom("");
                }

                alt.setRut_usuario(cnn.getRst().getString("alt_usuario"));
                alt.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                alt.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                alt.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));

                alt.setDif_dd(cnn.getRst().getInt("dif_dd"));
                alt.setDif_hh(cnn.getRst().getInt("dif_hh"));
                alt.setDif_mm(cnn.getRst().getInt("dif_mm"));
                alt.setDif_ss(cnn.getRst().getInt("dif_ss"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return alt;

    }

    public ArrayList lista_diagnosticos_das_en_camilla() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + " dia_id,das_id,dia_usuario, "
                + " dia_ingreso,dia_estado,dia_ip, "
                + " dia_tipo, dia_detalle "
                + " FROM  schema_suam.diagnostico "
                + " where das_id IN (select das_id from schema_suam.das where das_estado IN (1,2)) and "
                + " dia_estado='1' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("dia_id"));
                diag.setId_duo(cnn.getRst().getInt("das_id"));
                diag.setTipo_diagnostico(cnn.getRst().getInt("dia_tipo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("dia_detalle"));
                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();

        return lista;
    }

    public String dig_cero(int numero) {
        /*este metodo antepone un 0 si el numero es menor a 10 y mayor a -1*/
        String var = "";
        if (numero < 10 && numero > -1) {
            var += "0" + numero;
        } else {
            var += "" + numero;
        }
        return var;
    }

    public ArrayList listada_categorizaciones(String fecha_inicial, String fecha_final) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT "
                + " id_visita,obs_visita,to_char(fecha_visita,'DD/MM/YYYY' )as fecha_visita,hora_visita, "
                + " to_char(fecha_hora_visita,'DD/MM/YYYY HH24:MI:SS' )as fecha_hora_visita ,AA.rut_usuario, "
                + " nombre_usuario,apellidop_usuario, "
                + " AA.id_duo, descripcion_cama,BB.cat_visita_categorizacion,DD.rut_paciente, "
                + " paciente_nombres,paciente_apellidop,paciente_apellidom "
                + " FROM  "
                + " schema_uo.visita AA JOIN schema_uo.visita_categorizacion BB ON  "
                + " (AA.id_visita_categorizacion=BB.id_visita_categorizacion) "
                + " JOIN schema_uo.usuario CC ON (CC.rut_usuario=AA.rut_usuario) "
                + " JOIN schema_uo.duo DD ON (DD.id_duo=AA.id_duo) "
                + " JOIN schema_uo.cama EE ON (AA.id_cama=EE.id_cama) "
                + " JOIN schema_urgencia.paciente FF ON (DD.rut_paciente=FF.paciente_rut) "
                + " where fecha_visita BETWEEN '" + fecha_inicial + " 00:00:00' AND '" + fecha_final + " 23:59:59' "
                + " order by AA.id_cama");

        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDuo duo = new cDuo();
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                duo.setFecha_duo(cnn.getRst().getString("fecha_visita"));
                duo.setHora_duo(cnn.getRst().getString("hora_visita"));
                duo.setFecha_creacion(cnn.getRst().getString("fecha_hora_visita"));
// utilizo esta clase y mutadores por falta de tiempo para acomodar la cVisita 30-08-2012 18:14
                duo.setDerivador_descripcion(cnn.getRst().getString("obs_visita"));

                // duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                // duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                //duo.setCama(cnn.getRst().getInt("id_cama"));
                duo.setCama_descripcion(cnn.getRst().getString("descripcion_cama"));

                // duo.setFecha_hora_ing_duo(cnn.getRst().getString("fecha_hora_ing_duo"));
                duo.setCategorizacion_id(cnn.getRst().getInt("id_visita"));
                duo.setCategorizacion_descripcion(cnn.getRst().getString("cat_visita_categorizacion"));

                duo.setRut_paciente(cnn.getRst().getString("rut_paciente"));
                duo.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                duo.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                duo.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));

                duo.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                duo.setNombre_usuario_admision(cnn.getRst().getString("nombre_usuario") + " " + cnn.getRst().getString("apellidop_usuario"));
//                duo.setTelefono1(cnn.getRst().getString("paciente_telefono1"));
//                duo.setTelefono2(cnn.getRst().getString("paciente_telefono2"));
//                duo.setFecha_nac(cnn.getRst().getString("paciente_fecha_nac"));

                lista.add(duo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public cDuo obtiene_fecha_enfermeria_medico_ingreso(int numero_duo) {
        cDuo duo = new cDuo();

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("  Select fecha_hora_ing_med as fecha, "
                + " EXTRACT(SECOND FROM CURRENT_TIMESTAMP-fecha_hora_ing_med) as dif_ss,   "
                + " EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-fecha_hora_ing_med) as dif_mm,    "
                + " EXTRACT(HOUR FROM CURRENT_TIMESTAMP-fecha_hora_ing_med)as dif_hh,    "
                + " EXTRACT(DAY FROM CURRENT_TIMESTAMP-fecha_hora_ing_med)as dif_dd, "
                + " 'medico' as tipo "
                + " from schema_uo.duo where id_duo='" + numero_duo + "' "
                + " UNION "
                + " SELECT  "
                + " fecha_hora_ing_enfermeria as fecha, "
                + " EXTRACT(SECOND FROM CURRENT_TIMESTAMP-fecha_hora_ing_enfermeria) as dif_ss,   "
                + " EXTRACT(MINUTE FROM CURRENT_TIMESTAMP-fecha_hora_ing_enfermeria) as dif_mm,    "
                + " EXTRACT(HOUR FROM CURRENT_TIMESTAMP-fecha_hora_ing_enfermeria)as dif_hh,    "
                + " EXTRACT(DAY FROM CURRENT_TIMESTAMP-fecha_hora_ing_enfermeria)as dif_dd, "
                + " 'enfermeria' as tipo "
                + " FROM  "
                + "   schema_uo.ing_enfermeria "
                + "  where id_duo_ing_enfermeria='" + numero_duo + "' order by fecha asc ; ");

        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {

                duo.setDif_dd(cnn.getRst().getInt("dif_dd"));
                duo.setDif_hh(cnn.getRst().getInt("dif_hh"));
                duo.setDif_mm(cnn.getRst().getInt("dif_mm"));
                duo.setDif_ss(cnn.getRst().getInt("dif_ss"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return duo;

    }

    public String corta_cadena(String cadena) {

        int encuentra_posicion = 0;
        int recorriendo = 0;
        boolean encontro = false;
        cadena = cadena + "";
        if (cadena.length() > 0) {
            while (recorriendo < cadena.length()) {
                if (cadena.substring(recorriendo, recorriendo + 1).equals(" ") && encontro == false) {
                    encuentra_posicion = recorriendo;
                    encontro = true;
                }
                recorriendo++;
            }
            if (encuentra_posicion != 0) {
                cadena = cadena.substring(0, encuentra_posicion) + "";
            }
        }

        return cadena;
    }

    /* 25-01-2013*/
    /* DATOS PARA INFORME DAS  */
    public ArrayList lista_das(String fecha_inicio, String fecha_final, int modo) {
        ArrayList lista = new ArrayList();
        String campo = "";
        if (modo == 1) {
            campo = "das_fecha_ingreso";
        } else {
            campo = "alt_ingreso";
        }

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " AA.das_id,dau_id, "
                + " das_paciente,CC.paciente_nombres,CC.paciente_apellidop,CC.paciente_apellidom, "
                + " das_estado, "
                + " (case "
                + " WHEN das_estado=1 THEN 'En cama' "
                + " WHEN das_estado=2 THEN 'Egresado' "
                + " WHEN das_estado=3 THEN '---' "
                + " WHEN das_estado=4 THEN 'Dado Alta' "
                + " WHEN das_estado=99 THEN 'Anulado' "
                + "  end) as das_estado_descripcion, "
                + " to_char(das_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as das_fecha_ingreso, "
                + " das_camilla, cam_descripcion, das_derivador,EE.der_descripcion,  "
                + " das_medico,DD.usuario_urgencia_nombres,DD.usuario_urgencia_apellidop, "
                + " das_usuario, das_ip,das_indicacion_destino, "
                + " das_indicacion_usuario, "
                + " to_char(das_indicacion_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as das_indicacion_fecha_ingreso, "
                + " das_indicacion_ip, "
                + " ALT.alt_id, to_char(ALT.alt_ingreso,'DD/MM/YYYY HH24:MI:SS')as alt_ingreso,ALT.alt_usuario, "
                + " ALT.alt_destino,DES.des_descripcion,ALT.alt_ip,ALT.alt_detalle "
                + " FROM  schema_suam.das AA "
                + " JOIN schema_suam.camilla BB ON (BB.cam_id=AA.das_camilla) "
                + " JOIN schema_urgencia.paciente CC ON (CC.paciente_rut=AA.das_paciente) "
                + " JOIN schema_urgencia.usuario_urgencia DD ON (DD.usuario_urgencia_rut=das_medico) "
                + " JOIN schema_suam.derivador EE ON (EE.der_id=AA.das_derivador)  "
                + " JOIN schema_uo.usuario FF ON (FF.rut_usuario=AA.das_usuario) "
                + " JOIN schema_suam.alta_das ALT ON (ALT.das_id=AA.das_id) "
                + " JOIN schema_suam.destino DES ON (DES.des_id=ALT.alt_destino) "
                + " WHERE " + campo + " BETWEEN '" + fecha_inicio + " 00:00:00' AND '" + fecha_final + " 23:59:59' "
                + " AND dau_nn_id =0 and ALT.alt_estado=1 "
                + " ORDER BY das_id desc ");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDas das = new cDas();
                das.setId_das(cnn.getRst().getInt("das_id"));
                das.setDau_id(cnn.getRst().getInt("dau_id"));
                //das.setDau_nn_id(cnn.getRst().getInt("dau_nn_id"));
                das.setRut_paciente(cnn.getRst().getString("das_paciente"));

                das.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                das.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                das.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));

                das.setEstado(cnn.getRst().getInt("das_estado"));
                das.setEstado_descri(cnn.getRst().getString("das_estado_descripcion"));

                das.setFecha_ingreso(cnn.getRst().getString("das_fecha_ingreso"));
                das.setCamilla(cnn.getRst().getInt("das_camilla"));
                das.setCamilla_descri(cnn.getRst().getString("cam_descripcion"));

                das.setDerivador(cnn.getRst().getInt("das_derivador"));
                das.setDerivador_descri(cnn.getRst().getString("der_descripcion"));

                das.setMedico(cnn.getRst().getString("das_medico"));

                das.setUsuario(cnn.getRst().getString("das_usuario"));
                das.setApellidom_usuario(cnn.getRst().getString("das_medico"));
                das.setApellidop_usuario(cnn.getRst().getString("usuario_urgencia_apellidop"));
                das.setNombre_usuario(cnn.getRst().getString("usuario_urgencia_nombres"));

                das.setIp(cnn.getRst().getString("das_ip"));

                das.setIndicacion_destino_descri("-- -- --");
                das.setIndicacion_destino_id(cnn.getRst().getInt("das_indicacion_destino"));
                das.setIndicacion_usuario_rut(cnn.getRst().getString("das_indicacion_usuario"));
                das.setIndicacion_fecha(cnn.getRst().getString("das_indicacion_fecha_ingreso"));

                if (das.getIndicacion_fecha() == null) {
                    das.setIndicacion_fecha("");
                }

                das.setIndicacion_ip(cnn.getRst().getString("das_indicacion_ip"));

                das.setAlta_destino(cnn.getRst().getInt("alt_destino"));
                das.setAlta_destino_descri(cnn.getRst().getString("des_descripcion"));
                das.setAlta_detalle(cnn.getRst().getString("alt_detalle"));
                das.setAlta_usuario_rut(cnn.getRst().getString("alt_usuario"));
                das.setAlta_ip(cnn.getRst().getString("alt_ip"));
                das.setAlta_fecha_ingreso(cnn.getRst().getString("alt_ingreso"));

                lista.add(das);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }
    /* FIN DATOS INFORME DAS*/

    public ArrayList lista_contacto(int id_das) {

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT "
                + " con_id,das_id,con_usuario,con_ingreso,  to_char(con_ingreso,'DD/MM/YYYY HH24:MI:SS')as con_ingreso_corta,"
                + " con_estado,con_ip,con_nombre, to_char(con_fecha,'DD/MM/YYYY') as con_fecha,"
                + " to_char(con_fecha,'DD/MM') as con_fecha_corta,  "
                + " con_hora, to_char(con_hora,'HH24:MI') as con_hora_corta , con_observacion,UU.nombre_usuario,UU.apellidop_usuario,UU.apellidom_usuario "
                + " FROM schema_suam.contacto CC JOIN schema_uo.usuario UU ON  "
                + " (CC.con_usuario=UU.rut_usuario) WHERE con_estado='1' and das_id='" + id_das + "' ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cContacto con = new cContacto();
                con.setId_contacto(cnn.getRst().getInt("con_id"));
                con.setDas_id(cnn.getRst().getInt("das_id"));
                con.setEstado(cnn.getRst().getInt("con_estado"));

                con.setFecha_ingreso(cnn.getRst().getString("con_ingreso"));
                con.setFecha(cnn.getRst().getString("con_fecha"));
                con.setHora(cnn.getRst().getString("con_hora"));
                con.setIp(cnn.getRst().getString("con_ip"));
                con.setObservacion(cnn.getRst().getString("con_observacion"));
                con.setNombre(cnn.getRst().getString("con_nombre"));

                con.setFecha_corta(cnn.getRst().getString("con_fecha_corta") + " " + cnn.getRst().getString("con_hora_corta"));
                con.setFecha_ingreso_corta(cnn.getRst().getString("con_ingreso_corta"));

                con.setRut_usuario(cnn.getRst().getString("con_usuario"));
                con.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                con.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                con.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));

                lista.add(con);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }
    /* EXAMENES Y RADIOGRAFAIAS SUAM 14-02-2013 */

    public ArrayList lista_examen_radiografia(String tipo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  exa_id,exa_descripcion, exa_tipo, "
                + " CASE  "
                + " WHEN exa_tipo='1' THEN 'EXAMEN'  "
                + " WHEN exa_tipo='2' THEN 'RADIOGRAFIA'  "
                + " ELSE 'OTRO' END as tipo ,exa_estado, to_char(exa_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as exa_fecha_ingreso "
                + " FROM  schema_suam.examen_radio WHERE exa_estado='1' AND exa_tipo IN (" + tipo + ") "
                + " ORDER BY exa_orden,exa_descripcion ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cExamen exa = new cExamen();
                exa.setId_examen(cnn.getRst().getInt("exa_id"));
                exa.setDescripcion(cnn.getRst().getString("exa_descripcion"));
                exa.setTipo(cnn.getRst().getInt("exa_tipo"));
                exa.setTipo_descripcion(cnn.getRst().getString("tipo"));
                exa.setFecha_ingreso(cnn.getRst().getString("exa_fecha_ingreso"));
                exa.setEstado(cnn.getRst().getInt("exa_estado"));
                lista.add(exa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    public ArrayList lista_examen_x_das(int id_das) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + " er_id, er_das_id,AA.er_examen, "
                + " BB.exa_descripcion, BB.exa_tipo,  "
                + " CASE   "
                + " WHEN BB.exa_tipo='1' THEN 'EXAMEN'  "
                + " WHEN BB.exa_tipo='2' THEN 'RADIOGRAFIA'   "
                + " ELSE 'OTRO' END as tipo , "
                + " to_char(er_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as er_fecha_ingreso , "
                + " to_char(er_fecha_ingreso,'DD/MM HH24:MI')as er_fecha_corta , "
                + " er_usuario "
                + " FROM  schema_suam.examen_radio_das AA "
                + " JOIN schema_suam.examen_radio BB ON(AA.er_examen=BB.exa_id) "
                + " WHERE er_estado=1 and AA.er_das_id='" + id_das + "'");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cExamen exa = new cExamen();
                exa.setId_examen(cnn.getRst().getInt("er_examen"));
                exa.setDescripcion(cnn.getRst().getString("exa_descripcion"));
                exa.setTipo(cnn.getRst().getInt("exa_tipo"));
                exa.setTipo_descripcion(cnn.getRst().getString("tipo"));
                // exa.setFecha_ingreso(cnn.getRst().getString("exa_fecha_ingreso"));
                // exa.setEstado(cnn.getRst().getInt("exa_estado"));
                exa.setId_das(cnn.getRst().getInt("er_das_id"));
                exa.setId_das_examen(cnn.getRst().getInt("er_id"));
                exa.setFecha_corta(cnn.getRst().getString("er_fecha_corta"));
                exa.setFecha_ingreso(cnn.getRst().getString("er_fecha_ingreso"));
                exa.setRut_usuario(cnn.getRst().getString("er_usuario"));
                lista.add(exa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    /* FIN EXAMENES Y RADIOGRAFAIAS SUAM 14-02-2013 */
    /**
     * para informe
     */
    public ArrayList obtiene_duo_liviano_x_fecha() {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  "
                + " id_duo, "
                + " to_char((fecha_duo+hora_duo),'DD/MM/YYYY HH24:MI:SS')as fecha_hora_ing_duo, "
                + " rut_paciente, "
                + " BB.paciente_nombres, "
                + " BB.paciente_apellidop, "
                + " BB.paciente_apellidom "
                + " FROM  schema_uo.duo AA JOIN schema_urgencia.paciente BB ON "
                + " (AA.rut_paciente=BB.paciente_rut) "
                + " WHERE id_derivador=2 and estado_duo NOT IN (99) AND "
                + " (fecha_duo+hora_duo) BETWEEN '01-01-2012 00:00:00' AND '12-31-2012 23:59:59' order by id_duo  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDuo duo = new cDuo();
                duo.setId_duo(cnn.getRst().getInt("id_duo"));
                // duo.setFecha_duo(cnn.getRst().getString("fecha_duo"));
                // duo.setHora_duo(cnn.getRst().getString("hora_duo"));
                // duo.setEstado_duo(cnn.getRst().getInt("estado_duo"));
                duo.setFecha_hora_ing_duo(cnn.getRst().getString("fecha_hora_ing_duo"));
                duo.setRut_paciente(cnn.getRst().getString("rut_paciente"));
                duo.setNombres_paciente(cnn.getRst().getString("paciente_nombres"));
                duo.setApellidop_paciente(cnn.getRst().getString("paciente_apellidop"));
                duo.setApellidom_paciente(cnn.getRst().getString("paciente_apellidom"));
                lista.add(duo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    /*para informe*/
    public String espacio(int cantidad) {
        String cadena = "";
        for (int k = 0; k < cantidad; k++) {
            cadena += "&nbsp;";
        }
        return cadena;
    }

    public String espacio2(int cantidad) {
        String cadena = "";
        for (int k = 0; k < cantidad; k++) {
            cadena += " ";
        }
        return cadena;
    }

    public String MMDDYYY(String fecha, int MODALIDAD) {
//MODALIDAD 1 llega DDMMYYYY    ||  MODALIDAD 2 llega YYYYMMDD
        String cadena = "";
        if (MODALIDAD == 1) {
            String DD = fecha.substring(0, 2);
            String MM = fecha.substring(3, 5);
            String YYYY = fecha.substring(6, 10);
            cadena = MM + "/" + DD + "/" + YYYY;
        } else {
            String YYYY = fecha.substring(0, 4);
            String MM = fecha.substring(5, 7);
            String DD = fecha.substring(8, 10);
            cadena = DD + "/" + MM + "/" + YYYY;
        }
        return cadena;
    }

    /*27082014*/
    public ArrayList lista_documento_segun_duo(int id_duo) {
        ArrayList lista = new ArrayList();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_duo_documento,\n"
                + "  DOC.id_documento,\n"
                + " DOC.descripcion_documento\n"
                + "FROM  schema_uo.duo_documento DUO\n"
                + "INNER JOIN schema_uo.documento DOC ON (DUO.id_documento=DOC.id_documento)\n"
                + " WHERE  id_duo='" + id_duo + "';");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDocumento doc = new cDocumento();
                doc.setDescripcion(cnn.getRst().getString("descripcion_documento"));
                doc.setId(cnn.getRst().getInt("id_documento"));

                lista.add(doc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NegocioQ.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }

    /*para informe*/
    public ArrayList lista_diagnostico_segun_duo() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL(" SELECT    id_diagnostico_duo, descripcion_diagnostico_duo,  "
                + " tipo_diagnostico_duo, id_duo  "
                + " FROM  schema_uo.diagnostico_duo  "
                + " where id_duo  IN (SELECT id_duo "
                + " FROM  schema_uo.duo AA WHERE id_derivador=2 and estado_duo NOT IN (99) AND "
                + "  (fecha_duo+hora_duo) BETWEEN '01-01-2012 00:00:00' AND '12-31-2012 23:59:59' "
                + " ) and  tipo_diagnostico_duo IN (1) ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico diag = new cDiagnostico();
                diag.setId_diagnostico(cnn.getRst().getInt("id_diagnostico_duo"));
                diag.setId_duo(cnn.getRst().getInt("id_duo"));
                diag.setTipo_diagnostico(cnn.getRst().getInt("tipo_diagnostico_duo"));
                diag.setDescripcion_diagnostico(cnn.getRst().getString("descripcion_diagnostico_duo"));

                lista.add(diag);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return lista;
    }
    /*para informe*/

    /*kinesiologia*/
//    public ArrayList lista_sesion_kinesiologia(int id_duo) {
//        ArrayList lista = new ArrayList();
//        this.configurarConexion("");
//        this.cnn.setEsSelect(true);
//        this.cnn.setSentenciaSQL("SELECT  ses_id, TO_CHAR(ses_fecha_ingreso,'DD/MM/YYYY')as ses_fecha_ingreso  FROM  schema_uo.kin_sesion SES\n"
//                + "WHERE SES.ses_duo='" + id_duo + "'");
//        this.cnn.conectar();
//        try {
//            while (cnn.getRst().next()) {
//                cSesionKine ses = new cSesionKine();
//                ses.setId_sesion_kine(cnn.getRst().getInt("ses_id"));
//                ses.setFecha_ingreso_sesion(cnn.getRst().getString("ses_fecha_ingreso"));
//
//                lista.add(ses);
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return lista;
//    }
    public ArrayList lista_sesion_kinesiologia(int id_duo) {

        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  ses_id, ses_estado, ses_usuario, \n"
                + " TO_CHAR(ses_fecha_ingreso,'DD/MM/YYYY HH24:MI:SS')as ses_fecha_ingreso,"
                + " TO_CHAR(ses_fecha_hora,'DD/MM/YYYY HH24:MI:SS')as ses_fecha_hora, \n"
                + " TO_CHAR(ses_fecha_hora,'DD/MM/YYYY')as ses_fecha,\n"
                + " TO_CHAR(ses_fecha_hora,'HH24:MI:SS')as ses_hora,\n"
                + " ses_detalle, ses_duo,\n"
                + " USU.nombre_usuario,USU.apellidop_usuario,USU.apellidom_usuario\n"
                + " FROM  schema_uo.kin_sesion\n"
                + " SES INNER JOIN schema_uo.usuario USU ON(SES.ses_usuario=USU.rut_usuario)\n"
                + " WHERE  ses_duo='" + id_duo + "' order by ses_fecha_hora  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cSesionKine ses = new cSesionKine();
                ses.setId_sesion_kine(cnn.getRst().getInt("ses_id"));

                ses.setId_duo(cnn.getRst().getInt("ses_duo"));
                ses.setFecha_ingreso_sesion(cnn.getRst().getString("ses_fecha_ingreso"));

                ses.setFecha_hora(cnn.getRst().getString("ses_fecha_hora"));
                ses.setFecha(cnn.getRst().getString("ses_fecha"));
                ses.setHora(cnn.getRst().getString("ses_hora"));
                ses.setDetalle(cnn.getRst().getString("ses_detalle"));

                ses.setRut_usuario(cnn.getRst().getString("ses_usuario"));
                ses.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                ses.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                ses.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));

                ses.setEstado_sesion(cnn.getRst().getInt("ses_estado"));
                if (ses.getEstado_sesion() == 0) {
                    ses.setEstado_desc_sesion("Anulado");
                    //  ses.setRut_usuario_anula(cnn.getRst().getString("ses_usuario_anulacion"));
//  ses.setNombre_usuario_anula(cnn.getRst().getString(""));
//  ses.setApellidop_usuario_anula(cnn.getRst().getString(""));
//  ses.setApellidom_usuario_anula(cnn.getRst().getString(""));
                } else {
                    ses.setEstado_desc_sesion("Activo");
                }

                lista.add(ses);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lista;
    }

    public ArrayList lista_evaluacion_traumatologia(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  tra_id, TO_CHAR(tra_fecha_ingreso,'DD/MM/YYYY')as tra_fecha_ingreso  \n"
                + "FROM  schema_uo.kin_eva_traumatologia WHERE tra_id_duo='" + id_duo + "' and tra_estado='1' ");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEvaTraumatologia tra = new cEvaTraumatologia();
                tra.setId_eva_traumatologia(cnn.getRst().getInt("tra_id"));
                tra.setFecha_ingreso_trauma(cnn.getRst().getString("tra_fecha_ingreso"));

                lista.add(tra);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public cEvaTraumatologia obtiene_evaluacion_traumatologia(int id_evaluacion) {
        cEvaTraumatologia eva = new cEvaTraumatologia();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  tra_id,tra_id_duo, tra_estado, tra_usuario,\n"
                + "  to_char(tra_fecha_ingreso,'DD/MM/YYYY')as tra_fecha_ingreso,\n"
                + "  to_char(tra_fecha_ingreso,'HH24:MI:SS')as tra_hora_ingreso,\n"
                + "  tra_observacion_inicial, tra_historial, tra_dolor,\n"
                + "  tra_prueba_especial,tra_palpacion, tra_marcha,\n"
                + "  tra_plano_frontal, tra_plano_sagital, tra_plano_posterior,\n"
                + "  tra_movimiento_pasivo, tra_movimiento_activo, tra_observacion,\n"
                + "  tra_dermatoma, tra_miotoma, tra_reflejo_osteotendineo, tra_test_neurodinamico,\n"
                + "  tra_diagnostico_imagen, tra_diagnostico_kinesico,\n"
                + "  to_char(tra_fecha_anulacion,'DD/MM/YYYY HH24:MI:SS')as tra_fecha_anulacion, \n"
                + "  tra_motivo_anulacion,tra_usuario_anulacion,\n"
                + "  USU.nombre_usuario,USU.apellidop_usuario,USU.apellidom_usuario\n"
                + "FROM  schema_uo.kin_eva_traumatologia TRA INNER JOIN \n"
                + " schema_uo.usuario USU ON (TRA.tra_usuario=USU.rut_usuario)\n"
                + "  WHERE tra_id='" + id_evaluacion + "'  ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                eva.setId_eva_traumatologia(cnn.getRst().getInt("tra_id"));
                eva.setId_duo(cnn.getRst().getInt("tra_id_duo"));
                eva.setEstado_trauma(cnn.getRst().getInt("tra_estado"));
                eva.setRut_usuario(cnn.getRst().getString("tra_usuario"));
                eva.setFecha_ingreso_trauma(cnn.getRst().getString("tra_fecha_ingreso"));
                eva.setHora_ingreso_trauma(cnn.getRst().getString("tra_hora_ingreso"));
                eva.setObservacion_inicial(cnn.getRst().getString("tra_observacion_inicial"));
                eva.setHistorial_usuario(cnn.getRst().getString("tra_historial"));
                eva.setDolor(cnn.getRst().getString("tra_dolor"));
                eva.setPrueba_especial(cnn.getRst().getString("tra_prueba_especial"));
                eva.setPalpacion(cnn.getRst().getString("tra_palpacion"));
                eva.setMarcha(cnn.getRst().getString("tra_marcha"));
                eva.setPlano_frontal(cnn.getRst().getString("tra_plano_frontal"));
                eva.setPlano_sagital(cnn.getRst().getString("tra_plano_sagital"));
                eva.setPlano_posterior(cnn.getRst().getString("tra_plano_posterior"));
                eva.setMovimiento_activo(cnn.getRst().getString("tra_movimiento_activo"));
                eva.setMovimiento_pasivo(cnn.getRst().getString("tra_movimiento_pasivo"));
                eva.setObservacion(cnn.getRst().getString("tra_observacion"));
                eva.setDermatoma(cnn.getRst().getString("tra_dermatoma"));
                eva.setMiotoma(cnn.getRst().getString("tra_miotoma"));
                eva.setReflejo_osteotendineo(cnn.getRst().getString("tra_reflejo_osteotendineo"));
                eva.setTest_neurodinamico(cnn.getRst().getString("tra_test_neurodinamico"));
                eva.setDiagnostico_imagen(cnn.getRst().getString("tra_diagnostico_imagen"));
                eva.setDiagnostico_kinesico(cnn.getRst().getString("tra_diagnostico_kinesico"));

                eva.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                eva.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                eva.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));
                if (eva.getEstado_trauma() == 0) {
                    eva.setEstado_desc_trauma("Anulado");
                    eva.setRut_usuario_anula(cnn.getRst().getString("ses_usuario_anulacion"));
                    eva.setFecha_anula(cnn.getRst().getString("ses_usuario_anulacion"));
//  ses.setNombre_usuario_anula(cnn.getRst().getString(""));
//  ses.setApellidop_usuario_anula(cnn.getRst().getString(""));
//  ses.setApellidom_usuario_anula(cnn.getRst().getString(""));
                } else {
                    eva.setEstado_desc_trauma("Activo");
                }
                eva.setRut_usuario(null);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return eva;
    }

    public ArrayList lista_evaluacion_neurologia(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  neu_id,  neu_id_duo, neu_estado,neu_usuario,\n"
                + "to_char ( neu_fecha_ingreso,'DD/MM/YYYY')as neu_fecha_ingreso ,\n"
                + "neu_lesion_evaluada,\n"
                + "neu_reaccion_apoyo,neu_fecha_anulacion,neu_motivo_anulacion\n"
                + "FROM schema_uo.kin_eva_neurologia WHERE neu_id_duo='" + id_duo + "'  AND neu_estado='1'  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEvaNeurologia neu = new cEvaNeurologia();

                neu.setId_duo(cnn.getRst().getInt("neu_id_duo"));
                neu.setId_neuro(cnn.getRst().getInt("neu_id"));
                neu.setFecha_ingreso_neuro(cnn.getRst().getString("neu_fecha_ingreso"));
                neu.setLesion_evaluada(cnn.getRst().getInt("neu_lesion_evaluada"));

                lista.add(neu);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lista;
    }

    public ArrayList lista_lesion() {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  les_id,les_descripcion,  les_fecha_ingreso, les_categoria\n"
                + "FROM  schema_uo.kin_neu_lesion WHERE les_estado='1'  ;");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cDiagnostico dia = new cDiagnostico();
                dia.setId_diagnostico(cnn.getRst().getInt("les_id"));
                dia.setTipo_diagnostico(cnn.getRst().getInt("les_categoria"));
                dia.setDescripcion_diagnostico(cnn.getRst().getString("les_descripcion"));
                lista.add(dia);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public cEvaNeurologia obtiene_evaluacion_neurologia(int id_evaluacion_neurologia) {
        cEvaNeurologia eva = new cEvaNeurologia();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT   neu_id,  neu_id_duo, neu_estado, neu_usuario,"
                + "   nombre_usuario,apellidop_usuario,apellidom_usuario, \n"
                + " to_char(neu_fecha_ingreso,'DD/MM/YYYY')as neu_fecha_ingreso, \n"
                + " to_char(neu_fecha_ingreso,'HH24:MI:SS')as neu_hora_ingreso, \n"
                + "  neu_lesion_evaluada, neu_lesion,\n"
                + " neu_ashworth,\n"
                + "  neu_dato1, neu_dato2, neu_dato3,\n"
                + "  neu_dato4, neu_dato5, neu_dato6,\n"
                + "  neu_dato7, neu_dato8, neu_dato9,\n"
                + "  neu_dato10, neu_dato11,\n"
                + "  neu_trofismo,  neu_reflejo_osteorendineo, neu_propiocepcion,\n"
                + "  neu_reaccion_equilibrio, neu_reaccion_enderezamiento, neu_reaccion_apoyo,\n"
                + "  (case when  neu_trofismo=1 then 'Normal'\n"
                + "  when  neu_trofismo=0 then 'Atrofia' end ) as neu_trofismo_desc,\n"
                + "  (case when  neu_propiocepcion=1 then 'Normal'\n"
                + "  when  neu_propiocepcion=0 then 'Alterada' end ) as neu_propiocepcion_desc,\n"
                + "   (case when  neu_reaccion_equilibrio=1 then 'Presente'\n"
                + "  when  neu_reaccion_equilibrio=0 then 'No presente' end ) as neu_reaccion_equilibrio_desc,\n"
                + "    (case when  neu_reaccion_enderezamiento=1 then 'Presente'\n"
                + "  when  neu_reaccion_enderezamiento=0 then 'No presente' end ) as neu_reaccion_enderezamiento_desc,\n"
                + "    (case when  neu_reaccion_apoyo=1 then 'Presente'\n"
                + "  when  neu_reaccion_apoyo=0 then 'No presente' end ) as neu_reaccion_apoyo_desc,\n"
                + "  neu_fecha_anulacion, neu_motivo_anulacion, neu_usuario_anulacion,\n"
                + "  neu_opcion1, neu_opcion2, neu_opcion3,\n"
                + "  neu_opcion4, neu_opcion5, neu_opcion6,\n"
                + "  neu_opcion1_desc, neu_opcion2_desc, neu_opcion3_desc,\n"
                + "  neu_opcion4_desc, neu_opcion5_desc, neu_opcion6_desc ,"
                + "   LES1.les_descripcion as lesion_evaluada ,\n"
                + "   LES2.les_descripcion as lesion_medular ,\n"
                + "    LES3.les_descripcion as tipo_lesion_medular ,\n"
                + "     LES12.les_descripcion as lesion_otra  ,\n"
                + "      LES14.les_descripcion as reflejo_ostetendineo_otra \n"
                + "FROM  schema_uo.kin_eva_neurologia NEU\n"
                + "INNER JOIN schema_uo.usuario USU ON (NEU.neu_usuario=USU.rut_usuario)\n"
                + "INNER JOIN schema_uo.kin_neu_lesion LES1 ON (NEU.neu_lesion_evaluada=LES1.les_id)  \n"
                + "INNER JOIN schema_uo.kin_neu_lesion LES2 ON (NEU.neu_lesion=LES2.les_id)  \n"
                + "INNER JOIN schema_uo.kin_neu_lesion LES3 ON (NEU.neu_tipo_lesion=LES3.les_id)  \n"
                + "INNER JOIN schema_uo.kin_neu_lesion LES12 ON (NEU.neu_lesion=LES12.les_id)  \n"
                + "INNER JOIN schema_uo.kin_neu_lesion LES14 ON (NEU.neu_reflejo_osteorendineo=LES14.les_id)  \n"
                + "WHERE neu_id='" + id_evaluacion_neurologia + "';");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {

                eva.setId_neuro(cnn.getRst().getInt("neu_id"));
                eva.setId_duo(cnn.getRst().getInt("neu_id_duo"));
                eva.setLesion_evaluada(cnn.getRst().getInt("neu_lesion_evaluada"));

                eva.setRut_usuario(cnn.getRst().getString("neu_usuario"));
                eva.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                eva.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                eva.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));

                eva.setFecha_ingreso_neuro(cnn.getRst().getString("neu_fecha_ingreso"));
                eva.setHora_ingreso_neuro(cnn.getRst().getString("neu_hora_ingreso"));

                eva.setLesion(cnn.getRst().getInt("neu_lesion"));
                eva.setAshworth(cnn.getRst().getString("neu_ashworth"));

                if (eva.getLesion_evaluada() == 1) {

                    eva.setLesion_evaluada_desc(cnn.getRst().getString("lesion_evaluada"));
                    eva.setLesion_desc(cnn.getRst().getString("lesion_medular"));
                    eva.setAsia(cnn.getRst().getString("neu_dato2"));
                    eva.setLesion_tipo_desc(cnn.getRst().getString("tipo_lesion_medular"));
                    eva.setReflejo_osteorendineo_desc(cnn.getRst().getString("neu_dato1"));
                    eva.setEvaluacion_sensitiva(cnn.getRst().getString("neu_dato3"));
                    eva.setSensibilidad(cnn.getRst().getString("neu_dato4"));
                    eva.setMotor_index(cnn.getRst().getString("neu_dato5"));
                    eva.setContraccion(cnn.getRst().getString("neu_dato6"));

                    eva.setSilla_ruedas(cnn.getRst().getString("neu_dato7"));
                    eva.setMarcha(cnn.getRst().getString("neu_dato11"));

                    eva.setNivel_motor(cnn.getRst().getString("neu_dato9"));
                    eva.setNivel_neurologico(cnn.getRst().getString("neu_dato10"));
                    eva.setNivel_sentivo(cnn.getRst().getString("neu_dato8"));

                    eva.setOp1(cnn.getRst().getInt("neu_opcion1"));
                    eva.setOp2(cnn.getRst().getInt("neu_opcion2"));
                    eva.setOp3(cnn.getRst().getInt("neu_opcion3"));
                    eva.setOp4(cnn.getRst().getInt("neu_opcion4"));
                    eva.setOp5(cnn.getRst().getInt("neu_opcion5"));
                    eva.setOp6(cnn.getRst().getInt("neu_opcion6"));

                    eva.setOp1_desc(cnn.getRst().getString("neu_opcion1_desc"));
                    eva.setOp2_desc(cnn.getRst().getString("neu_opcion2_desc"));
                    eva.setOp3_desc(cnn.getRst().getString("neu_opcion3_desc"));
                    eva.setOp4_desc(cnn.getRst().getString("neu_opcion4_desc"));
                    eva.setOp5_desc(cnn.getRst().getString("neu_opcion5_desc"));
                    eva.setOp6_desc(cnn.getRst().getString("neu_opcion6_desc"));

                } else {

                    eva.setLesion_evaluada_desc(cnn.getRst().getString("lesion_evaluada"));
                    eva.setLesion_desc(cnn.getRst().getString("lesion_otra"));

                    eva.setMotoneurona(cnn.getRst().getString("neu_dato1"));
                    eva.setExtrapiramiral(cnn.getRst().getString("neu_dato2"));
                    eva.setPostura(cnn.getRst().getString("neu_dato3"));
                    eva.setFuerza(cnn.getRst().getString("neu_dato4"));
                    eva.setTono_muscular(cnn.getRst().getString("neu_dato5"));

                    eva.setTrofismo_desc(cnn.getRst().getString("neu_trofismo_desc"));
                    eva.setTrofismo_adicional(cnn.getRst().getString("neu_dato6"));

                    eva.setEess(cnn.getRst().getString("neu_dato7"));
                    eva.setEeii(cnn.getRst().getString("neu_dato8"));

                    eva.setReflejo_osteorendineo_desc(cnn.getRst().getString("reflejo_ostetendineo_otra"));
                    eva.setPropiocepcion_desc(cnn.getRst().getString("neu_propiocepcion_desc"));
                    eva.setPropiocepcion_adicional(cnn.getRst().getString("neu_dato9"));
                    eva.setTransicion(cnn.getRst().getString("neu_dato10"));

                    eva.setReaccion_apoyo_desc(cnn.getRst().getString("neu_reaccion_apoyo_desc"));
                    eva.setReaccion_enderezamiento_desc(cnn.getRst().getString("neu_reaccion_enderezamiento_desc"));
                    eva.setReaccion_equilibrio_desc(cnn.getRst().getString("neu_reaccion_equilibrio_desc"));
                    eva.setMarcha(cnn.getRst().getString("neu_dato11"));

                    eva.setOp1(cnn.getRst().getInt("neu_opcion1"));
                    eva.setOp2(cnn.getRst().getInt("neu_opcion2"));
                    eva.setOp3(cnn.getRst().getInt("neu_opcion3"));
                    eva.setOp4(cnn.getRst().getInt("neu_opcion4"));

                    eva.setOp1_desc(cnn.getRst().getString("neu_opcion1_desc"));
                    eva.setOp2_desc(cnn.getRst().getString("neu_opcion2_desc"));
                    eva.setOp3_desc(cnn.getRst().getString("neu_opcion3_desc"));
                    eva.setOp4_desc(cnn.getRst().getString("neu_opcion4_desc"));

                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return eva;
    }

    public ArrayList lista_evaluacion_respiratoria(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cEvaRespiratoria ses = new cEvaRespiratoria();

                lista.add(ses);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lista;
    }

    public cEvaRespiratoria obtiene_evaluacion_respiratoria(int id_evaluacion_respiratoria) {
        cEvaRespiratoria lista_sesion = new cEvaRespiratoria();

        return lista_sesion;
    }

    /*fin kinesiologia*/
    /* ASISTENCIA SOCIAL*/
    public ArrayList lista_contacto(String paciente_rut) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT \n"
                + "  id_fono_direccion_paciente, rut_paciente,\n"
                + "  fono1_fono_direccion_paciente, fono2_fono_direccion_paciente,\n"
                + " direccion_fono_direccion_paciente,\n"
                + "  fecha_transaccion_fono_direccion_paciente, usuario_fono_direccion_paciente,\n"
                + "  ip_fono_direccion_paciente, id_comuna, COM.comuna_descripcion,\n"
                + "  contacto_nombre,  contacto_parentesco\n"
                + " FROM schema_urgencia.paciente_contacto CON\n"
                + " INNER JOIN schemaoirs.comuna COM ON (CON.id_comuna=COM.comuna_codigo)\n"
                + " WHERE rut_paciente='" + paciente_rut + "' and contacto_estado='1' ");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cPaciente pac = new cPaciente();

                pac.setDireccion(cnn.getRst().getString("direccion_fono_direccion_paciente"));
                pac.setTelefono1(cnn.getRst().getString("fono1_fono_direccion_paciente"));
                pac.setTelefono2(cnn.getRst().getString("fono2_fono_direccion_paciente"));
                pac.setComuna_descri(cnn.getRst().getString("comuna_descripcion"));

                pac.setRut_paciente(cnn.getRst().getString("rut_paciente"));
                pac.setNombres_paciente(cnn.getRst().getString("contacto_nombre"));

                pac.setParentesco_desc(cnn.getRst().getString("contacto_parentesco"));
                pac.setId_contacto(cnn.getRst().getInt("id_fono_direccion_paciente"));

                lista.add(pac);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lista;

    }

    public ArrayList lista_seguimiento(int id_duo) {
        ArrayList lista = new ArrayList();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  seg_id, seg_duo_id, seg_usuario,\n"
                + "TO_CHAR(seg_fecha_ingreso,'DD/MM/YYYY') AS seg_fecha_ingreso  ,\n"
                + "TO_CHAR(seg_fecha,'DD/MM/YYYY') AS seg_fecha, seg_descripcion\n"
                + "FROM  schema_uo.registro_social_seguimiento WHERE \n"
                + "seg_duo_id='" + id_duo + "' AND seg_estado='1'");
        this.cnn.conectar();
        try {
            while (cnn.getRst().next()) {
                cRegistroSeguimiento reg = new cRegistroSeguimiento();
                reg.setFecha_seguimiento(cnn.getRst().getString("seg_fecha"));
                reg.setDescripcion(cnn.getRst().getString("seg_descripcion"));
                reg.setId_registro_id(cnn.getRst().getInt("seg_duo_id"));
                reg.setId_seguimiento(cnn.getRst().getInt("seg_id"));
                lista.add(reg);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return lista;

    }

    public cRegistroSocial obtiene_registro_social(int id_duo) {

        cRegistroSocial reg = new cRegistroSocial();

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  reg_id,reg_usuario,\n"
                + "  reg_fecha_ingresa,  reg_estado, reg_estado_civil,\n"
                + "  reg_situacion_laboral,\n"
                + "  (CASE WHEN reg_situacion_laboral=1 THEN 'Activo'\n"
                + " WHEN reg_situacion_laboral=2 THEN 'Cesante'\n"
                + " WHEN reg_situacion_laboral=3 THEN 'Jubilado'\n"
                + " WHEN reg_situacion_laboral=0 THEN 'Otro'\n"
                + "   END) AS situacion_laboral_desc,\n"
                + "  reg_institucionalizado, reg_nombre_institucion,\n"
                + "  reg_vive, reg_hijos, reg_hijos_cantidad,\n"
                + "  reg_situacion,reg_plan, reg_duo\n"
                + "FROM  schema_uo.registro_social WHERE  reg_duo='" + id_duo + "' and reg_estado='1'  ;");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                reg.setId_registro(cnn.getRst().getInt("reg_id"));
                reg.setRut_usuario(cnn.getRst().getString("reg_usuario"));
                reg.setFecha_ingresa(cnn.getRst().getString("reg_fecha_ingresa"));
                reg.setEstado(cnn.getRst().getInt("reg_estado"));
                reg.setEstado_civil(cnn.getRst().getInt("reg_estado_civil"));
                reg.setSituacion(cnn.getRst().getString("reg_situacion"));

                reg.setSituacion_laboral(cnn.getRst().getInt("reg_situacion_laboral"));
                reg.setSituacion_laboral_desc(cnn.getRst().getString("situacion_laboral_desc"));
                reg.setInstitucionalizado(cnn.getRst().getInt("reg_institucionalizado"));
                if (reg.getInstitucionalizado() == 1) {
                    reg.setInstitucionalizado_desc("SI");
                } else {
                    reg.setInstitucionalizado_desc("NO");
                }
                reg.setHijos(cnn.getRst().getInt("reg_hijos"));
                reg.setVive(cnn.getRst().getInt("reg_vive"));
                if (reg.getVive() == 1) {
                    reg.setVive_desc("Solo");
                } else {
                    reg.setVive_desc("Con otros");
                }
                reg.setHijos_cantidad(cnn.getRst().getInt("reg_hijos_cantidad"));

                reg.setId_duo(cnn.getRst().getInt("reg_duo"));
                reg.setHijos(cnn.getRst().getInt("reg_hijos"));
                if (reg.getHijos() == 1) {
                    reg.setHijos_desc("SI");
                } else {
                    reg.setHijos_desc("NO");
                }
                reg.setPlan(cnn.getRst().getString("reg_plan"));
                reg.setHijos_cantidad(cnn.getRst().getInt("reg_hijos_cantidad"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return reg;

    }

    /*FIN ASISTENCIA SOCIAL*/
}
