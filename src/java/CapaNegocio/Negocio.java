/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CapaNegocio;

import CapaConexion.Conexion;
import CapaDato.cAlta_Das;
import CapaDato.cContacto;
import CapaDato.cDas;
import CapaDato.cDiagnostico;
import CapaDato.cDuo;
import CapaDato.cEpicrisis;
import CapaDato.cEvaNeurologia;
import CapaDato.cEvaTraumatologia;
import CapaDato.cExamen;
import CapaDato.cHistorial_Consultorio;
import CapaDato.cHito;
import CapaDato.cPaciente;
import CapaDato.cRegistroSeguimiento;
import CapaDato.cRegistroSocial;
import CapaDato.cSesionKine;
import CapaDato.cUsuario;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dis
 */
public class Negocio {

    public Conexion cnn;

    public void configurarConexion(String tabla) {
        cnn = new Conexion();
        this.cnn.setDriver("org.postgresql.Driver");
        this.cnn.setNombreTabla(tabla);

        //this.cnn.setUser("postgres");
        //this.cnn.setPassword("crsdb2008");
        //this.cnn.setNombreBaseDatos("jdbc:postgresql://10.8.4.11:5432/postgres_08072011");
        //this.cnn.setNombreBaseDatos("jdbc:postgresql://192.168.0.3:5432/postgres");
//        this.cnn.setNombreBaseDatos("jdbc:postgresql://localhost/postgres");
        this.cnn.setUser("postgres");
//        this.cnn.setPassword("crsdb2008");
        //this.cnn.setNombreBaseDatos("jdbc:postgresql://localhost/dis");
////////////////
        /*
         this.cnn.setUser("uo");
         this.cnn.setPassword("crsdb2008");
         this.cnn.setNombreBaseDatos("jdbc:postgresql://10.8.4.9:5432/postgres");
         * */
/////
        this.cnn.setUser("postgres");
        this.cnn.setPassword("crsdb2008");
        this.cnn.setNombreBaseDatos("jdbc:postgresql://10.8.4.9:5432/postgres");

//        this.cnn.setUser("postgres");
//        this.cnn.setPassword("crsdb2008");
//        this.cnn.setNombreBaseDatos("jdbc:postgresql://localhost/postgres");
    }

    public String getLocal() {
//        String local = "http://10.8.4.29:8085/modulo_uh/";
//        String local = "http://10.8.4.29:8084/modulo_uhce/";
//       String local = "http://10.8.4.29:8084/modulo_uhce/";
        String local = "http://10.8.4.9:9090/modulo_uhce/";
//        String local = "http://localhost:/modulo_uhce/";
        return local;
    }

    public void ingresa_anula_duo(String motivo, String usuario, int duo) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_uo.anulo_duo "
                + " (  motivo_anulo_duo,  fecha_anulo_duo, rut_usuario, id_duo "
                + " ) VALUES ( "
                + " '" + motivo + "',CURRENT_TIMESTAMP, "
                + "   '" + usuario + "','" + duo + "' );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_paciente_datos(String rut, int consultorio) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_urgencia.paciente  "
                + " SET  paciente_consultorio = '" + consultorio + "' "
                + " WHERE  paciente_rut ='" + rut + "' ;");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_valida_epicrisis(int duo_id) {
        /* ESTE METODO SETA LA HORA DE ALTA MEDICA EN EL REGISTRO DUO CORRESPONDIENTE,
         CORRESPONDE A LA HORA EN QUE VERDADERAMENTE EL PACIENTE DEJA LA CAMA*/
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.duo  "
                + " SET  fecha_hora_alta_med_duo = CURRENT_TIMESTAMP "
                + " WHERE  id_duo = '" + duo_id + "'");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_estado_duo_alta_administrativa(int duo_id, int nuevo_estado) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.duo  "
                + " SET  estado_duo = '" + nuevo_estado + "'"
                + " , fecha_hora_alta_adm_duo = CURRENT_TIMESTAMP "
                + " WHERE  id_duo ='" + duo_id + "';");
        cnn.conectar();
        cnn.cerrarConexion();

    }

    public void modifica_estado_duo(int duo_id, int nuevo_estado) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.duo  "
                + " SET  estado_duo = '" + nuevo_estado + "' "
                + " WHERE  id_duo ='" + duo_id + "';");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_epicrisis(cEpicrisis epi) {
//        String resumen, String examen, String diagnostico,
//            String indicacion, String fecha, String hora, String rut_usuario, int id_duo
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.epicrisis (resumen_epicrisis,examen_epicrisis,"
                + "diagnostico_epicrisis,indicacion_epicrisis,fecha_epicrisis,hora_epicrisis,rut_usuario,id_duo,ip_epicrisis)"
                + " values('" + epi.getResumen_epicrisis().toUpperCase().replaceAll("'", "''")
                + "','" + epi.getExamen_epicrisis().toUpperCase().replaceAll("'", "''") + "', "
                + "'" + epi.getDiagnostico_epicrisis().toUpperCase().replaceAll("'", "''")
                + "','" + epi.getIndicacion_epicrisis().toUpperCase().replaceAll("'", "''") + "',"
                + "'" + epi.getFecha_epicrisis() + "','" + epi.getHora_epicrisis() + "','"
                + epi.getRut_usuario() + "'," + epi.getId_duo() + ",'" + epi.getIp_epicrisis() + "')");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_alta_adm(String obs, String fecha, String hora, String rut_usuario, int id_duo, int id_destino, String ip) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.alta_adm(fecha_hora_alta_adm,rut_usuario,obs_alta_adm,id_duo,id_destino,ip_alta_adm) "
                + "values('" + fecha + " " + hora + "','" + rut_usuario + "','" + obs + "'," + id_duo + "," + id_destino + ",'" + ip + "')");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_duo_x_medico(cDuo duo) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE schema_uo.duo SET "
                + " estado_duo='" + duo.getEstado_duo() + "',anamnesis_duo='" + duo.getAnamnesis_duo() + "',"
                + " id_categorizacion='" + duo.getCategorizacion_id() + "', rut_usuario_ing_med='" + duo.getRut_usuario_ing_med() + "', "
                + " fecha_hora_ing_med=current_timestamp, ip_ing_med='" + duo.getIp_ing_med() + "' "
                + "   where id_duo='" + duo.getId_duo() + "';");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_duo_x_enfermeria(cDuo duo) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE schema_uo.duo SET "
                + " estado_duo='" + duo.getEstado_duo() + "',duo_tiene_enfermeria='1', ip_ing_enf='" + duo.getIp_ing_enf() + "' "
                + " where id_duo='" + duo.getId_duo() + "' ;");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_ingreso_enfermeria(String morbilidades, String farmacos, String observacion,
            String rut_usuario, int id_ex_fisico, int id_duo, String otro_ex_docto_ing_enfermeria) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        String query = "insert into schema_uo.ing_enfermeria(otro_comorbilidad_ing_enfermeria,"
                + "farmaco_ing_enfermeria,"
                + "obs_ing_enfermeria,rut_usuario_ing_enfermeria,id_examen_fisico_ing_enfermeria,id_duo_ing_enfermeria,otro_ex_docto_ing_enfermeria) "
                + "values('" + morbilidades.toUpperCase().replace("'", "''") + "','" + farmacos.toUpperCase().replace("'", "''") + "',"
                + "'" + observacion.toUpperCase().replace("'", "''") + "','" + rut_usuario.toUpperCase().replace("'", "''") + "',"
                + "" + id_ex_fisico + "," + id_duo + ",'" + otro_ex_docto_ing_enfermeria.toUpperCase().replace("'", "''") + "')";
        cnn.setSentenciaSQL(query);
        cnn.conectar();
        cnn.cerrarConexion();

    }

    public int ingresa_ExamenFisico(String conciencia, String cabeza, String mucoza, String torax,
            String abdomen, String eess, String eeii, String zona, String sng, String sfoley,
            String peso, String talla, String pulso, String presion, String temp, String sat,
            String vvp1, String vvp2, String vvc, String dorso_lumbar_ex_fisico, String piel_ex_fisico) {
        int id_examen_fisico = 0;
        this.configurarConexion("");
        cnn.setEsSelect(false);
        String query = "insert into schema_uo.ex_fisico(conciencia_ex_fisico,cabeza_ex_fisico,"
                + "mucosa_ex_fisico,torax_ex_fisico,abdomen_ex_fisico,eess_ex_fisico,"
                + "eeii_ex_fisico,z_sacra_ex_fisico,peso_ex_fisico,talla_ex_fisico,"
                + "pulso_ex_fisico,presion_a_ex_fisico,temp_ex_fisico,satura_ex_fisico,"
                + "vvp1_ex_fisico,vvp2_ex_fisico,vvc_ex_fisico,sng_ex_fisico,s_foley_ex_fisico,dorso_lumbar_ex_fisico,piel_ex_fisico) "
                + "VALUES('" + conciencia.toUpperCase().replace("'", "''") + "','" + cabeza.toUpperCase().replace("'", "''") + "',"
                + "'" + mucoza.toUpperCase().replace("'", "''") + "','" + torax.toUpperCase().replace("'", "''") + "',"
                + "'" + abdomen.toUpperCase().replace("'", "''") + "','" + eess.toUpperCase().replace("'", "''") + "',"
                + "'" + eeii.toUpperCase().replace("'", "''") + "','" + zona.toUpperCase().replace("'", "''") + "',"
                + "'" + peso.toUpperCase().replace("'", "''") + "','" + talla.toUpperCase().replace("'", "''") + "',"
                + "'" + pulso.toUpperCase().replace("'", "''") + "','" + presion.toUpperCase().replace("'", "''") + "',"
                + "'" + temp.toUpperCase().replace("'", "''") + "','" + sat.toUpperCase().replace("'", "''") + "',"
                + "'" + vvp1.toUpperCase().replace("'", "''") + "','" + vvp2.toUpperCase().replace("'", "''") + "',"
                + "'" + vvc.toUpperCase().replace("'", "''") + "','" + sng.toUpperCase().replace("'", "''") + "',"
                + "'" + sfoley.toUpperCase().replace("'", "''") + "','" + dorso_lumbar_ex_fisico.toUpperCase().replace("'", "''") + "',"
                + "'" + piel_ex_fisico.toUpperCase().replace("'", "''") + "')";
        cnn.setSentenciaSQL(query);
        cnn.conectar();
        cnn.cerrarConexion();
        /*luego se busca el correlativo actual para ingresarlo en la tabla ing enfermeria */
        String query_curval = "select id_ex_fisico from schema_uo.ex_fisico order by id_ex_fisico desc limit 1 ";
        cnn.setSentenciaSQL(query_curval);
        cnn.setEsSelect(true);
        cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                id_examen_fisico = cnn.getRst().getInt("id_ex_fisico");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        cnn.cerrarConexion();
        return id_examen_fisico;
    }

    public void ingresa_DoctoAdjunto(int id_duo, int id_documento) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.duo_documento(id_duo,id_documento)"
                + " values(" + id_duo + "," + id_documento + ")");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_EnfCronica(int id_duo, int id_enfermedad) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.duo_enfermedad_cro(id_duo,id_enfermedad_cro)"
                + " values(" + id_duo + "," + id_enfermedad + ")");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_PrestacionesDuo(int duo, int prestacion) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_uo.prestacion_duo "
                + "  (duop_duo,duop_prestacion, duop_estado)  "
                + "  VALUES ('" + duo + "', '" + prestacion + "','1'); ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_duo(cDuo duo) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO schema_uo.duo "
                + " (fecha_duo, hora_duo, "
                + "   estado_duo, id_cama, id_prevision, "
                + "  fecha_hora_ing_duo, rut_usuario, rut_paciente, "
                + "   id_derivador, id_categorizacion, "
                + "  tipo_duo_id, duo_tiene_enfermeria,programa_duo) VALUES ( "
                + " '" + duo.getFecha_duo() + "','" + duo.getHora_duo() + "','1','" + duo.getCama() + "','" + duo.getId_prevision()
                + "',CURRENT_TIMESTAMP, "
                + " '" + duo.getRut_usuario() + "','" + duo.getRut_paciente() + "','" + duo.getDerivador_id()
                + "','0','1','0' ,'" + duo.getPrograma() + "' );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_diagnostico_duo(cDiagnostico diag) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_uo.diagnostico_duo "
                + " ( descripcion_diagnostico_duo, tipo_diagnostico_duo, id_duo ) VALUES "
                + "( '" + diag.getDescripcion_diagnostico() + "', '" + diag.getTipo_diagnostico() + "', "
                + "'" + diag.getId_duo() + "');");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void elimina_diagnostico_duo(int id_diagnostico) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("Delete from schema_uo.diagnostico_duo where "
                + " id_diagnostico_duo='" + id_diagnostico + "' ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_cama_duo(int duo_id, int cama_nueva) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.duo  "
                + " SET  id_cama = '" + cama_nueva + "'  "
                + " WHERE  id_duo ='" + duo_id + "'");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_paciente(cPaciente pac) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_urgencia.paciente "
                + " (paciente_rut, paciente_nombres, paciente_apellidop, paciente_apellidom, "
                + "  paciente_sexo, paciente_fecha_nac, paciente_direccion, paciente_telefono1, "
                + "  paciente_telefono2, comuna_codigo, paciente_fecha_creacion, paciente_procedencia, "
                + "  paciente_consultorio, paciente_pueblo,paciente_mail,paciente_nacionalidad) VALUES ( "
                + "  '" + pac.getRut_paciente() + "',  '" + pac.getNombres_paciente() + "', "
                + "  '" + pac.getApellidop_paciente() + "','" + pac.getApellidom_paciente() + "',  '" + pac.getSexo() + "', "
                + "  '" + pac.getFecha_nac() + "', '" + pac.getDireccion() + "', "
                + "  '" + pac.getTelefono1() + "',  '" + pac.getTelefono2() + "', "
                + "  '" + pac.getComuna_codigo() + "',  CURRENT_TIMESTAMP, "
                + "  '7',  '" + pac.getConsultorio() + "',  '" + pac.getPueblo() + "','"
                + pac.getMail() + "','" + pac.getNacion() + "' );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_paciente(cPaciente pac) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  "
                + " schema_urgencia.paciente   "
                + " SET  "
                + " paciente_direccion = '" + pac.getDireccion() + "', "
                + "  paciente_telefono1 = '" + pac.getTelefono1() + "', "
                + "  paciente_telefono2 = '" + pac.getTelefono2() + "', "
                + "  comuna_codigo = '" + pac.getComuna_codigo() + "',  "
                + "  paciente_pueblo = '" + pac.getPueblo() + "', "
                + "  paciente_consultorio = '" + pac.getConsultorio() + "', "
                + "  paciente_mail = '" + pac.getMail() + "',  "
                + "  paciente_nacionalidad = '" + pac.getNacion() + "' "
                + "  WHERE  "
                + "   paciente_rut = '" + pac.getRut_paciente() + "';");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_prevision(cPaciente pac) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO schema_urgencia.prevision_paciente "
                + " ( id_paciente_prevision_paciente, "
                + "  codigo_fonasa_prevision_paciente, tramo_prevision_paciente,estado_prevision_paciente, "
                + "  prais_prevision_paciente,verificado_fonasa "
                + " ) VALUES ( "
                + "  '" + pac.getRut_paciente() + "', "
                + "  '" + pac.getCodigo_fonasa() + "', "
                + "  '" + pac.getTramo_prevision() + "', "
                + "  '1', '" + pac.getPrais() + "', "
                + "  '" + pac.getVerificado_fonasa() + "');");
        cnn.conectar();
        cnn.cerrarConexion();

    }

    public void modifica_prevision(int id_num) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL(" UPDATE schema_urgencia.prevision_paciente  "
                + " SET  estado_prevision_paciente = '0' "
                + " WHERE  id_prevision_paciente = '" + id_num + "'");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_prevision_todas(String rut) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL(" UPDATE schema_urgencia.prevision_paciente  "
                + " SET  estado_prevision_paciente = '0' "
                + " WHERE  id_paciente_prevision_paciente = '" + rut + "'");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public cUsuario valida_Usuario(String rut, String pass) {
        cUsuario aux = new cUsuario();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  rut_usuario, perfil_usuario, pass_usuario, "
                + " nombre_usuario, apellidop_usuario,apellidom_usuario, "
                + " BB.descripcion_perfil  FROM schema_uo.usuario AA JOIN schema_uo.perfil BB ON  "
                + " (AA.perfil_usuario=BB.id_perfil)  "
                + " where rut_usuario='" + rut + "' "
                + " and pass_usuario='" + pass + "' and estado_usuario=1 ; ");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                aux.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));
                aux.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                aux.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                aux.setPass_usuario(cnn.getRst().getString("pass_usuario"));
                aux.setPerfil_usuario(cnn.getRst().getInt("perfil_usuario"));
                aux.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                aux.setPerfil_descripcion_usuario(cnn.getRst().getString("descripcion_perfil"));
            } else {
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return aux;
    }

    public cUsuario valida_Usuario_sinPass(String rut) {
        cUsuario aux = new cUsuario();
        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("SELECT  rut_usuario, perfil_usuario, pass_usuario, "
                + " nombre_usuario, apellidop_usuario,apellidom_usuario,estado_usuario, "
                + " BB.descripcion_perfil  FROM schema_uo.usuario AA JOIN schema_uo.perfil BB ON  "
                + " (AA.perfil_usuario=BB.id_perfil)  "
                + " where rut_usuario='" + rut + "'  ");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                aux.setApellidom_usuario(cnn.getRst().getString("apellidom_usuario"));
                aux.setApellidop_usuario(cnn.getRst().getString("apellidop_usuario"));
                aux.setNombre_usuario(cnn.getRst().getString("nombre_usuario"));
                aux.setPass_usuario(cnn.getRst().getString("pass_usuario"));
                aux.setPerfil_usuario(cnn.getRst().getInt("perfil_usuario"));
                aux.setRut_usuario(cnn.getRst().getString("rut_usuario"));
                aux.setPerfil_descripcion_usuario(cnn.getRst().getString("descripcion_perfil"));
                aux.setEstado_usuario(cnn.getRst().getInt("estado_usuario"));
            } else {
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return aux;
    }


    /* VISITA ENFERMERIA*/
    public int ingresa_categorizacion_enfermeria(int d1, int d2, int d3, int d4, int d5, int d6, int r1,
            int r2, int r3, int r4, int r5, int r6, int r7, int r8, String cat) {
        int id_categorizacion = 0;
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.visita_categorizacion ("
                + "d1_visita_categorizacion,d2_visita_categorizacion,"
                + "d3_visita_categorizacion,d4_visita_categorizacion,"
                + "d5_visita_categorizacion,d6_visita_categorizacion,"
                + "r1_visita_categorizacion,r2_visita_categorizacion,"
                + "r3_visita_categorizacion,r4_visita_categorizacion,"
                + "r5_visita_categorizacion,r6_visita_categorizacion,"
                + "r7_visita_categorizacion,r8_visita_categorizacion,cat_visita_categorizacion) "
                + "values(" + d1 + "," + d2 + "," + d3 + "," + d4 + "," + d5 + "," + d6 + "," + r1 + "," + r2 + "," + r3 + "," + r4 + "," + r5 + "," + r6 + "," + r7 + "," + r8 + ",'" + cat.trim() + "')");
        cnn.conectar();
        cnn.cerrarConexion();

        cnn.setEsSelect(true);
        cnn.setSentenciaSQL("SELECT  id_visita_categorizacion FROM "
                + " schema_uo.visita_categorizacion order by  "
                + "  id_visita_categorizacion desc limit 1  ;");
        cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                id_categorizacion = cnn.getRst().getInt("id_visita_categorizacion");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        cnn.cerrarConexion();

        return id_categorizacion;
    }

    public int ingresa_visita_enfermeria(String obs, String fecha, String hora, String rut_usu, int id_cama, int id_cat, int tipo, int id_duo) {
        int id_visita = 0;
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("insert into schema_uo.visita (obs_visita,fecha_visita,hora_visita,rut_usuario,"
                + "id_cama,id_visita_categorizacion,tipo_visita,id_duo) "
                + "values('" + obs + "','" + fecha + "','" + hora + "','" + rut_usu + "'," + id_cama + "," + id_cat + "," + tipo + "," + id_duo + ")");
        cnn.conectar();
        cnn.cerrarConexion();

        /**
         * **********
         */
        cnn.setEsSelect(true);
        cnn.setSentenciaSQL("SELECT id_visita  FROM   schema_uo.visita order by id_visita desc limit 1 ;");
        cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                id_visita = cnn.getRst().getInt("id_visita");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        cnn.cerrarConexion();

        return id_visita;
    }
    /* FIN VISITA ENFERMERIA */

    public void modifica_ultima_sesion(String rut_usuario) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.usuario set ultima_sesion=current_timestamp where rut_usuario='" + rut_usuario + "'   ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_clave(String rut_usuario, String clave_usuario) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.usuario  set pass_usuario='" + clave_usuario + "' where rut_usuario='" + rut_usuario + "'   ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_usuario(cUsuario usu) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO   schema_uo.usuario "
                + "(  rut_usuario,  perfil_usuario,  pass_usuario, "
                + "  nombre_usuario, apellidop_usuario, "
                + "  apellidom_usuario,estado_usuario "
                + " )  VALUES ( "
                + "  '" + usu.getRut_usuario() + "', " + usu.getPerfil_usuario() + ", "
                + " '" + usu.getPass_usuario() + "',  '" + usu.getNombre_usuario() + "', "
                + "   '" + usu.getApellidop_usuario() + "','" + usu.getApellidom_usuario() + "','1');");

        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void modifica_usuario(cUsuario usu) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_uo.usuario  \n"
                + "SET  perfil_usuario = '" + usu.getPerfil_usuario() + "', \n"
                + "  nombre_usuario = '" + usu.getNombre_usuario() + "', \n"
                + "  apellidop_usuario =  '" + usu.getApellidop_usuario() + "', \n"
                + "  apellidom_usuario = '" + usu.getApellidom_usuario() + "', \n"
                + "  estado_usuario = '" + usu.getEstado_usuario() + "' \n"
                + "WHERE  rut_usuario = '" + usu.getRut_usuario() + "' ");

        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void modifica_password_usuario(cUsuario usu) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE   schema_uo.usuario  \n"
                + "SET pass_usuario = '" + usu.getPass_usuario() + "' \n"
                + "WHERE  rut_usuario ='" + usu.getRut_usuario() + "' ");

        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void modifica_duo_x_alta_adm(int duo, int estado, int prevision) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE   schema_uo.duo  "
                + " SET  estado_duo = '" + estado + "', id_prevision = '" + prevision + "', "
                + " fecha_hora_alta_adm_duo = CURRENT_TIMESTAMP "
                + " WHERE id_duo = '" + duo + "';");

        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    /*09-08-2012*/
    public void ingresa_historial_consultorio_pertenencia(cHistorial_Consultorio his) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_uo.historial_consultorio_pertenencia "
                + " ( his_paciente_rut, his_consultorio_anterior, his_consultorio_nuevo, "
                + "  his_fecha, his_usuario, his_ip) VALUES ( "
                + "  '" + his.getHis_paciente_rut() + "', '" + his.getHis_consultorio_anterior() + "', '" + his.getHis_consultorio_nuevo() + "', "
                + "  CURRENT_TIMESTAMP, '" + his.getHis_usuario() + "', '" + his.getHis_ip() + "');");
        this.cnn.conectar();
        this.cnn.cerrarConexion();
    }

    public void ingresa_hito_paciente(cHito hit) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("  INSERT INTO schema_uo.hito_paciente "
                + " ( hit_tipo,hit_fecha, hit_paciente_rut, "
                + "  hit_detalle, hit_estado, hit_usuario,hit_ip ) VALUES ( "
                + "  '" + hit.getTipo() + "',CURRENT_TIMESTAMP, '" + hit.getRut_paciente() + "', '" + hit.getDetalle() + "', "
                + "  '1', '" + hit.getUsuario_rut() + "', '" + hit.getIp() + "' );");
        this.cnn.conectar();
        this.cnn.cerrarConexion();

    }

    /*FIN 09-08-2012*/
    /**
     * ****************************************************
     */
    public void ingresa_diagnostico_suam(cDiagnostico diag) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_suam.diagnostico "
                + "( das_id, dia_usuario,dia_ingreso,dia_estado, "
                + "  dia_ip,dia_tipo, dia_detalle) VALUES ( "
                + "  '" + diag.getId_duo() + "', '" + diag.getRut_usuario() + "', "
                + "  CURRENT_TIMESTAMP, '1', "
                + "  '" + diag.getIp() + "','" + diag.getTipo_diagnostico() + "','" + diag.getDescripcion_diagnostico() + "'  );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void elimina_diagnostico_suam(int id_diagnostico) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_suam.diagnostico  "
                + " SET  dia_estado ='0' "
                + " WHERE  dia_id = '" + id_diagnostico + "' ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_observacion_suam(cDiagnostico diag) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_suam.observacion "
                + " ( das_id,obs_usuario,obs_ingreso,obs_estado, "
                + "  obs_ip,obs_espera_radiografia,obs_espera_ex_laboratorio,obs_detalle "
                + " ) VALUES ( "
                + "   '" + diag.getId_duo() + "', "
                + "  '" + diag.getRut_usuario() + "', "
                + "  CURRENT_TIMESTAMP, "
                + "  '1', "
                + "  '" + diag.getIp() + "', "
                + "  '" + diag.getEspera_radiografia() + "', "
                + "  '" + diag.getEspera_ex_laboratorio() + "', "
                + "  '" + diag.getDescripcion_diagnostico() + "');");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void elimina_observacion_suam(int id_observacion) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_suam.observacion  "
                + " SET  obs_estado = '0' "
                + " WHERE  obs_id = '" + id_observacion + "'");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void ingresa_das(cDas das) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("INSERT INTO  schema_suam.das "
                + " ( dau_id,dau_nn_id,das_paciente,das_estado,das_fecha_ingreso, "
                + "  das_camilla,das_derivador,das_medico,das_usuario,das_ip "
                + " ) VALUES ( "
                + "   '" + das.getDau_id() + "',  '" + das.getDau_nn_id() + "', "
                + "  '" + das.getRut_paciente() + "',  '1', "
                + "  CURRENT_TIMESTAMP,  '" + das.getCamilla() + "', "
                + "  '" + das.getDerivador() + "',  '" + das.getMedico() + "', "
                + "  '" + das.getUsuario() + "',  '" + das.getIp() + "' );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_camilla(int das_id, int camilla_id) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_suam.das  "
                + " SET  das_camilla ='" + camilla_id + "' WHERE  das_id = '" + das_id + "';");
        cnn.conectar();
        cnn.cerrarConexion();

    }

    public void ingresa_alta_das(cAlta_Das aux) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("  INSERT INTO  schema_suam.alta_das "
                + " ( das_id,alt_usuario,alt_ingreso, "
                + "  alt_estado,alt_destino, alt_detalle, alt_ip,alt_medico "
                + " ) VALUES ( "
                + "  '" + aux.getId_das() + "', "
                + "  '" + aux.getRut_usuario() + "', "
                + "  CURRENT_TIMESTAMP, "
                + "  '1', "
                + "  '" + aux.getDestino() + "', "
                + "  '" + aux.getDetalle() + "', "
                + "  '" + aux.getIp() + "','" + aux.getMedico_rut() + "' "
                + " );");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_das_x_indicacion_alta(cDas aux) {
        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_suam.das  SET  "
                + " das_estado = '2', "
                + " das_indicacion_destino = '" + aux.getIndicacion_destino_id() + "', "
                + " das_indicacion_usuario = '" + aux.getIndicacion_usuario_rut() + "', "
                + " das_indicacion_fecha_ingreso = CURRENT_TIMESTAMP, "
                + " das_indicacion_ip = '" + aux.getIndicacion_ip() + "' "
                + " WHERE  das_id = '" + aux.getId_das() + "' ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    public void modifica_estado_das(int id_das, int nuevo_estado) {

        this.configurarConexion("");
        cnn.setEsSelect(false);
        cnn.setSentenciaSQL("UPDATE  schema_suam.das  SET  "
                + " das_estado = '" + nuevo_estado + "' "
                + " WHERE  das_id = '" + id_das + "' ");
        cnn.conectar();
        cnn.cerrarConexion();
    }

    /**
     * ****************************************************
     */
    public boolean obtiene_ip_prohibida(String ip) {
        boolean sw = false;

        this.configurarConexion("");
        this.cnn.setEsSelect(true);
        this.cnn.setSentenciaSQL("select ip_descripcion from schema_uo.ip_prohibida "
                + " where ip_descripcion='" + ip + "' and ip_estado='1' limit 1");
        this.cnn.conectar();
        try {
            if (cnn.getRst().next()) {
                sw = true;
            } else {
                sw = false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Negocio.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.cnn.cerrarConexion();
        return sw;

    }

    public boolean ingresa_contacto(cContacto con) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO  schema_suam.contacto "
                + " ( das_id, con_usuario, con_ingreso, "
                + "  con_estado, con_ip, con_nombre, con_fecha, "
                + "  con_hora, con_observacion )  VALUES ( "
                + "  '" + con.getDas_id() + "',  '" + con.getRut_usuario() + "', "
                + "  CURRENT_TIMESTAMP, '" + con.getEstado() + "', "
                + "  '" + con.getIp() + "', '" + con.getNombre() + "', "
                + "  '" + con.getFecha() + "', '" + con.getHora() + "', '" + con.getObservacion() + "');");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        }

        this.cnn.cerrarConexion();
        return sw;
    }

    public boolean elimina_epicrisis_segun_duo(int id_duo, String usuario) {
        boolean sw = false;
        this.configurarConexion("");
        try {
            /* registro epicrisis anulada */
            this.cnn.setEsSelect(false);
            this.cnn.setSentenciaSQL("INSERT INTO  schema_uo.epicrisis_anulada "
                    + " (id_epicrisis,resumen_epicrisis,examen_epicrisis,diagnostico_epicrisis, "
                    + "  indicacion_epicrisis,fecha_epicrisis,hora_epicrisis,rut_usuario, "
                    + "  id_duo,fecha_hora_epicrisis,ip_epicrisis,usuario_responsable )  "
                    + " SELECT  id_epicrisis,resumen_epicrisis,examen_epicrisis, "
                    + "  diagnostico_epicrisis,indicacion_epicrisis,fecha_epicrisis, "
                    + "  hora_epicrisis,rut_usuario,id_duo, "
                    + "  fecha_hora_epicrisis,ip_epicrisis,'" + usuario + "' "
                    + " FROM  schema_uo.epicrisis WHERE id_duo='" + id_duo + "';");
            this.cnn.conectar();
            this.cnn.cerrarConexion();
            /* registro epicrisis anulada */


            /* elimina epicrisis*/
            this.configurarConexion("");
            this.cnn.setEsSelect(false);
            this.cnn.setSentenciaSQL("DELETE FROM  schema_uo.epicrisis  EP  "
                    + " WHERE   EP.id_duo='" + id_duo + "' ");
            this.cnn.conectar();
            this.cnn.cerrarConexion();
            /* elimina epicrisis */

            /*UPDATE DUO */
            this.cnn.setEsSelect(false);
            this.cnn.setSentenciaSQL("UPDATE  schema_uo.duo  SET  estado_duo = '21', "
                    + " fecha_hora_alta_med_duo = null WHERE  id_duo = '" + id_duo + "' ;");
            this.cnn.conectar();
            this.cnn.cerrarConexion();
            /* FIN UPDATE DUO */

            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }

        return sw;

    }

    public boolean elimina_ingreso_enfermeria_duo(int id_duo, String usuario) {
        boolean sw = false;
        this.configurarConexion("");
        try {
            /* registro epicrisis anulada */
            this.cnn.setEsSelect(false);
            this.cnn.setSentenciaSQL("INSERT INTO   schema_uo.ing_enfermeria_anulada"
                    + "( id_ing_enfermeria, fecha_hora_ing_enfermeria,"
                    + "  otro_comorbilidad_ing_enfermeria, farmaco_ing_enfermeria,"
                    + "  obs_ing_enfermeria, rut_usuario_ing_enfermeria,"
                    + "  id_examen_fisico_ing_enfermeria, id_duo_ing_enfermeria,"
                    + "  otro_ex_docto_ing_enfermeria,usuario_responsable"
                    + ")  (SELECT id_ing_enfermeria, fecha_hora_ing_enfermeria,"
                    + "  otro_comorbilidad_ing_enfermeria, farmaco_ing_enfermeria,"
                    + "  obs_ing_enfermeria,rut_usuario_ing_enfermeria,"
                    + "  id_examen_fisico_ing_enfermeria, id_duo_ing_enfermeria,"
                    + "  otro_ex_docto_ing_enfermeria, '" + usuario + "' "
                    + "FROM  schema_uo.ing_enfermeria where id_duo_ing_enfermeria='" + id_duo + "'"
                    + ");");
            this.cnn.conectar();

            this.cnn.setSentenciaSQL("UPDATE  schema_uo.duo  "
                    + " SET   duo_tiene_enfermeria = '0', ip_ing_enf = '' "
                    + " WHERE  id_duo = '999999999'");
            this.cnn.conectar();
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }

        return sw;
    }


    /* EXAMENES Y RADIOGRAFIAS SUAM 14-02-2013 */
    public boolean ingresa_examen_radiografia(cExamen exa) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO  schema_suam.examen_radio_das "
                + " ( er_das_id,er_examen,er_usuario, er_fecha_ingreso, er_estado "
                + " ) VALUES ('" + exa.getId_das() + "','" + exa.getId_examen() + "', "
                + "   '" + exa.getRut_usuario() + "',CURRENT_TIMESTAMP,'1');");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        }
        this.cnn.cerrarConexion();
        return sw;
    }

    public boolean elimina_examen_radiografia(int id) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE schema_suam.examen_radio_das  "
                + " SET  er_estado = '0' "
                + " WHERE  er_id = '" + id + "' ");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        }
        return sw;
    }
    /* FIN EXAMENES Y RADIOGRAFAIAS SUAM 14-02-2013 */

    /*SISTEMA UOCE 21 AGOSTO 2014*/
    public boolean ingresa_sesion_kine(cSesionKine ses) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO   schema_uo.kin_sesion ( ses_estado, ses_usuario,\n"
                + "  ses_fecha_ingreso, ses_fecha_hora, ses_detalle,ses_duo ) \n"
                + "VALUES ( '1', '" + ses.getRut_usuario() + "',\n"
                + "  CURRENT_TIMESTAMP, '" + ses.getFecha_hora() + "', '" + ses.getDetalle() + "', '" + ses.getId_duo() + "' );");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    public boolean elimina_sesion_kine(int id_sesionKine, String rut_usuario, String motivo) {
        boolean sw = false;

        return sw;
    }
    /**/

    public boolean ingresa_evaluacion_traumatologica(cEvaTraumatologia tra) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO   schema_uo.kin_eva_traumatologia  \n"
                + "(tra_id_duo,tra_estado, tra_usuario,tra_fecha_ingreso,  \n"
                + "  tra_observacion_inicial, tra_historial, tra_dolor,  \n"
                + "  tra_prueba_especial, tra_palpacion, tra_marcha,  \n"
                + "  tra_plano_frontal, tra_plano_sagital, tra_plano_posterior,  \n"
                + "  tra_movimiento_pasivo,tra_movimiento_activo,tra_observacion,  \n"
                + "  tra_dermatoma, tra_miotoma, tra_reflejo_osteotendineo,  \n"
                + "  tra_test_neurodinamico, tra_diagnostico_imagen, tra_diagnostico_kinesico  \n"
                + ")  VALUES ( '" + tra.getId_duo() + "', '1', '" + tra.getRut_usuario() + "',CURRENT_TIMESTAMP,  \n"
                + "  '" + tra.getObservacion_inicial() + "','" + tra.getHistorial_usuario() + "','" + tra.getDolor() + "',  \n"
                + "  '" + tra.getPrueba_especial() + "', '" + tra.getPalpacion() + "', '" + tra.getMarcha() + "',  \n"
                + "  '" + tra.getPlano_frontal() + "', '" + tra.getPlano_sagital() + "', '" + tra.getPlano_posterior() + "',  \n"
                + "  '" + tra.getMovimiento_pasivo() + "','" + tra.getMovimiento_activo() + "','" + tra.getObservacion() + "',  \n"
                + "  '" + tra.getDermatoma() + "', '" + tra.getMiotoma() + "', '" + tra.getReflejo_osteotendineo() + "',  \n"
                + "  '" + tra.getTest_neurodinamico() + "', '" + tra.getDiagnostico_imagen() + "','" + tra.getDiagnostico_kinesico() + "');");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    public boolean elimina_evaluacion_traumatologica(int id_evaTrauma, String rut_usuario, String motivo) {
        boolean sw = false;

        return sw;
    }

    public boolean ingresa_contacto(cPaciente con) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO  schema_urgencia.paciente_contacto\n"
                + " (rut_paciente, fono1_fono_direccion_paciente, fono2_fono_direccion_paciente, direccion_fono_direccion_paciente,\n"
                + "  fecha_transaccion_fono_direccion_paciente, usuario_fono_direccion_paciente, ip_fono_direccion_paciente,\n"
                + "  id_comuna, contacto_nombre, contacto_parentesco ) \n"
                + " VALUES (\n"
                + " '" + con.getRut_paciente() + "', '" + con.getTelefono1() + "',\n"
                + "  '" + con.getTelefono2() + "', '" + con.getDireccion() + "',\n"
                + "  CURRENT_TIMESTAMP, '" + con.getRut_usuario() + "',\n"
                + "  '" + con.getIp_contacto() + "', '" + con.getComuna_codigo() + "',\n"
                + "  '" + con.getNombres_paciente() + "', '" + con.getParentesco_desc() + "' );");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    //in
    public boolean ingresa_registro_social(cRegistroSocial reg) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO  schema_uo.registro_social\n"
                + "( reg_usuario, reg_fecha_ingresa, reg_estado, reg_estado_civil,\n"
                + "  reg_situacion_laboral, reg_institucionalizado, reg_nombre_institucion,\n"
                + "  reg_vive,reg_hijos, reg_hijos_cantidad,\n"
                + "  reg_situacion, reg_plan,  reg_duo )  VALUES (\n"
                + "  '" + reg.getRut_usuario() + "', CURRENT_TIMESTAMP, '1', '" + reg.getEstado_civil() + "',\n"
                + "  '" + reg.getSituacion_laboral() + "', '" + reg.getInstitucionalizado() + "', '" + reg.getInstitucion_nombre() + "',\n"
                + "  '" + reg.getVive() + "','" + reg.getHijos() + "', '" + reg.getHijos_cantidad() + "',\n"
                + "  '" + reg.getSituacion() + "', '" + reg.getPlan() + "', '" + reg.getId_duo() + "' ); ");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    public boolean modifica_registro_social(cRegistroSocial reg) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL(" UPDATE  schema_uo.registro_social  \n"
                + "SET  reg_usuario = '" + reg.getRut_usuario() + "',\n"
                + "  reg_estado_civil = '" + reg.getEstado_civil() + "',\n"
                + "  reg_situacion_laboral = '" + reg.getSituacion_laboral() + "',\n"
                + "  reg_institucionalizado = '" + reg.getInstitucionalizado() + "',\n"
                + "  reg_nombre_institucion = '" + reg.getInstitucion_nombre() + "',\n"
                + "  reg_vive = '" + reg.getVive() + "',\n"
                + "  reg_hijos = '" + reg.getHijos() + "',\n"
                + "  reg_hijos_cantidad = '" + reg.getHijos_cantidad() + "',\n"
                + "  reg_situacion = '" + reg.getSituacion() + "',\n"
                + "  reg_plan =  '" + reg.getPlan() + "' \n"
                + "WHERE  reg_id = '" + reg.getId_registro() + "' ;");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    public boolean ingresa_registro_seguimiento(cRegistroSeguimiento seg) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("INSERT INTO schema_uo.registro_social_seguimiento\n"
                + "(  seg_usuario, seg_duo_id,seg_fecha_ingreso,\n"
                + "  seg_fecha, seg_descripcion, seg_estado\n"
                + ")  VALUES (  '" + seg.getRut_usuario() + "', '" + seg.getId_registro_id() + "', CURRENT_TIMESTAMP,\n"
                + "  '" + seg.getFecha_seguimiento() + "','" + seg.getDescripcion() + "','1' );");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    public boolean anula_registro_seguimiento(int id_seguimiento) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);
        this.cnn.setSentenciaSQL("UPDATE  schema_uo.registro_social_seguimiento  \n"
                + "SET seg_estado = '0' WHERE  seg_id = '" + id_seguimiento + "' ");
        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }

    /**/
    public boolean ingresa_evaluacion_neurologica(cEvaNeurologia neu) {
        boolean sw = false;
        this.configurarConexion("");
        this.cnn.setEsSelect(false);

        if (neu.getLesion_evaluada() == 1) {
            this.cnn.setSentenciaSQL("INSERT INTO \n"
                    + "  schema_uo.kin_eva_neurologia\n"
                    + "( neu_id_duo,neu_estado, neu_usuario,\n"
                    + "  neu_fecha_ingreso, neu_lesion_evaluada, neu_lesion, neu_ashworth,neu_tipo_lesion,\n"
                    + "  neu_dato1,neu_dato2, neu_dato3,neu_dato4, \n"
                    + "  neu_dato5, neu_dato6, neu_dato7, neu_dato8, \n"
                    + "  neu_dato9, neu_dato10,neu_dato11,\n"
                    + "  neu_opcion1, neu_opcion2, neu_opcion3,\n"
                    + "  neu_opcion4, neu_opcion5, neu_opcion6,\n"
                    + "  neu_opcion1_desc, neu_opcion2_desc, neu_opcion3_desc,\n"
                    + "  neu_opcion4_desc, neu_opcion5_desc, neu_opcion6_desc\n"
                    + ")  VALUES (\n"
                    + "  '" + neu.getId_duo() + "', '1', '" + neu.getRut_usuario() + "',  CURRENT_TIMESTAMP, "
                    + " '" + neu.getLesion_evaluada() + "', '" + neu.getLesion() + "', '" + neu.getAshworth() + "', '" + neu.getLesion_tipo() + "',\n"
                    + "   '" + neu.getReflejo_osteorendineo_desc() + "', '" + neu.getAsia() + "',  '" + neu.getEvaluacion_sensitiva() + "', '" + neu.getSensibilidad() + "',\n"
                    + "  '" + neu.getMotor_index() + "', '" + neu.getContraccion() + "', '" + neu.getSilla_ruedas() + "', '" + neu.getNivel_sentivo() + "',\n"
                    + "   '" + neu.getNivel_motor() + "', '" + neu.getNivel_neurologico() + "', '" + neu.getMarcha() + "',\n"
                    + "  '" + neu.getOp1() + "', '" + neu.getOp2() + "', '" + neu.getOp3() + "',\n"
                    + "  '" + neu.getOp4() + "','" + neu.getOp5() + "','" + neu.getOp6() + "',\n"
                    + "  '" + neu.getOp1_desc() + "','" + neu.getOp2_desc() + "','" + neu.getOp3_desc() + "',\n"
                    + "  '" + neu.getOp4_desc() + "','" + neu.getOp5_desc() + "', '" + neu.getOp6_desc() + "' );");
        } else {
            this.cnn.setSentenciaSQL("INSERT INTO \n"
                    + "  schema_uo.kin_eva_neurologia\n"
                    + "( neu_id_duo,neu_estado, neu_usuario, neu_fecha_ingreso,"
                    + " neu_lesion_evaluada, neu_lesion, neu_ashworth,\n"
                    + "  neu_dato1,neu_dato2, neu_dato3,neu_dato4, \n"
                    + "  neu_dato5, neu_dato6, neu_dato7, neu_dato8, \n"
                    + "  neu_dato9, neu_dato10,neu_dato11,\n"
                    + "  neu_trofismo, neu_reflejo_osteorendineo, neu_propiocepcion,\n"
                    + "  neu_reaccion_equilibrio,neu_reaccion_enderezamiento, neu_reaccion_apoyo,\n"
                    + "  neu_opcion1, neu_opcion2, neu_opcion3,\n"
                    + "  neu_opcion4, neu_opcion5, neu_opcion6,\n"
                    + "  neu_opcion1_desc, neu_opcion2_desc, neu_opcion3_desc,\n"
                    + "  neu_opcion4_desc, neu_opcion5_desc, neu_opcion6_desc\n"
                    + ") VALUES ( "
                    + " '" + neu.getId_duo() + "', '1', '" + neu.getRut_usuario() + "',  CURRENT_TIMESTAMP, "
                    + " '" + neu.getLesion_evaluada() + "', '" + neu.getLesion() + "', '" + neu.getAshworth() + "',\n"
                    + "  '" + neu.getMotoneurona() + "', '" + neu.getExtrapiramiral() + "', '" + neu.getPostura() + "', '" + neu.getFuerza() + "',\n"
                    + "  '" + neu.getTono_muscular() + "', '" + neu.getTrofismo_adicional() + "', '" + neu.getEess() + "', '" + neu.getEeii() + "',\n"
                    + "  '" + neu.getPropiocepcion_adicional() + "','" + neu.getTransicion() + "', '" + neu.getMarcha() + "',\n"
                    + "  '" + neu.getTrofismo() + "', '" + neu.getReflejo_osteorendineo() + "','" + neu.getPropiocepcion() + "',\n"
                    + " '" + neu.getReaccion_equilibrio() + "','" + neu.getReaccion_enderezamiento() + "', '" + neu.getReaccion_apoyo() + "',\n"
                    + "  '" + neu.getOp1() + "', '" + neu.getOp2() + "', '" + neu.getOp3() + "',\n"
                    + "  '" + neu.getOp4() + "','" + neu.getOp5() + "','" + neu.getOp6() + "',\n"
                    + "  '" + neu.getOp1_desc() + "','" + neu.getOp2_desc() + "','" + neu.getOp3_desc() + "',\n"
                    + "  '" + neu.getOp4_desc() + "','" + neu.getOp5_desc() + "', '" + neu.getOp6_desc() + "' );");
        }

        try {
            this.cnn.conectar();
            sw = true;
        } catch (Exception ex) {
            sw = false;
        } finally {
            this.cnn.cerrarConexion();
        }
        return sw;
    }
    


}
