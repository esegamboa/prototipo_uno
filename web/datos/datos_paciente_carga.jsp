<%-- 
    Document   : datos_paciente_carga
    Created on : 17-may-2012, 12:32:48
    Author     : EseGamboa
--%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="iso-8859-1"%>
<%@page import="CapaDato.cDuo"%>
<%@page import="CapaNegocio.NegocioQ"%>
<%@page import="CapaNegocio.Negocio"%>
<%
    HttpSession session1 = request.getSession();
    String titulo = " style=' background-color: #4169E1 ; color: white '  ";
    String datos = " style=' background-color: #87CEFA ; color: black '  ";
    // String datos = "  style=' color: #000080 '  ";
    int obtiene_perfil = 0;
    String tipo_perfil = "" + session.getAttribute("usuario_perfil_descripcion");
    try {
        obtiene_perfil = Integer.parseInt("" + session.getAttribute("usuario_perfil"));
    } catch (NumberFormatException ex) {
        obtiene_perfil = -1;
    }

    int numero_duo = Integer.parseInt(request.getParameter("duo").toString().trim());

    NegocioQ neg = new NegocioQ();
    cDuo duo = neg.obtiene_duo(numero_duo);

    ArrayList see_visita = new ArrayList();
    see_visita.add(1);
    see_visita.add(2);
    see_visita.add(8);
    see_visita.add(9);
    see_visita.add(4);
    see_visita.add(10);
    see_visita.add(12);

    // see_visita.add(12);
    ArrayList see_alta_medica = new ArrayList();
    see_alta_medica.add(1);
    see_alta_medica.add(2);
    see_alta_medica.add(4);
    see_alta_medica.add(8);
    see_alta_medica.add(9);
    see_alta_medica.add(10);
    see_alta_medica.add(12);

    ArrayList see_alta_administrativa = new ArrayList();
    see_alta_administrativa.add(3);
    see_alta_administrativa.add(10);
    see_alta_administrativa.add(12);

    ArrayList see_kinesiologia = new ArrayList();

    see_kinesiologia.add(10);
    see_kinesiologia.add(12);
    see_kinesiologia.add(13);

    ArrayList see_cambio_cama = new ArrayList();
    see_cambio_cama.add(1);
    see_cambio_cama.add(2);
    see_cambio_cama.add(4);
    see_cambio_cama.add(8);
    see_cambio_cama.add(9);
    see_cambio_cama.add(10);
    see_cambio_cama.add(12);


%>
<script>
    function valida_valida_epicrisis()
    {

        if (confirm('Esta a Punto de Validar que el Paciente Realmente dejo la Cama?'))
        {
            return true;
        } else {
            return false;
        }
    }
</script>

<div style=" vertical-align: top  " align="right" >
    <a href="../uh_visita.jsp">
        <img src="../Imagenes/fileclose.png" width="30" height="30" alt="Cerrar Ventana"/>
    </a>
</div>
<fieldset><legend>Datos del Paciente</legend>
    <table border="1" >
        <tbody>
            <tr>
                <td <%=titulo%> >Rut Paciente</td>
                <td colspan="2" <%=datos%> ><% out.write("" + duo.getRut_paciente());%></td>

                <td <%=titulo%> >Fecha Nacimiento</td>
                <td colspan="2" <%=datos%> >   <% out.write("" + duo.getFecha_nac());%></td>

            </tr>
            <tr>
                <td <%=titulo%> >Nombre Completo</td>
                <td colspan="3" <%=datos%> ><% out.write("" + duo.getNombres_paciente() + " " + duo.getApellidop_paciente() + " " + duo.getApellidom_paciente());%></td>

                <td <%=titulo%> >Edad</td>
                <td <%=datos%> ><% out.write("" + duo.getEdad());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Direcci�n</td>
                <td colspan="5" <%=datos%> ><% out.write("" + duo.getDireccion() + " ," + duo.getComuna_descri());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Tel�fono</td>
                <td colspan="5" <%=datos%> ><% out.write("" + duo.getTelefono1() + "/" + duo.getTelefono2());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Previsi�n</td>
                <td colspan="3" <%=datos%> ><% out.write("" + duo.getCodigo_fonasa_descripcion());%></td>

                <td <%=titulo%> >Tramo</td>
                <td <%=datos%> ><% out.write("" + duo.getTramo_prevision());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Pueblo Ori.</td>
                <td colspan="3" <%=datos%> ><% out.write("" + duo.getPueblo_descri());%></td>

                <td <%=titulo%> >Consultorio Pert.</td>
                <td <%=datos%> ><% out.write("" + duo.getConsultorio_descri());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Naci�n</td>
                <td colspan="3" <%=datos%> ><% out.write("" + duo.getNacion_descripcion());%></td>

                <td <%=titulo%> ></td>
                <td <%=datos%> ></td>
            </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend>Datos Cl�nicos</legend>
    <table border="1">
        <tbody>
            <tr>
                <td <%=titulo%> >ID</td>
                <td colspan="3" <%=datos%> ><% out.write("" + duo.getId_duo());%></td>
                <td <%=titulo%> >CAMA ACTUAL</td>
                <td <%=datos%> ><% out.write("" + duo.getCama_descripcion());%></td>
            </tr>
            <tr>
                <td <%=titulo%> >Derivador</td>
                <td colspan="3" <%=datos%> ><% out.write(duo.getDerivador_descripcion());%></td>
                <td <%=titulo%> >Programa</td>
                <td <%=datos%> ><% out.write("" + duo.getPrograma_descripcion());%></td>
            </tr>
            <tr>
                <td colspan="2" <%=titulo%> >Primera CRD (Interna):</td>
                <td colspan="2"  <%=datos%> ><% out.write("" + duo.getPrimera_clasificacion());%></td>

                <td <%=titulo%> >�ltima CRD:</td>
                <td <%=datos%> ><% out.write("" + duo.getUltima_clasificacion());%></td>
            </tr>
            <tr>
                <td colspan="2" <%=titulo%> >Fecha Ingreso Admision</td>
                <td <%=datos%> ><% out.write("" + duo.getFecha_hora_ing_duo());%></td>
                <td <%=titulo%> >Admisor(a)</td>
                <td colspan="2" <%=datos%> ><% out.write("" + duo.getNombre_usuario_admision());%></td>
            </tr>
            <tr>
                <td colspan="2" <%=titulo%> >Fecha Ingreso Enfermeria</td>
                <td <%=datos%> ><% out.write("" + duo.getFecha_hora_ing_enf());%></td>
                <td <%=titulo%> >Enfermero(a)</td>
                <td colspan="2" <%=datos%> ><% out.write("" + duo.getNombre_usuario_ing_enf());%></td>
            </tr>
            <tr>
                <td colspan="2" <%=titulo%> >Fecha Ingreso Medico</td>
                <td <%=datos%> ><% out.write("" + duo.getFecha_hora_ing_med());%></td>
                <td <%=titulo%> >M�dico(a)</td>
                <td colspan="2" <%=datos%> ><% out.write("" + duo.getNombre_usuario_ing_med());%></td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend>Movimientos</legend>
    <center>
        <table border="0">
            <tbody>
                <tr valign="top" >
                    <% if (see_visita.contains(obtiene_perfil)) {%>
                    <td align="left"  >
                        <%
                            out.write("<form name='form_enf" + duo.getId_duo() + "' id='form_enf" + duo.getId_duo() + "' action='" + neg.getLocal() + "ingreso/visita_enfermeria.jsp' method='POST' >");
                            out.write("<input type='hidden' name='txt_manda_duo' value='" + duo.getId_duo() + "' >");
                            out.write(" <img title='Visita Enfermera' width='25' height='26' style='border:2px solid blue'  src='../Imagenes/Nurse_edit.png' onclick='document.forms[\"form_enf" + duo.getId_duo() + "\"].submit();' style='cursor:pointer'>");
                            out.write("</form>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                        %>
                    </td>
                    <td>

                        <%   out.write("<a href='" + neg.getLocal() + "PDF_DUO?id_duo=" + duo.getId_duo() + "' target='_blank'><img src='../Imagenes/doctorImp.png' width='25' height='26' alt='Ingreso Medico'/></a>");%>
                        <% out.write(" &nbsp;&nbsp;");%>
                        <%
                            out.write("<a href='" + neg.getLocal() + "PDF_ingreso_enfermeria?txt_duo=" + duo.getId_duo() + "' target='_blank'><img src='../Imagenes/enfermeraImp.png' width='25' height='26' alt='Ingreso Enfermeria'/></a>&nbsp;&nbsp;");

                            //    out.write("<a href='http://10.8.4.9:9090/modulo_uo/Ingreso/IngEnfPDF.jsp?id_duo=" + duo.getId_duo() + "' target='_blank'><img src='../Imagenes/enfermeraImp.png' width='25' height='26' alt='Ingreso Enfermeria'/></a>&nbsp;&nbsp;");
%>


                    </td>

                    <td>

                        <form action="../egreso/cambio_cama.jsp" name="form_cambiar_entre" method="POST" >
                            <input type="hidden" name="txt_manda_duo" value="<%=duo.getId_duo()%>"  >
                            <input type="hidden"name="txt_modo" value="1"  >
                            <input type="submit" name="btn_cambiar_disponible"  value="CAMBIAR A CAMA DISPONIBLE" style="width: 200px"   />
                            &nbsp;
                        </form>
                        <br>       
                        <form action="../egreso/cambio_cama.jsp" name="form_cambiar_entre" method="POST" >
                            <input type="hidden" name="txt_manda_duo" value="<%=duo.getId_duo()%>"  >
                            <input type="hidden" name="txt_modo" value="2"  >
                            <input type="submit" name="btn_cambiar_entre"  value="CAMBIAR ENTRE CAMAS" style=" width:  200px"   />
                            &nbsp;
                        </form>

                    </td>
                    <td>
                        &nbsp;&nbsp;
                    </td>


                    <% }%>

                    <td>
                        <%     if (see_kinesiologia.contains(obtiene_perfil)) {
                                out.write("<form name='form_kine" + duo.getId_duo() + "' id='form_kine" + duo.getId_duo() + "' action='" + neg.getLocal() + "kinesiologia/datos_kinesiologia.jsp' method='POST' >");
                                out.write("<input type='hidden' name='txt_manda_duo' value='" + duo.getId_duo() + "' >");
                                out.write(" <img title='Kinesiologia' width='25' height='26'   src='../Imagenes/k.png' onclick='document.forms[\"form_kine" + duo.getId_duo() + "\"].submit();' style='cursor:pointer'>");
                                out.write("</form>");
                            }
                        %>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>


                    <td>
                        <% if (duo.getEstado_duo() == 21) {
                                if (see_alta_medica.contains(obtiene_perfil)) {
                        %>
                        <form action="<%=neg.getLocal()%>egreso/alta_medica.jsp" name="form_alta_medica" method="POST"   >
                            <input type="hidden" name="txt_manda_duo" value="<%=duo.getId_duo()%>"  >
                            <input type="hidden" name="txt_modo" value="1"  >
                            <input type="submit" name="btn_alta_medica"  value="DAR ALTA MEDICA"   />
                            &nbsp;
                        </form>
                        <%     } else {
                                out.write("Debe tener Epicrisis en UO");
                            }

                        } else if (duo.getEstado_duo() == 3) {

                            if (duo.getFecha_hora_alta_med_duo().contains("---")) {

                                if (see_alta_medica.contains(obtiene_perfil)) {
                        %>
                        <form action="<%=neg.getLocal()%>ingresa_alta" name="form_alta_adm" method="POST" onsubmit="return  valida_valida_epicrisis()"  >
                            <input type="hidden" name="id_duo" value="<%=duo.getId_duo()%>"  >
                            <input type="hidden" name="txt_modo" value="2"  >
                            <input type="submit" name="btn_alta_adm"  value="VALIDAR ALTA MEDICA"   />
                            &nbsp;
                        </form>
                        <%
                            } else {
                                out.write("Debe validarse en UO");
                            }
                        } else {

                            if (see_alta_administrativa.contains(obtiene_perfil)) {
                        %>
                        <form action="<%=neg.getLocal()%>egreso/alta_administrativa.jsp" name="form_alta_adm" method="POST"  >
                            <input type="hidden" name="txt_manda_duo" value="<%=duo.getId_duo()%>"  >
                            <input type="hidden" name="txt_modo" value="3"  >
                            <input type="submit" name="btn_alta_adm"  value="DAR ALTA ADMINISTRATIVA"   />
                            &nbsp;
                        </form>
                        <%  } else {
                                        out.write("Debe ser Dado de Alta en Admision UGU");
                                    }
                                }
                            } else {
                                out.write("Duo estado:" + duo.getEstado_duo_descripcion());
                            }
                        %>
                    </td>
                </tr>
            </tbody>
        </table>
    </center>
</fieldset>

