<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><head>
        <title>New document</title>
        <meta http-equiv="content-type" content="text/html; charset=windows-1252">
            <style>
                body {
                    font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
                    font-size: 12;
                }

                #content {
                    width: 1200px;
                    margin: 0px auto;
                }

                #column-left {
                    background-color: #EBE9EA;
                    border: 1px solid #D2D2D2;
                    border-radius: 8px 8px 8px 8px;
                    float: left;
                    position: fixed;
                    min-height: 225px;
                    margin-bottom: 10px;
                    margin-right: 10px;
                    overflow: hidden;
                    text-align: center;
                    width: 200px;
                }

                #central {
                    background-color: #EBE9EA;
                    border: 1px solid #D2D2D2;
                    border-radius: 8px 8px 8px 8px;
                    float: right;
                    height: 5000px;
                    margin-bottom: 10px;
                    width: 985px;
                }


            </style>
    </head>
    <body>
        <div id="content">
            <div id="column-left">Columna izquierda que se mueve con el scroll...</div>
            <div id="central">Bloque central</div>
        </div>

    </body></html>