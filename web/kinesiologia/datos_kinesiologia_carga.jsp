<%-- 
    Document   : datos_kinesiologia_carga
    Created on : 27-ago-2014, 16:13:55
    Author     : Informatica
--%>

<%@page import="CapaDato.cEvaNeurologia"%>
<%@page import="CapaDato.cDuo"%>
<%@page import="CapaDato.cEvaTraumatologia"%>
<%@page import="CapaDato.cSesionKine"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CapaNegocio.NegocioQ"%>
<%@page contentType="text/html" pageEncoding="iso-8859-1"%>

<%

    NegocioQ neg = new NegocioQ();

    int id_duo = Integer.parseInt(request.getParameter("duo"));
    ArrayList lista_sesion_kine = neg.lista_sesion_kinesiologia(id_duo);
    ArrayList lista_eva_trauma = neg.lista_evaluacion_traumatologia(id_duo);
    
     ArrayList lista_eva_neuro = neg.lista_evaluacion_neurologia(id_duo);

    String titulo = " style=' background-color: #4169E1 ; color: white '  ";
    String datos = " style=' background-color: #87CEFA ; color: black '  ";
%>
<jsp:include page="../css/boton_html.jsp" />
<div style=" vertical-align: top  " align="right" >
    <a href="#" onclick="history.back(1)" >
        <img src="../Imagenes/fileclose.png" width="30" height="30" alt="Cerrar Ventana"/>
    </a>
</div>

<table>
    <tr>
        <td rowspan="2" ><img src="../Imagenes/kinesiologia.png" width="48" height="48" alt="kinesiologia"/></td>
        <td <%=titulo%> >Datos de Kinesiología</td>

    </tr>
    <tr>
        <td <%=datos%>>Documentos</td>

    </tr>

</table>

<fieldset>
    <legend  >Sesión kinésica</legend>
    <table>
        <tr>
            <%    //
                Iterator it_lista_sesion_kine = lista_sesion_kine.iterator();
                int contador = 0;
                if (lista_sesion_kine.size() == 0) {
                    out.write("<h3>No hay registros para este DUO</h3>");
                } else {
                    if (it_lista_sesion_kine.hasNext()) {
                        cSesionKine ses = (cSesionKine) it_lista_sesion_kine.next();
                        contador++;
                        if (contador == 5) {
                            out.write("<tr>");
                            contador = 0;
                        }
                        out.write("<td>");
                        out.write("<form  action='../PDF_sesion_kinesiologia' method='POST' target='_blank' > "
                                + "   <input type='hidden' value='" + id_duo+ "' name='txt_duo'> "
                                + "   <button name='boton' type='submit'><img src='../Imagenes/pdf.png'> " + ses.getFecha() + "</button>  "
                                + " </form>  ");
                        out.write("</td>");
                    }
                }


            %>
    </table>  
</fieldset>

<fieldset>
    <legend>Evaluación Traumatológica</legend>
    <table>
        <tr>
            <%  ////
                Iterator it_eva_trauma = lista_eva_trauma.iterator();
                if (lista_eva_trauma.size() == 0) {
                    out.write("<h3>No hay registros para este DUO</h3>");
                } else {
                    while (it_eva_trauma.hasNext()) {
                        cEvaTraumatologia tra = (cEvaTraumatologia) it_eva_trauma.next();

                        out.write("<td>");
                        out.write("<form  action='../PDF_evaluacion_traumatologica' method='POST' target='_blank' > "
                                + "   <input type='hidden' value='" + tra.getId_eva_traumatologia() + "' name='txt_id'> "
                                + "   <button name='boton' type='submit'><img src='../Imagenes/pdf.png'> " + tra.getFecha_ingreso_trauma() + "</button>  "
                                + " </form>  ");
                        out.write("</td>");
                    }
                }

            %>
    </table>  
</fieldset>
    <br>
    <fieldset>
    <legend>Evaluación Neurológica</legend>
    <table>
        <tr>
            <%  ////
                Iterator it_eva_neuro = lista_eva_neuro.iterator();
                if (lista_eva_neuro.size() == 0) {
                    out.write("<h3>No hay registros para este DUO</h3>");
                } else {
                    while (it_eva_neuro.hasNext()) {
                        cEvaNeurologia neu = (cEvaNeurologia) it_eva_neuro.next();

                        out.write("<td>");
                        out.write("<form  action='../PDF_evaluacion_neurologica' method='POST' target='_blank' > "
                                + "   <input type='hidden' value='" + neu.getId_neuro() + "' name='txt_id'> "
                                + "   <button name='boton' type='submit'><img src='../Imagenes/pdf.png'> " +neu.getFecha_ingreso_neuro() + "</button>  "
                                + " </form>  ");
                        out.write("</td>");
                    }
                }

            %>
    </table>  
</fieldset>
<br>
<fieldset>
    <legend>Registros</legend>
    <center>
        <TABLE>
            <TR>
                <TD>
                    <form name="form_res"  action="evaluacion_respiratoria.jsp" method="POST"  >
                        <input type="hidden" name="txt_duo" value="<%=id_duo%>" > 
                        <input class="botonMonitor" type="submit" value="Registrar Ev. Respiratoria" name="btn_respiratoria" disabled="disabled" />
                    </form> 
                </TD>
                <TD>
                    <form name="form_neu"  action="evaluacion_neurologica.jsp" method="POST"  >
                        <input type="hidden" name="txt_duo" value="<%=id_duo%>" > 
                        <input class="botonMonitor" type="submit" value="Registrar Ev. Neurológica" name="btn_neu"  />
                    </form>  
                </TD>
                <TD>
                    <form name="form_tra"  action="evaluacion_traumatologica.jsp" method="POST"  >
                        <input type="hidden" name="txt_duo" value="<%=id_duo%>" > 
                        <input class="botonMonitor" type="submit" value="Registrar Ev. Traumatológica" name="btn_tra" />
                    </form> 
                </TD>
                <TD>
                    <form name="form_kin"  action="sesion_kinesica.jsp" method="POST"  >
                        <input type="hidden" name="txt_duo" value="<%=id_duo%>" > 
                        <input class="botonMonitor" type="submit" value="Registrar Sesión Kinésica" name="btn_ses" />
                    </form> 
                </TD>
            </TR>
        </TABLE>
    </center>
</fieldset>




