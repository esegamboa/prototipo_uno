<%-- 
    Document   : admision_ugu_carga
    Created on : 10-may-2012, 12:11:06
    Author     : EseGamboa
--%>
<%@page import="CapaDato.cNacion"%>
<%@page import="CapaDato.cEpicrisis"%>
<%@page import="CapaDato.cPaciente"%>
<%@page import="CapaNegocio.Negocio_fonasa"%>
<%@page import="CapaDato.cPueblo"%>
<%@page import="CapaDato.cCama"%>
<%@page import="CapaDato.cConsultorio"%>
<%@page import="CapaDato.cComuna"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="CapaNegocio.NegocioQ"%>
<%@page contentType="text/html" pageEncoding="iso-8859-1"%>
<%
    
    NegocioQ neg = new NegocioQ();
    Negocio_fonasa n_fon = new Negocio_fonasa();
    String obtiene_rut = request.getParameter("user");
    
    ArrayList lista_duo = neg.lista_documentos_paciente(obtiene_rut);
    Iterator itt = lista_duo.iterator();
    boolean sw_esta = false;
    while (itt.hasNext()) {
        cEpicrisis epi = (cEpicrisis) itt.next();
        if (epi.getEstado_duo() == 1 || epi.getEstado_duo() == 2 || epi.getEstado_duo() == 3 || epi.getEstado_duo() == 21) {
            sw_esta = true;
        }
    }
    
    if (sw_esta) {
        out.write("<h2>ESTE PACIENTE YA TIENE UN REGISTRO ACTIVO</h2>");
    } else {
        
        String hora_Registro = neg.obtiene_fecha_hora();
        //String obtiene_rut = "16.623.070-5";
            /* informacion para llenar el formulario*/
        ArrayList comuna = neg.lista_comuna();
        ArrayList consultorio = neg.lista_consultorio_pertenecia();
        ArrayList derivador = neg.lista_derivador();
        ArrayList cama = neg.lista_cama_desocupada("11,12,13,14,15,16,17,18,19,20"); // 1 es el area para camas disponibles normales , puede ser tambien -(1,2)-
        ArrayList pueblo = neg.lista_pueblo();
        ArrayList nacion = neg.lista_nacion();
        
        Iterator it_com = comuna.iterator();
        Iterator it_cons = consultorio.iterator();
        Iterator it_der = derivador.iterator();
        Iterator it_cam = cama.iterator();
        Iterator it_pue = pueblo.iterator();
        Iterator it_nac = nacion.iterator();
        
        cComuna com = new cComuna();
        cConsultorio cons = new cConsultorio();
        cConsultorio der = new cConsultorio();
        cCama cam = new cCama();
        cPueblo pue = new cPueblo();
        cNacion nac = new cNacion();

        /* FIN informacion para llenar el formulario*/
        // cPaciente pac_fon = n_fon.getConsultaPrevision(obtiene_rut);
        cPaciente pac = neg.obtiene_paciente(obtiene_rut);
        
        String a_nombres = "";
        String a_apellidop = "";
        String a_apellidom = "";
        String a_fono1 = "";
        String a_fono2 = "";
        String a_rut = "";
        String a_direccion = "";
        String a_fecha_nacimiento = "";
        int a_sexo = -1;
        String a_comuna = "";
        int a_pueblo = -1;
        int a_consultorio_pertenencia = -1;
        String a_mail = "";
        
        String a_codigo_fonasa = "";
        String a_codigo_fonasa_descripcion = "";
        String a_tramo = "";
        int a_prais = -1;
        int existe = 0;

        //  pac.setRut("");
        if (pac.getRut_paciente().equals("")) {
            existe = 0;
            cPaciente pac_fon = null;
            try {
                pac_fon = n_fon.getConsultaPrevision(obtiene_rut);
            } catch (Exception ex) {
                pac_fon = new cPaciente();
            }
            
            a_nombres = pac_fon.getNombres_paciente() + "";
            a_apellidop = pac_fon.getApellidop_paciente();
            a_apellidom = pac_fon.getApellidom_paciente();
            a_fono1 = pac_fon.getTelefono1();
            a_fono2 = pac_fon.getTelefono2();
            a_rut = pac_fon.getRut_paciente();
            a_rut = obtiene_rut;
            
            a_fecha_nacimiento = pac_fon.getFecha_nac();
            a_sexo = pac_fon.getSexo();
            a_codigo_fonasa_descripcion = pac_fon.getCodigo_fonasa_descripcion();
            a_tramo = pac_fon.getTramo_prevision();
            a_prais=pac_fon.getPrais();
            
        } else {
            existe = 1;
            a_nombres = pac.getNombres_paciente();
            a_apellidop = pac.getApellidop_paciente();
            a_apellidom = pac.getApellidom_paciente();
            a_fono1 = pac.getTelefono1();
            a_fono2 = pac.getTelefono2();
            a_rut = pac.getRut_paciente();
            a_direccion = pac.getDireccion();
            a_fecha_nacimiento = pac.getFecha_nac();
            a_sexo = pac.getSexo();
            
            a_comuna = pac.getComuna_codigo() + ""; // xq el tipo de id comuna es varchar en la BD ��
            a_pueblo = pac.getPueblo();
            a_consultorio_pertenencia = pac.getConsultorio();
            a_mail = pac.getMail();
            a_prais = pac.getPrais();
            a_tramo = pac.getTramo_prevision();
        }

        /**/
        String cbo_opcion_seleccionada = "";
        /**/

        //  out.write(""+pac.getCodigo_fonasa()+"<br>"+pac.getTramo_prevision()+"<br>"+pac.getPrais());
%>

<form id="form1" name="form1" action="<% out.write(neg.getLocal());%>ingreso_uh" onsubmit="return valida_form()" method="GET"   >
    <input type="hidden" name="modo" id="modo" value="1">
    <input type="hidden" name="existe" id="existe" value="<%=existe%>">
    <input type="hidden" name="verificado_fonasa" id="verificado_fonasa" name="verificado_fonasa" value="0">
    <div id="Datitos"><input type="hidden" id="id_duo" value="0"></div>
    <fieldset>
        <legend>Ingreso del Paciente:<%=obtiene_rut%></legend>
        <table  style="FONT-FAMILY: Verdana; FONT-SIZE: 13px;" BORDER="0" width="720">
            <tr><td>Nombre:</td>
                <td><input type="text" size="25" name="nombres" id="nombres" value="<%=a_nombres%>" readonly="readonly" ></td>
                <td>Apellido P.</td><td><input name="apellidop" id="apellidop" type="text" size="25" value="<%=a_apellidop%>" readonly="readonly" ></td>
                <td>Apellido M.</td><td><input name="apellidom" id="apellidom" type="text" size="25" value="<%=a_apellidom%>" readonly="readonly" ></td>
            <tr>
                <td>Rut:</td>
                <td>
                    <input type="text" name="rut" id="rut" value="<%=a_rut%>" readonly="readonly" >
                </td>
                <td>Fecha Nac:</td>
                <td>
                    <input name="fecha_nac" id="fecha_nac" type="text" size="20" value="<%=a_fecha_nacimiento%>" readonly="readonly" >
                    <%
                        /*
                         <img src="Imagenes/calender.png" id="f_trigger_a" style="cursor:pointer" onclick="return showCalendar('fecha_nac', '%Y-%m-%d %H:%M', '24', true);">
                         */
                    %>
                </td>
                <td>Sexo:</td>
                <td>
                    <%    
                        String marca0 = "";
                        String marca1 = "";
                        if (a_sexo == 0) {
                            marca0 = "  checked='checked' ";//HOMBRE
                        } else if (a_sexo == 1) {
                            marca1 = "  checked='checked' "; // MUJER
                        }
                    %>
                    M<input type="radio" name="rbt_sexo" id="rbt_sexo0" readonly="readonly" value="0" <%=marca0%>    />
                    F<input type="radio" name="rbt_sexo" id="rbt_sexo1" readonly="readonly"  value="1" <%=marca1%> />
                </td>
            </tr>
            <tr>
                <td>Direcci�n:</td>
                <td colspan="3">
                    <input type="text" size="60" onkeyup="for (i = 0; i < 100; i++) {
                                if (this.value[i] == '#') {
                                    alert('No use Caracteres como # u Otros!!!');
                                    this.value = this.value.replace('#', 'N� ');
                                }
                            }" id="direccion" name="direccion" value="<%=a_direccion%>">
                </td>
                <td>Comuna:</td>
                <td>
                    <select id="id_comuna" name="id_comuna" >
                        <option value="-2" >Seleccione...</option>
                        <%
                            
                            while (it_com.hasNext()) {
                                com = (cComuna) it_com.next();
                                cbo_opcion_seleccionada = "  ";
                                if (a_comuna.equals(com.getComuna_codigo())) {
                                    cbo_opcion_seleccionada = " selected='selected' ";
                                }
                                out.write("<option value='" + com.getComuna_codigo() + "' " + cbo_opcion_seleccionada + " >" + com.getComuna_descripcion() + "</option>");
                            }
                        %>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tel�fono:</td>
                <td>
                    <input type="text" id="telefono1" name="telefono1"  value="<%=a_fono1%>">
                </td>
                <td>Celular: 09-</td>
                <td><input type="text" id="telefono2" name="telefono2"  value="<%=a_fono2%>"></td>
            </tr>

            <tr>
                <td>Mail</td>
                <td colspan="3" >
                    <input style=" width: 400px " type="text" id="txt_mail" name="txt_mail"  value="<%=a_mail%>">
                </td>
            </tr>

            <tr>
                <td>Consultorio Pertenecia:</td>
                <td>
                    <select  name="id_consultorio_pertenencia" id="id_consultorio_pertenencia">
                        <option value="-2" >Seleccione...</option>
                        <%
                            while (it_cons.hasNext()) {
                                cons = (cConsultorio) it_cons.next();
                                cbo_opcion_seleccionada = "  ";
                                if (cons.getId() == a_consultorio_pertenencia) {
                                    cbo_opcion_seleccionada = " selected='selected' ";
                                }
                                out.write("<option value='" + cons.getId() + "' " + cbo_opcion_seleccionada + " >" + cons.getDescripcion() + "</option>");
                            }
                        %>
                    </select>
                </td>
                <td>Pueblo Originario:</td>
                <td>
                    <select  name="id_pueblo" id="id_pueblo">
                        <option value="-2" >Seleccione...</option>
                        <%    
                            while (it_pue.hasNext()) {
                                pue = (cPueblo) it_pue.next();
                                cbo_opcion_seleccionada = "  ";
                                if (pue.getId_pueblo() == a_pueblo) {
                                    cbo_opcion_seleccionada = " selected='selected' ";
                                }
                                out.write("<option value='" + pue.getId_pueblo() + "' " + cbo_opcion_seleccionada + " >" + pue.getDescripcion_pueblo() + "</option>");
                            }
                        %>
                    </select>
                </td>
                <td>Naci�n Origen:
                </td>
                <td>
                    <select name="paciente_nacion" id="paciente_nacion" style=" width:150px;  " >
                        <%    
                            while (it_nac.hasNext()) {
                                nac = (cNacion) it_nac.next();
                                if (nac.getDescripcion_nac().equalsIgnoreCase("CHILE")) {
                                    out.write("<option value='" + nac.getId_nac() + "' selected='selected' >" + nac.getDescripcion_nac() + "</option>");
                                } else {
                                    out.write("<option value='" + nac.getId_nac() + "' >" + nac.getDescripcion_nac() + "</option>");
                                }
                                
                            }

                        %>
                    </select>
                </td>
            </tr>

            <tr>
                <td>Previsi�n:</td>
                <td>
                    <select id='paciente_prevision' name='paciente_prevision'>
                        <%                            String seleccionador_A = "";
                            String seleccionador_B = "";
                            String seleccionador_C = "";
                            String seleccionador_D = "";
                            String seleccionador_0 = "";
                            String seleccionador_prais = "";
                            if (a_tramo.contains("A")) {
                                seleccionador_A = " selected='selected' ";
                            } else if (a_tramo.contains("B")) {
                                seleccionador_B = " selected='selected' ";
                            } else if (a_tramo.contains("C")) {
                                seleccionador_C = " selected='selected' ";
                            } else if (a_tramo.contains("D")) {
                                seleccionador_D = " selected='selected' ";
                            } else if (a_tramo.contains("0")) {
                                seleccionador_0 = " selected='selected' ";
                            } else if (a_prais == 1) {
                                seleccionador_prais = " selected='selected' ";
                            }
                            
                            out.write("<option value=\"-2\">Seleccione...");
                            out.write(" <OPTION VALUE=\"00110A0\" " + seleccionador_A + " >FONASA A");  // <!--los 4 primeros es el cdigo fonasa, el quinto el tramo y el sexto Prais o no-->
                            out.write("<OPTION VALUE=\"00110B0\" " + seleccionador_B + " >FONASA B");
                            out.write("<OPTION VALUE=\"00110C0\" " + seleccionador_C + " >FONASA C");
                            out.write("<OPTION VALUE=\"00110D0\" " + seleccionador_D + " >FONASA D");
                            out.write("<OPTION VALUE=\"0190100\" " + seleccionador_0 + " >PARTICULAR/ISAPRE");
                            out.write("<OPTION VALUE=\"00110A1\" " + seleccionador_prais + " >PRAIS");

                        %>
                    </select>
                </td>
                <td colspan="4"> <% 
                            
                            if (a_codigo_fonasa_descripcion.length()>0){
                            out.write(a_codigo_fonasa_descripcion + "(Tramo " + a_tramo + ")<br> "); 
                            }
                            if (a_prais==1){
                            out.write("PRAIS<BR>");
                            }
                
                %> <img src="Iconos/dialog_information_small.png" width="15" alt="" height="20">[Sujeta a verificaci�n a la salida del paciente]</td>

               
                </td><td>
                    <input type="hidden" name="paciente_programa" id="paciente_programa" value="0" >
                <td>
            </tr>
            <tr>
                <td>Fecha y Hora:</td>
                <td>
                    <input name="fecha_duo" id="fecha_duo" type="text" size="22" value="<% out.write(hora_Registro);%>" >
                </td>
                <td>Derivado desde:</td>
                <td>
                    <select name="id_derivado" id="id_derivado">
                        <option value="-2">Seleccione...</option>
                        <%
                            while (it_der.hasNext()) {
                                der = (cConsultorio) it_der.next();
                                out.write("<option value='" + der.getId() + "' >" + der.getDescripcion() + "</option>");
                            }
                        %>
                    </select>
                </td>
                <td>N� Cama:</td>
                <td>
                    <select  name="id_cama" id="id_cama">
                        <option value="-2">Seleccione...</option>
                        <%    
                            while (it_cam.hasNext()) {
                                cam = (cCama) it_cam.next();
                                out.write("<option value='" + cam.getId_cama() + "' >" + cam.getDescripcion_cama() + "</option>");
                                
                            }
                        %>
                    </select>
                </td>
            </tr>

        </table>


        <fieldset class="buttons">
            <br><br>
            <input class="botonMonitor" type="submit" value="GUARDAR DATOS" name="btn_guarda_datos" />

            <%
                /*
                 * <input type="button" id="BtnGuardarDatos" value="Guardar Datos" >
                 <input type="button" id="BtnIngresar" style="display:none;" class="DR" value="Ingresar Paciente a Cama" onclick="if(confirm('Esta a Punto de Ingresar Al Paciente: '+document.getElementById('nombres').value+'a la cama '+document.getElementById('id_cama').value+'\nEsta Seguro?')){location.href='ModificaEstadoDuo.jsp?estado=1&id_duo='+document.getElementById('id_duo').value;}">
                 <input type="button" id="BtnGuardarCambios" style="display:none" value="Guardar Cambios" onclick="if(DuoSinConexion()){GuardaDUO(2);}">
                 <input type="button" disabled style="display:none" id="BtnFicha" value="Ver Ficha" onclick="window.open('http://10.8.4.9:9090/modulo_agenda/pdf_portada_ficha.jsp?rut='+document.getElementById('paciente_rut').value+'&rut2='+document.getElementById('txtRutSinDV').value+'&dv='+document.getElementById('txtDV').value,'pop-up','width=500, height=500, scrollbars=yes, menubar=no, location=yes, status=no, resizable=yes,left = 800,top = 0')">
                 */

            %>

            <br><br>
        </fieldset>
    </fieldset>
</form>

<%    }
%>